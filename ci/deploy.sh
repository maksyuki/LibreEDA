
build=${CI_BUILD_ID:-999999}
ref=$(git symbolic-ref -q HEAD 2>/dev/null || :)
ref=${ref##refs/heads/}
ref=${ref:-unknown}
ref=${CI_BUILD_REF_NAME:-$ref}

case $ref in
    master)
        dir=master
        ;;
    *)
        dir=branch/$ref
        ;;
esac

remoteLink=www/snapshots/$dir/latest
remoteDir=www/snapshots/$dir/$build

files=$(find . -name 'libre-eda-*' -maxdepth 1 -type f | tr '\n' ' ')
[ "x$files" == "x" ] && { echo "** No files to deploy."; return 1; }

echo "** Deploying the following files: $files ..."

host=download-libre-eda-org@download.libre-eda.org

scp $files $host:~
ssh $host <<EOF
set -e
set -x
mkdir -p $remoteDir;
mv $files $remoteDir;
ln -sfT $build $remoteLink;
ls -l $remoteDir
ls -l $remoteDir/..
EOF
