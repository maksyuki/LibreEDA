#include "core/core.h"

#include <QtTest>
#include <QDir>
#include <QFile>

class tst_Path : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase();

    void pathIsDefined_data();
    void pathIsDefined();
    void pathIsAccessible_data();
    void pathIsAccessible();
};

void tst_Path::initTestCase()
{
    Core::initialise();
}

void tst_Path::pathIsDefined_data()
{
    QTest::addColumn<QString>("path");
    QTest::newRow("Source") << Core::sourcePath();
    QTest::newRow("Root") << Core::rootPath();
    QTest::newRow("Application") << Core::applicationPath();
    QTest::newRow("Binary") << Core::binaryPath();
    QTest::newRow("Private binary") << Core::privateBinaryPath();
    QTest::newRow("Library") << Core::libraryPath();
    QTest::newRow("Plugin") << Core::pluginPath();
    QTest::newRow("Data") << Core::dataPath();
    QTest::newRow("Documentation") << Core::documentationPath();
}

void tst_Path::pathIsDefined()
{
    QFETCH(QString, path);
    QVERIFY2(!path.isEmpty(), path.toStdString().c_str());
    QVERIFY2(!path.isNull(), path.toStdString().c_str());
}

void tst_Path::pathIsAccessible_data()
{
    QTest::addColumn<QString>("path");
    QTest::newRow("Source") << Core::sourcePath();
    QTest::newRow("Root") << Core::rootPath();
    QTest::newRow("Application") << Core::applicationPath();
    QTest::newRow("Binary") << Core::binaryPath();
    // Nothing installed QTest::newRow("Private binary") << Core::privateBinaryPath();
    QTest::newRow("Library") << Core::libraryPath();
    QTest::newRow("Plugin") << Core::pluginPath();
    QTest::newRow("Data") << Core::dataPath();
    // Nothing installed QTest::newRow("Documentation") << Core::documentationPath();
}

void tst_Path::pathIsAccessible()
{
    QFETCH(QString, path);
    QDir dir(path);
    QVERIFY2(dir.exists(), path.toStdString().c_str());
    QVERIFY2(dir.isReadable(), path.toStdString().c_str());
}

QTEST_MAIN(tst_Path)

#include "tst_path.moc"
