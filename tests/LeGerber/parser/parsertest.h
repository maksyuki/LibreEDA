#pragma once

#include <QtTest>

class parserTest : public QObject
{
	Q_OBJECT

public:
	parserTest();

private Q_SLOTS:
	void initTestCase();
	void cleanupTestCase();

	void testDefaultState();
	void testEmptyDataIsValid();
	void testReadCoordinateNumber_data();
	void testReadCoordinateNumber();
	void testReadCoordinate();

	void testParseMultiline_data();
	void testParseMultiline();

	void testBasicGCodes();
	void testBasicMCodes();
	void testBasicDCodes();

	void testNominalD01();
	void testNominalD02();
	void testNominalD03();

	void testNominalDnn_data();
	void testNominalDnn();
	void testNominalG54Dnn_data();
	void testNominalG54Dnn();

	void testNominalStandaloneCoordinates_data();
	void testNominalStandaloneCoordinates();

	void testComposedGCodes_data();
	void testComposedGCodes();

	void testNominalFSCode_data();
	void testNominalFSCode();

	void testNominalMOCode_data();
	void testNominalMOCode();

	void testNominalADCode_data();
	void testNominalADCode();

	void testNominalAMCode_data();
	void testNominalAMCode();

	void testNominalLPCode_data();
	void testNominalLPCode();

	void testNominalSRCode_data();
	void testNominalSRCode();

	void testNominalTACode_data();
	void testNominalTACode();

	void testNominalTFCode_data();
	void testNominalTFCode();

	void testNominalTDCode_data();
	void testNominalTDCode();

	void testNominalASCode_data();
	void testNominalASCode();

	void testNominalINCode_data();
	void testNominalINCode();

	void testNominalIPCode_data();
	void testNominalIPCode();

	void testNominalIRCode_data();
	void testNominalIRCode();

	void testNominalLNCode_data();
	void testNominalLNCode();

	void testNominalMICode_data();
	void testNominalMICode();

	void testNominalOFCode_data();
	void testNominalOFCode();

	void testNominalSFCode_data();
	void testNominalSFCode();

	void testSimpleApertureAttributeCodes();
};
