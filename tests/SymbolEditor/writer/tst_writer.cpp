
#include "xdl/symbol.h"
#include "xdl/symbolwriter.h"

#include <QtTest>
#include <QtDebug>

using namespace SymbolEditor;

// TODO: Test serialisation and deseraialisation of double
// with focus of keeping precision between document and code
class tst_Writer : public QObject
{
    Q_OBJECT

private slots:
    void emptyDocument();

    void symbolProperties();
    void symbolPropertiesUtf8();

    void circleProperties();
    void circularArcProperties();
    void ellipseProperties();
    void ellipticalArcProperties();
    void rectangleProperties();
    void polylineProperties();
    void polygonProperties();
    void pinProperties();
    void groupProperties();
    void labelProperties();

    void lineStyleProperty_data();
    void lineStyleProperty();
    void lineWidthProperty_data();
    void lineWidthProperty();
    void fillStyleProperty_data();
    void fillStyleProperty();
    void colorProperty_data();
    void colorProperty();
};

// TODO: Would be nice to have XML differ.
void tst_Writer::emptyDocument()
{
    QString expected("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                     "<symbol xmlns=\"http://www.leda.org/xdl\">"
                     "<name></name>"
                     "<label></label>"
                     "<drawing/>"
                     "</symbol>\n");

    auto symbol = new Symbol();
    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::symbolProperties()
{
    QString expected("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                     "<symbol xmlns=\"http://www.leda.org/xdl\">"
                     "<name>ANAME</name>"
                     "<label>ALABEL</label>"
                     "<drawing/>"
                     "</symbol>\n");

    auto symbol = new Symbol();
    symbol->name = "ANAME";
    symbol->description = "ALABEL";
    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::symbolPropertiesUtf8()
{
    QString expected("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                     "<symbol xmlns=\"http://www.leda.org/xdl\">"
                     "<name>ANAME©</name>"
                     "<label>µæðϾ</label>"
                     "<drawing/>"
                     "</symbol>\n");

    auto symbol = new Symbol();
    symbol->name = "ANAME©";
    symbol->description = "µæðϾ";
    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::circleProperties()
{
    QString expected("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                     "<symbol xmlns=\"http://www.leda.org/xdl\">"
                     "<name></name>"
                     "<label></label>"
                     "<drawing>"
                     "<circle>"
                     "<position><x>0</x><y>0</y></position>"
                     "<visible>true</visible>"
                     "<locked>false</locked>"
                     "<x-mirrored>false</x-mirrored>"
                     "<y-mirrored>false</y-mirrored>"
                     "<opacity>100</opacity>"
                     "<rotation>0</rotation>"
                     "<line-style>Solid</line-style>"
                     "<line-width>Medium</line-width>"
                     "<line-color>SecondaryContent</line-color>"
                     "<fill-style>Solid</fill-style>"
                     "<fill-color>BackgroundHighlight</fill-color>"
                     "<radius>0</radius>"
                     "</circle>"
                     "<circle>"
                     "<position><x>1.2</x><y>3.4</y></position>"
                     "<visible>false</visible>"
                     "<locked>true</locked>"
                     "<x-mirrored>true</x-mirrored>"
                     "<y-mirrored>true</y-mirrored>"
                     "<opacity>49.9</opacity>"
                     "<rotation>12.5</rotation>"
                     "<line-style>DashDotDot</line-style>"
                     "<line-width>Thickest</line-width>"
                     "<line-color>Red</line-color>"
                     "<fill-style>None</fill-style>"
                     "<fill-color>Violet</fill-color>"
                     "<radius>3.14</radius>"
                     "</circle>"
                     "</drawing>"
                     "</symbol>\n");

    auto symbol = new Symbol();

    auto circle1 = new CircleItem();
    symbol->drawingItems.append(circle1);

    auto circle2 = new CircleItem();
    circle2->setPosition(QPointF(1.2, 3.4));
    circle2->setVisible(false);
    circle2->setLocked(true);
    circle2->setXMirrored(true);
    circle2->setYMirrored(true);
    circle2->setOpacity(49.9);
    circle2->setRotation(12.5);
    circle2->setLineStyle(DashDotDotLine);
    circle2->setLineWidth(ThickestLine);
    circle2->setLineColor(Red);
    circle2->setFillStyle(NoFill);
    circle2->setFillColor(Violet);
    circle2->setRadius(3.14);
    symbol->drawingItems.append(circle2);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::circularArcProperties()
{
    QString expected("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                     "<symbol xmlns=\"http://www.leda.org/xdl\">"
                     "<name></name>"
                     "<label></label>"
                     "<drawing>"
                     "<circular-arc>"
                     "<position><x>0</x><y>0</y></position>"
                     "<visible>true</visible>"
                     "<locked>false</locked>"
                     "<x-mirrored>false</x-mirrored>"
                     "<y-mirrored>false</y-mirrored>"
                     "<opacity>100</opacity>"
                     "<rotation>0</rotation>"
                     "<line-style>Solid</line-style>"
                     "<line-width>Medium</line-width>"
                     "<line-color>SecondaryContent</line-color>"
                     "<fill-style>Solid</fill-style>"
                     "<fill-color>BackgroundHighlight</fill-color>"
                     "<radius>0</radius>"
                     "<start-angle>0</start-angle>"
                     "<span-angle>360</span-angle>"
                     "</circular-arc>"
                     "<circular-arc>"
                     "<position><x>1.2</x><y>3.4</y></position>"
                     "<visible>false</visible>"
                     "<locked>true</locked>"
                     "<x-mirrored>true</x-mirrored>"
                     "<y-mirrored>true</y-mirrored>"
                     "<opacity>49.9</opacity>"
                     "<rotation>12.5</rotation>"
                     "<line-style>DashDotDot</line-style>"
                     "<line-width>Thickest</line-width>"
                     "<line-color>Red</line-color>"
                     "<fill-style>None</fill-style>"
                     "<fill-color>Violet</fill-color>"
                     "<radius>3.14</radius>"
                     "<start-angle>9.999</start-angle>"
                     "<span-angle>359.999</span-angle>"
                     "</circular-arc>"
                     "</drawing>"
                     "</symbol>\n");

    auto symbol = new Symbol();

    auto arc1 = new CircularArcItem();
    symbol->drawingItems.append(arc1);

    auto arc2 = new CircularArcItem();
    arc2->setPosition(QPointF(1.2, 3.4));
    arc2->setVisible(false);
    arc2->setLocked(true);
    arc2->setXMirrored(true);
    arc2->setYMirrored(true);
    arc2->setOpacity(49.9);
    arc2->setRotation(12.5);
    arc2->setLineStyle(DashDotDotLine);
    arc2->setLineWidth(ThickestLine);
    arc2->setLineColor(Red);
    arc2->setFillStyle(NoFill);
    arc2->setFillColor(Violet);
    arc2->setRadius(3.14);
    arc2->setStartAngle(9.999);
    arc2->setSpanAngle(359.999);
    symbol->drawingItems.append(arc2);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::ellipseProperties()
{
    QString expected("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                     "<symbol xmlns=\"http://www.leda.org/xdl\">"
                     "<name></name>"
                     "<label></label>"
                     "<drawing>"
                     "<ellipse>"
                     "<position><x>0</x><y>0</y></position>"
                     "<visible>true</visible>"
                     "<locked>false</locked>"
                     "<x-mirrored>false</x-mirrored>"
                     "<y-mirrored>false</y-mirrored>"
                     "<opacity>100</opacity>"
                     "<rotation>0</rotation>"
                     "<line-style>Solid</line-style>"
                     "<line-width>Medium</line-width>"
                     "<line-color>SecondaryContent</line-color>"
                     "<fill-style>Solid</fill-style>"
                     "<fill-color>BackgroundHighlight</fill-color>"
                     "<x-radius>0</x-radius>"
                     "<y-radius>0</y-radius>"
                     "</ellipse>"
                     "<ellipse>"
                     "<position><x>1.2</x><y>3.4</y></position>"
                     "<visible>false</visible>"
                     "<locked>true</locked>"
                     "<x-mirrored>true</x-mirrored>"
                     "<y-mirrored>true</y-mirrored>"
                     "<opacity>49.9</opacity>"
                     "<rotation>12.5</rotation>"
                     "<line-style>DashDotDot</line-style>"
                     "<line-width>Thickest</line-width>"
                     "<line-color>Red</line-color>"
                     "<fill-style>None</fill-style>"
                     "<fill-color>Violet</fill-color>"
                     "<x-radius>3.14</x-radius>"
                     "<y-radius>6.28</y-radius>"
                     "</ellipse>"
                     "</drawing>"
                     "</symbol>\n");

    auto symbol = new Symbol();

    auto ellipse1 = new EllipseItem();
    symbol->drawingItems.append(ellipse1);

    auto ellipse2 = new EllipseItem();
    ellipse2->setPosition(QPointF(1.2, 3.4));
    ellipse2->setVisible(false);
    ellipse2->setLocked(true);
    ellipse2->setXMirrored(true);
    ellipse2->setYMirrored(true);
    ellipse2->setOpacity(49.9);
    ellipse2->setRotation(12.5);
    ellipse2->setLineStyle(DashDotDotLine);
    ellipse2->setLineWidth(ThickestLine);
    ellipse2->setLineColor(Red);
    ellipse2->setFillStyle(NoFill);
    ellipse2->setFillColor(Violet);
    ellipse2->setXRadius(3.14);
    ellipse2->setYRadius(6.28);
    symbol->drawingItems.append(ellipse2);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::ellipticalArcProperties()
{
    QString expected("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                     "<symbol xmlns=\"http://www.leda.org/xdl\">"
                     "<name></name>"
                     "<label></label>"
                     "<drawing>"
                     "<elliptical-arc>"
                     "<position><x>0</x><y>0</y></position>"
                     "<visible>true</visible>"
                     "<locked>false</locked>"
                     "<x-mirrored>false</x-mirrored>"
                     "<y-mirrored>false</y-mirrored>"
                     "<opacity>100</opacity>"
                     "<rotation>0</rotation>"
                     "<line-style>Solid</line-style>"
                     "<line-width>Medium</line-width>"
                     "<line-color>SecondaryContent</line-color>"
                     "<fill-style>Solid</fill-style>"
                     "<fill-color>BackgroundHighlight</fill-color>"
                     "<x-radius>0</x-radius>"
                     "<y-radius>0</y-radius>"
                     "<start-angle>0</start-angle>"
                     "<span-angle>360</span-angle>"
                     "</elliptical-arc>"
                     "<elliptical-arc>"
                     "<position><x>1.2</x><y>3.4</y></position>"
                     "<visible>false</visible>"
                     "<locked>true</locked>"
                     "<x-mirrored>true</x-mirrored>"
                     "<y-mirrored>true</y-mirrored>"
                     "<opacity>49.9</opacity>"
                     "<rotation>12.5</rotation>"
                     "<line-style>DashDotDot</line-style>"
                     "<line-width>Thickest</line-width>"
                     "<line-color>Red</line-color>"
                     "<fill-style>None</fill-style>"
                     "<fill-color>Violet</fill-color>"
                     "<x-radius>3.14</x-radius>"
                     "<y-radius>6.28</y-radius>"
                     "<start-angle>9.999</start-angle>"
                     "<span-angle>359.999</span-angle>"
                     "</elliptical-arc>"
                     "</drawing>"
                     "</symbol>\n");

    auto symbol = new Symbol();

    auto arc1 = new EllipticalArcItem();
    symbol->drawingItems.append(arc1);

    auto arc2 = new EllipticalArcItem();
    arc2->setPosition(QPointF(1.2, 3.4));
    arc2->setVisible(false);
    arc2->setLocked(true);
    arc2->setXMirrored(true);
    arc2->setYMirrored(true);
    arc2->setOpacity(49.9);
    arc2->setRotation(12.5);
    arc2->setLineStyle(DashDotDotLine);
    arc2->setLineWidth(ThickestLine);
    arc2->setLineColor(Red);
    arc2->setFillStyle(NoFill);
    arc2->setFillColor(Violet);
    arc2->setXRadius(3.14);
    arc2->setYRadius(6.28);
    arc2->setStartAngle(9.999);
    arc2->setSpanAngle(359.999);
    symbol->drawingItems.append(arc2);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::rectangleProperties()
{
    QString expected("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                     "<symbol xmlns=\"http://www.leda.org/xdl\">"
                     "<name></name>"
                     "<label></label>"
                     "<drawing>"
                     "<rectangle>"
                     "<position><x>0</x><y>0</y></position>"
                     "<visible>true</visible>"
                     "<locked>false</locked>"
                     "<x-mirrored>false</x-mirrored>"
                     "<y-mirrored>false</y-mirrored>"
                     "<opacity>100</opacity>"
                     "<rotation>0</rotation>"
                     "<line-style>Solid</line-style>"
                     "<line-width>Medium</line-width>"
                     "<line-color>SecondaryContent</line-color>"
                     "<fill-style>Solid</fill-style>"
                     "<fill-color>BackgroundHighlight</fill-color>"
                     "<width>0</width>"
                     "<height>0</height>"
                     "</rectangle>"
                     "<rectangle>"
                     "<position><x>1.2</x><y>3.4</y></position>"
                     "<visible>false</visible>"
                     "<locked>true</locked>"
                     "<x-mirrored>true</x-mirrored>"
                     "<y-mirrored>true</y-mirrored>"
                     "<opacity>49.9</opacity>"
                     "<rotation>12.5</rotation>"
                     "<line-style>DashDotDot</line-style>"
                     "<line-width>Thickest</line-width>"
                     "<line-color>Red</line-color>"
                     "<fill-style>None</fill-style>"
                     "<fill-color>Violet</fill-color>"
                     "<width>11.11</width>"
                     "<height>9.99</height>"
                     "</rectangle>"
                     "</drawing>"
                     "</symbol>\n");

    auto symbol = new Symbol();

    auto rect1 = new RectangleItem();
    symbol->drawingItems.append(rect1);

    auto rect2 = new RectangleItem();
    rect2->setPosition(QPointF(1.2, 3.4));
    rect2->setVisible(false);
    rect2->setLocked(true);
    rect2->setXMirrored(true);
    rect2->setYMirrored(true);
    rect2->setOpacity(49.9);
    rect2->setRotation(12.5);
    rect2->setLineStyle(DashDotDotLine);
    rect2->setLineWidth(ThickestLine);
    rect2->setLineColor(Red);
    rect2->setFillStyle(NoFill);
    rect2->setFillColor(Violet);
    rect2->setWidth(11.11);
    rect2->setHeight(9.99);
    symbol->drawingItems.append(rect2);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::polylineProperties()
{
    QString expected("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                     "<symbol xmlns=\"http://www.leda.org/xdl\">"
                     "<name></name>"
                     "<label></label>"
                     "<drawing>"
                     "<polyline>"
                     "<position><x>0</x><y>0</y></position>"
                     "<visible>true</visible>"
                     "<locked>false</locked>"
                     "<x-mirrored>false</x-mirrored>"
                     "<y-mirrored>false</y-mirrored>"
                     "<opacity>100</opacity>"
                     "<rotation>0</rotation>"
                     "<line-style>Solid</line-style>"
                     "<line-width>Medium</line-width>"
                     "<line-color>SecondaryContent</line-color>"
                     "<fill-style>Solid</fill-style>"
                     "<fill-color>BackgroundHighlight</fill-color>"
                     "<vertices/>"
                     "</polyline>"
                     "<polyline>"
                     "<position><x>1.2</x><y>3.4</y></position>"
                     "<visible>false</visible>"
                     "<locked>true</locked>"
                     "<x-mirrored>true</x-mirrored>"
                     "<y-mirrored>true</y-mirrored>"
                     "<opacity>49.9</opacity>"
                     "<rotation>12.5</rotation>"
                     "<line-style>DashDotDot</line-style>"
                     "<line-width>Thickest</line-width>"
                     "<line-color>Red</line-color>"
                     "<fill-style>None</fill-style>"
                     "<fill-color>Violet</fill-color>"
                     "<vertices>"
                     "<point>"
                     "<x>100</x>"
                     "<y>10</y>"
                     "</point>"
                     "<point>"
                     "<x>100</x>"
                     "<y>20</y>"
                     "</point>"
                     "<point>"
                     "<x>200</x>"
                     "<y>20</y>"
                     "</point>"
                     "</vertices>"
                     "</polyline>"
                     "</drawing>"
                     "</symbol>\n");

    auto symbol = new Symbol();

    auto line1 = new PolylineItem();
    symbol->drawingItems.append(line1);

    auto line2 = new PolylineItem();
    line2->setPosition(QPointF(1.2, 3.4));
    line2->setVisible(false);
    line2->setLocked(true);
    line2->setXMirrored(true);
    line2->setYMirrored(true);
    line2->setOpacity(49.9);
    line2->setRotation(12.5);
    line2->setLineStyle(DashDotDotLine);
    line2->setLineWidth(ThickestLine);
    line2->setLineColor(Red);
    line2->setFillStyle(NoFill);
    line2->setFillColor(Violet);
    QList<QPointF> vertices;
    vertices << QPointF(100, 10) << QPointF(100, 20) << QPointF(200, 20);
    line2->setVertices(vertices);
    symbol->drawingItems.append(line2);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::polygonProperties()
{
    QString expected("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                     "<symbol xmlns=\"http://www.leda.org/xdl\">"
                     "<name></name>"
                     "<label></label>"
                     "<drawing>"
                     "<polygon>"
                     "<position><x>0</x><y>0</y></position>"
                     "<visible>true</visible>"
                     "<locked>false</locked>"
                     "<x-mirrored>false</x-mirrored>"
                     "<y-mirrored>false</y-mirrored>"
                     "<opacity>100</opacity>"
                     "<rotation>0</rotation>"
                     "<line-style>Solid</line-style>"
                     "<line-width>Medium</line-width>"
                     "<line-color>SecondaryContent</line-color>"
                     "<fill-style>Solid</fill-style>"
                     "<fill-color>BackgroundHighlight</fill-color>"
                     "<vertices/>"
                     "</polygon>"
                     "<polygon>"
                     "<position><x>1.2</x><y>3.4</y></position>"
                     "<visible>false</visible>"
                     "<locked>true</locked>"
                     "<x-mirrored>true</x-mirrored>"
                     "<y-mirrored>true</y-mirrored>"
                     "<opacity>49.9</opacity>"
                     "<rotation>12.5</rotation>"
                     "<line-style>DashDotDot</line-style>"
                     "<line-width>Thickest</line-width>"
                     "<line-color>Red</line-color>"
                     "<fill-style>None</fill-style>"
                     "<fill-color>Violet</fill-color>"
                     "<vertices>"
                     "<point>"
                     "<x>100</x>"
                     "<y>10</y>"
                     "</point>"
                     "<point>"
                     "<x>100</x>"
                     "<y>20</y>"
                     "</point>"
                     "<point>"
                     "<x>200</x>"
                     "<y>20</y>"
                     "</point>"
                     "</vertices>"
                     "</polygon>"
                     "</drawing>"
                     "</symbol>\n");

    auto symbol = new Symbol();

    auto poly1 = new PolygonItem();
    symbol->drawingItems.append(poly1);

    auto poly2 = new PolygonItem();
    poly2->setPosition(QPointF(1.2, 3.4));
    poly2->setVisible(false);
    poly2->setLocked(true);
    poly2->setXMirrored(true);
    poly2->setYMirrored(true);
    poly2->setOpacity(49.9);
    poly2->setRotation(12.5);
    poly2->setLineStyle(DashDotDotLine);
    poly2->setLineWidth(ThickestLine);
    poly2->setLineColor(Red);
    poly2->setFillStyle(NoFill);
    poly2->setFillColor(Violet);
    QList<QPointF> vertices;
    vertices << QPointF(100, 10) << QPointF(100, 20) << QPointF(200, 20);
    poly2->setVertices(vertices);
    symbol->drawingItems.append(poly2);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::pinProperties()
{

}

void tst_Writer::groupProperties()
{

}

void tst_Writer::labelProperties()
{

}

void tst_Writer::lineStyleProperty_data()
{
    QTest::addColumn<LineStyle>("style");
    QTest::addColumn<QString>("text");

    QTest::newRow("No line") << NoLine << "None";
    QTest::newRow("Solid line") << SolidLine << "Solid";
    QTest::newRow("Dash line") << DashLine << "Dash";
    QTest::newRow("Dot line") << DotLine << "Dot";
    QTest::newRow("Dash-dot line") << DashDotLine << "DashDot";
    QTest::newRow("Dash-dot-dot line") << DashDotDotLine << "DashDotDot";

}

void tst_Writer::lineStyleProperty()
{
    QFETCH(LineStyle, style);
    QFETCH(QString, text);

    QString expected = QString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                               "<symbol xmlns=\"http://www.leda.org/xdl\">"
                               "<name></name>"
                               "<label></label>"
                               "<drawing>"
                               "<circle>"
                               "<position><x>0</x><y>0</y></position>"
                               "<visible>true</visible>"
                               "<locked>false</locked>"
                               "<x-mirrored>false</x-mirrored>"
                               "<y-mirrored>false</y-mirrored>"
                               "<opacity>100</opacity>"
                               "<rotation>0</rotation>"
                               "<line-style>%1</line-style>"
                               "<line-width>Medium</line-width>"
                               "<line-color>SecondaryContent</line-color>"
                               "<fill-style>Solid</fill-style>"
                               "<fill-color>BackgroundHighlight</fill-color>"
                               "<radius>0</radius>"
                               "</circle>"
                               "</drawing>"
                               "</symbol>\n"
                               ).arg(text);

    auto symbol = new Symbol();

    auto circle = new CircleItem();
    circle->setLineStyle(style);
    symbol->drawingItems.append(circle);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::lineWidthProperty_data()
{
    QTest::addColumn<LineWidth>("width");
    QTest::addColumn<QString>("text");

    QTest::newRow("Thinest line") << ThinestLine << "Thinest";
    QTest::newRow("Thiner line") << ThinerLine << "Thiner";
    QTest::newRow("Thin line") << ThinLine << "Thin";
    QTest::newRow("Slightly thin line") << SlightlyThinLine << "SlightlyThin";
    QTest::newRow("Medium line") << MediumLine << "Medium";
    QTest::newRow("Slightly thick line") << SlightlyThickLine << "SlightlyThick";
    QTest::newRow("Thick line") << ThickLine << "Thick";
    QTest::newRow("Thicker line") << ThickerLine << "Thicker";
    QTest::newRow("Thickest line") << ThickestLine << "Thickest";
}

void tst_Writer::lineWidthProperty()
{
    QFETCH(LineWidth, width);
    QFETCH(QString, text);

    QString expected = QString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                               "<symbol xmlns=\"http://www.leda.org/xdl\">"
                               "<name></name>"
                               "<label></label>"
                               "<drawing>"
                               "<circle>"
                               "<position><x>0</x><y>0</y></position>"
                               "<visible>true</visible>"
                               "<locked>false</locked>"
                               "<x-mirrored>false</x-mirrored>"
                               "<y-mirrored>false</y-mirrored>"
                               "<opacity>100</opacity>"
                               "<rotation>0</rotation>"
                               "<line-style>Solid</line-style>"
                               "<line-width>%1</line-width>"
                               "<line-color>SecondaryContent</line-color>"
                               "<fill-style>Solid</fill-style>"
                               "<fill-color>BackgroundHighlight</fill-color>"
                               "<radius>0</radius>"
                               "</circle>"
                               "</drawing>"
                               "</symbol>\n"
                               ).arg(text);

    auto symbol = new Symbol();

    auto circle = new CircleItem();
    circle->setLineWidth(width);
    symbol->drawingItems.append(circle);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::fillStyleProperty_data()
{
    QTest::addColumn<FillStyle>("style");
    QTest::addColumn<QString>("text");

    QTest::newRow("No fill") << NoFill << "None";
    QTest::newRow("Solid fill") << SolidFill << "Solid";
}

void tst_Writer::fillStyleProperty()
{
    QFETCH(FillStyle, style);
    QFETCH(QString, text);

    QString expected = QString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                               "<symbol xmlns=\"http://www.leda.org/xdl\">"
                               "<name></name>"
                               "<label></label>"
                               "<drawing>"
                               "<circle>"
                               "<position><x>0</x><y>0</y></position>"
                               "<visible>true</visible>"
                               "<locked>false</locked>"
                               "<x-mirrored>false</x-mirrored>"
                               "<y-mirrored>false</y-mirrored>"
                               "<opacity>100</opacity>"
                               "<rotation>0</rotation>"
                               "<line-style>Solid</line-style>"
                               "<line-width>Medium</line-width>"
                               "<line-color>SecondaryContent</line-color>"
                               "<fill-style>%1</fill-style>"
                               "<fill-color>BackgroundHighlight</fill-color>"
                               "<radius>0</radius>"
                               "</circle>"
                               "</drawing>"
                               "</symbol>\n"
                               ).arg(text);

    auto symbol = new Symbol();

    auto circle = new CircleItem();
    circle->setFillStyle(style);
    symbol->drawingItems.append(circle);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

void tst_Writer::colorProperty_data()
{
    QTest::addColumn<Color>("color");
    QTest::addColumn<QString>("text");

    QTest::newRow("Emphasised content") << EmphasisedContent << "EmphasisedContent";
    QTest::newRow("Primary content") << PrimaryContent << "PrimaryContent";
    QTest::newRow("Secondary content") << SecondaryContent << "SecondaryContent";
    QTest::newRow("Background") << Background << "Background";
    QTest::newRow("Background highlight") << BackgroundHighlight << "BackgroundHighlight";
    QTest::newRow("Yellow") << Yellow << "Yellow";
    QTest::newRow("Orange") << Orange << "Orange";
    QTest::newRow("Red") << Red << "Red";
    QTest::newRow("Magenta") << Magenta << "Magenta";
    QTest::newRow("Violet") << Violet << "Violet";
    QTest::newRow("Blue") << Blue << "Blue";
    QTest::newRow("Cyan") << Cyan << "Cyan";
    QTest::newRow("Green") << Green << "Green";
}

void tst_Writer::colorProperty()
{
    QFETCH(Color, color);
    QFETCH(QString, text);

    QString expected = QString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                               "<symbol xmlns=\"http://www.leda.org/xdl\">"
                               "<name></name>"
                               "<label></label>"
                               "<drawing>"
                               "<circle>"
                               "<position><x>0</x><y>0</y></position>"
                               "<visible>true</visible>"
                               "<locked>false</locked>"
                               "<x-mirrored>false</x-mirrored>"
                               "<y-mirrored>false</y-mirrored>"
                               "<opacity>100</opacity>"
                               "<rotation>0</rotation>"
                               "<line-style>Solid</line-style>"
                               "<line-width>Medium</line-width>"
                               "<line-color>%1</line-color>"
                               "<fill-style>Solid</fill-style>"
                               "<fill-color>%1</fill-color>"
                               "<radius>0</radius>"
                               "</circle>"
                               "</drawing>"
                               "</symbol>\n"
                               ).arg(text);

    auto symbol = new Symbol();

    auto circle = new CircleItem();
    circle->setLineColor(color);
    circle->setFillColor(color);
    symbol->drawingItems.append(circle);

    Writer writer;
    writer.setAutoFormating(false);

    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);
    QCOMPARE(writer.write(&buffer, symbol), true);

    QCOMPARE(QString::fromUtf8(buffer.data()), expected);
}

QTEST_MAIN(tst_Writer)

#include "tst_writer.moc"
