import qbs

LedaAutotest {
    name: "Autotests.SymbolEditor.XdlSymbolReader"

    Depends { name: "Xdl" }
    Depends { name: "Qt"; submodules: ["gui"] }

    files: [
        "tst_reader.cpp",
    ]
}
