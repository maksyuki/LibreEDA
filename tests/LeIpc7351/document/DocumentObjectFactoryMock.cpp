#include "DocumentObjectFactoryMock.h"

#include "DocumentObjectMock.h"

DocumentObjectFactoryMock::DocumentObjectFactoryMock()
    : LDO::IDocumentObjectFactory()
{

}


QList<int> DocumentObjectFactoryMock::objectTypeIds() const
{
    QList<int> result;
    result << DocumentObjectMock0::Type;
    result << DocumentObjectMock1::Type;
    result << DocumentObjectMock2::Type;
    result << DocumentObjectMock3::Type;
    result << DocumentObjectMock4::Type;
    return result;
}

LDO::IDocumentObject *DocumentObjectFactoryMock::createObject(int typeId) const
{    switch (typeId)
    {
        case DocumentObjectMock0::Type: return new DocumentObjectMock0;
        case DocumentObjectMock1::Type: return new DocumentObjectMock1;
        case DocumentObjectMock2::Type: return new DocumentObjectMock2;
        case DocumentObjectMock3::Type: return new DocumentObjectMock3;
        case DocumentObjectMock4::Type: return new DocumentObjectMock4;
        default: return nullptr;
    }
}

QString DocumentObjectFactoryMock::typeName(int typeId) const
{
    switch (typeId)
    {
        case DocumentObjectMock0::Type: return "DocumentObjectMock0";
        case DocumentObjectMock1::Type: return "DocumentObjectMock1";
        case DocumentObjectMock2::Type: return "DocumentObjectMock2";
        case DocumentObjectMock3::Type: return "DocumentObjectMock3";
        case DocumentObjectMock4::Type: return "DocumentObjectMock4";
        default: return QString();
    }
}
