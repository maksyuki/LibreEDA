import qbs

LedaAutotest {
    name: "Autotests.LeIpc7351.Document"

    //property path pluginDir: project.leda_source_path + "/src/plugins/symboleditor"

    //cpp.includePaths: base.concat(pluginDir)

    Depends { name: "Qt.widgets" }
    Depends { name: "Core" }
    Depends { name: "LeIpc7351" }
    Depends { name: "LeDocumentObject" }

    files: [
    "DocumentObjectFactoryMock.cpp",
    "DocumentObjectFactoryMock.h",
    "DocumentObjectMock.cpp",
    "DocumentObjectMock.h",
    "main.cpp",
    "TestDocument.cpp",
    "TestDocument.h",
    "TestDocumentObject.cpp",
    "TestDocumentObject.h",
    "TestDocumentObjectListener.cpp",
    "TestDocumentObjectListener.h",
    "TestDocumentStreamReader.cpp",
    "TestDocumentStreamReader.h",
    "TestDocumentStreamWriter.cpp",
    "TestDocumentStreamWriter.h",
    "TestPadStack.cpp",
    "TestPadStack.h",
    ]

}
