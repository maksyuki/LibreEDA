#pragma once

#include "LeDocumentObject/IDocumentObjectFactory.h"


class DocumentObjectFactoryMock: public LDO::IDocumentObjectFactory
{
    Q_OBJECT

public:
    DocumentObjectFactoryMock();

    // IDocumentObjectFactory interface
public:
    virtual QList<int> objectTypeIds() const override;
    virtual LDO::IDocumentObject *createObject(int typeId) const override;
    virtual QString typeName(int typeId) const override;
};
