import qbs

LedaProduct {
    type: ["application"]
    name: "LibreEDA"
    targetName: project.leda_app_target
    version: project.leda_version

    installDir: project.leda_bin_path

    Depends { name: "Qt"; submodules: ["core", "widgets", "network", "printsupport"] }
    Depends { name: "Utils" }
    Depends { name: "Core" }
    Depends { name: "Data" }

    // FIXME: should rely on leda_library_path (LibreEDA.qbs)
    cpp.rpaths: qbs.targetOS.contains("osx") ? ["@executable_path/../Frameworks"] : ["$ORIGIN/../lib/leda", "$ORIGIN/../lib/Qt/lib"]

    //cpp.defines: base.concat(["RELATIVE_PLUGIN_PATH=\" + "\""])

    files : [
        "aboutdialog.cpp",
        "aboutdialog.h",
        "libreeda.qrc",
        "main.cpp",
        "mainwindow.cpp",
        "mainwindow.h",
    ]
}
