#pragma once

#include <QObject>

class QMainWindow;
class QWidget;

namespace Ipc2581b
{
    class Ipc2581;
}

enum class LevelOfDetails
{
    High = 0,
    Medium,
    Low
};
Q_DECLARE_METATYPE(LevelOfDetails)

class IViewer: public QObject
{
    Q_OBJECT

public:
    explicit IViewer(QObject *parent = nullptr);
    virtual ~IViewer();

    virtual QWidget *centralWidget() const = 0;
    virtual void activate(QMainWindow *mainWindow) = 0;
    virtual void desactivate(QMainWindow *mainWindow) = 0;

    virtual void setLevelOfDetails(LevelOfDetails lod) = 0;
    virtual void enableAntiAlias(bool enabled) = 0;
    virtual void enableTransparentLayers(bool enabled) = 0;
    virtual void enableDropShadowEffect(bool enabled) = 0;
    virtual void enableOpenGL(bool enabled) = 0;
};
