#ifndef BRUSHRENDERFRAME_H
#define BRUSHRENDERFRAME_H

#include <QFrame>

class BrushRenderFrame : public QFrame
{
    Q_OBJECT
    Q_PROPERTY(QBrush brush READ brush WRITE setBrush NOTIFY brushChanged)

public:
    explicit BrushRenderFrame(QWidget *parent = nullptr);
    ~BrushRenderFrame();

    QBrush brush() const;

public slots:
    void setBrush(QBrush brush);

signals:
    void brushChanged(QBrush brush);

private:
    QBrush m_brush;

    // QWidget interface
protected:
    virtual void paintEvent(QPaintEvent *event) override;
};

#endif // BRUSHRENDERFRAME_H
