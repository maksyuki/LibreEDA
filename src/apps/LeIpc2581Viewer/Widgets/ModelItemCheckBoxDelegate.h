#ifndef MODELITEMCHECKBOXDELEGATE_H
#define MODELITEMCHECKBOXDELEGATE_H

#include <QStyledItemDelegate>

class ModelItemCheckBoxDelegate : public QStyledItemDelegate
{
public:
    explicit ModelItemCheckBoxDelegate(QObject *parent = 0);

private slots:
    void commitAndCloseEditor();

    // QAbstractItemDelegate interface
public:
    virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    virtual bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;
};

#endif // MODELITEMCHECKBOXDELEGATE_H
