#ifndef LAYERVIEWER_H
#define LAYERVIEWER_H

#include "iviewer.h"

#include <QMap>
#include <QList>

class QAbstractItemModel;
class QAbstractItemView;
class QAction;
class QActionGroup;
class QDockWidget;
class QGraphicsItemGroup;
class QGraphicsPathItem;
class QSettings;
class QToolBar;
class QTableView;
class QTreeView;

class QtAbstractPropertyBrowser;

namespace Ipc2581b
{
    class Ipc2581;
    class Step;
}

class Document;
class GraphicsItemFactory;
class LeGraphicsScene;
class LeGraphicsView;
class LayerListWidget;

class ComponentListModel;
class FeatureSetListModel;
class HoleListModel;
class LayerListModel;
class LayerSetModel;
class LayerStackupModel;
class NetListModel;
class PackageListModel;
class PadListModel;

class LayerViewer : public IViewer
{
    Q_OBJECT
public:
    explicit LayerViewer(QObject *parent = nullptr);

    QString errorMessage() const;
signals:

public slots:
    bool openFile(const QString &fileName);

private:
    void setupScenePalette();
    void createActions();

    void loadStepProfile();
    void loadDesignDictionaries();
    void loadLayerSet();
    void loadLayerFeatures();
    void setupGraphicsScene();
    void setupGraphicsView();

    Document *m_document;

    LeGraphicsScene *m_graphicsScene;
    LeGraphicsView *m_graphicsView;
    GraphicsItemFactory *m_graphicsItemFactory;

    LayerSetModel *m_layerSetModel;
    QTableView *m_layerSetView;
    LayerListWidget *m_layerSetWidget;
    QDockWidget *m_layerSetDockWidget;

    LayerListModel *m_layerListModel;
    QTableView *m_layerListView;
    QDockWidget *m_layerListDockWidget;

    ComponentListModel *m_componentListModel;
    QTableView *m_componentListView;
    QDockWidget *m_componentListDockWidget;

    PackageListModel *m_packageListModel;
    QTableView *m_packageListView;
    QDockWidget *m_packageListDockWidget;

    NetListModel *m_netListModel;
    QTableView *m_netListView;
    QDockWidget *m_netListDockWidget;

    PadListModel *m_padListModel;
    QTableView *m_padListView;
    QDockWidget *m_padListDockWidget;

    HoleListModel *m_holeListModel;
    QTableView *m_holeListView;
    QDockWidget *m_holeListDockWidget;

    FeatureSetListModel *m_featureSetListModel;
    QTableView  *m_featureSetListView;
    QDockWidget *m_featureSetListDockWidget;

    LayerStackupModel *m_stackupModel;
    QTreeView  *m_stackupView;
    QDockWidget *m_stackupDockWidget;

    QtAbstractPropertyBrowser *m_propertyBrowser;
    QDockWidget *m_propertyBrowserDockWidget;

    bool m_navigationAutoFitView;
    bool m_navigationAutoShowLayer;
    bool m_navigationAutoSelectLayer;

    // Helpers
    template <typename Func>
    QAction *createAction(const QString &text, const QString &shortcut,
                          const QObject *receiver, Func slot);
    QActionGroup *createActionGroup(const QList<QAction *> & actions);

    void setupTableViewDockWidget(const QString &title, QTableView *view, QDockWidget *dock);
    void setupTableView(QTableView *view, QAbstractItemModel *model);
    void setupTreeViewDockWidget(const QString &title, QTreeView *view, QDockWidget *dock);
    void setupTreeView(QTreeView *view, QAbstractItemModel *model);
    void saveItemViewState(QAbstractItemView *view, QSettings &settings);
    void restoreItemViewState(QAbstractItemView *view, QSettings &settings);

    // IViewer interface

public:
    virtual QWidget *centralWidget() const override;
    virtual void enableAntiAlias(bool enabled) override;
    virtual void enableTransparentLayers(bool enabled) override;
    virtual void enableDropShadowEffect(bool enabled) override;
    virtual void enableOpenGL(bool enabled) override;
    virtual void setLevelOfDetails(LevelOfDetails lod) override;
    virtual void activate(QMainWindow *mainWindow) override;
    virtual void desactivate(QMainWindow *mainWindow) override;
};

#endif // LAYERVIEWER_H
