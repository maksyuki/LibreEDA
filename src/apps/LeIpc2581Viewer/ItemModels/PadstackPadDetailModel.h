#pragma once

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class PadstackPadDef;
}

class PadstackPadDetailModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    PadstackPadDetailModel(QObject *parent = nullptr);

    void setPadstackPadDef(Ipc2581b::PadstackPadDef *padDef);

private:
    Ipc2581b::PadstackPadDef *m_padDef;

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};