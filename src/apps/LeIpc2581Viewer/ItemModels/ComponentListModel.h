#pragma once

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class Component;
}

class ComponentListModelPrivate;
class ComponentListModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(ComponentListModel)
    QScopedPointer<ComponentListModelPrivate> const d_ptr;

public:
    enum
    {
        NameColumn = 0,
        PackageRefColumn,
        PartRefColumn,
        LayerRefColumn,
        MountTypeColumn,
        WeightCoolumn,
        HeightColumn,
        StandOffColumn,
        TransformColumn,
        LocationColumn,
        ColumnCount
    };

    explicit ComponentListModel(QObject *parent = 0);
    ~ComponentListModel();

    void addComponent(const Ipc2581b::Component *component);
    const Ipc2581b::Component *component(const QModelIndex &index) const;

public slots:
    void clear();

    // QAbstractTableModel Interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

};

Q_DECLARE_METATYPE(ComponentListModel*)
