#include "PadstackPadDetailModel.h"
#include "enumtranslator.h"
#include "PadstackPadDef.h"

enum
{
    LayerRefRow = 0,
    PadUseRow,
    PadCommentRow,
    LocationRow,
    RowCount
};

enum
{
    PropertyNameColumn = 0,
    PropertyValueColumn,
    ColumnCount
};

PadstackPadDetailModel::PadstackPadDetailModel(QObject *parent):
    QAbstractTableModel(parent),
    m_padDef(nullptr)
{

}

void PadstackPadDetailModel::setPadstackPadDef(Ipc2581b::PadstackPadDef *padDef)
{
    beginResetModel();
    m_padDef = padDef;
    endResetModel();
}


int PadstackPadDetailModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (m_padDef != nullptr)
        return RowCount;
    return 0;
}

int PadstackPadDetailModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ColumnCount;
}

QVariant PadstackPadDetailModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    switch (index.column())
    {
        case PropertyNameColumn:
            switch (index.row())
            {
                case LayerRefRow:
                    return QStringLiteral("Layer");
                case PadUseRow:
                    return QStringLiteral("Pad use");
                case PadCommentRow:
                    return QStringLiteral("Comment");
                case LocationRow:
                    return QStringLiteral("Location");
                default:
                    return QVariant();
            }
        case PropertyValueColumn:
            switch (index.row())
            {
                case LayerRefRow:
                    return m_padDef->layerRef;
                case PadUseRow:
                    return Ipc2581b::EnumTranslator::padUseText(m_padDef->padUse);
                case PadCommentRow:
                    if (m_padDef->commentOptional.hasValue)
                        return m_padDef->commentOptional.value;
                    return QVariant();
                case LocationRow:
                    return QString("[%1, %2]").arg(m_padDef->location->x).arg(m_padDef->location->y);
                default:
                    return QVariant();
            }
        default:
            return QVariant();
    }
}

QVariant PadstackPadDetailModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal)
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    if (section == PropertyNameColumn)
        return QStringLiteral("Property");

    if (section == PropertyValueColumn)
        return QStringLiteral("Value");

    return QVariant();
}
