#pragma once

#include <QAbstractTableModel>

#include "LeIpc2581/PadUsage.h"

namespace Ipc2581b
{
    class Layer;
    class Set;
}

class LayerListModelPrivate;
class LayerListModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(LayerListModel)
    QScopedPointer<LayerListModelPrivate> const d_ptr;

public:
    enum
    {
        NameColumn = 0,
        FunctionColumn,
        SideColumn,
        PolarityColumn,
        ColumnCount
    };

    explicit LayerListModel(QObject *parent = 0);
    ~LayerListModel();

    void addLayer(const Ipc2581b::Layer *layer);
    void addFeatureSet(const QString &layerName, const Ipc2581b::Set *set);

    const Ipc2581b::Layer *layer(const QString &name) const;
    const Ipc2581b::Layer *layer(int row) const;
    QList<const Ipc2581b::Layer *> layers() const;
    QList<const Ipc2581b::Set *> featureSetList(int row);

public slots:
    void clear();

    // QAbstractTableModel Interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};
