#ifndef LAYERSTACKUPMODEL_H
#define LAYERSTACKUPMODEL_H

#include <QAbstractItemModel>

namespace Ipc2581b
{
    class Stackup;
}

class Document;

class LayerStackupModelPrivate;
class LayerStackupModel : public QAbstractItemModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(LayerStackupModel)
    QScopedPointer<LayerStackupModelPrivate> const d_ptr;

public:
    enum
    {
        NameColumn,
        ThicknessColumn,
        CommentColumn,
        BomItemRefColumn,
        ColumnCount
    };

    explicit LayerStackupModel(QObject *parent = 0);
    ~LayerStackupModel();

    void setIpcStackup(const Document *document, const Ipc2581b::Stackup *stackup);

    // QAbstractItemModel Interface
public:
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};

#endif // LAYERSTACKUPMODEL_H
