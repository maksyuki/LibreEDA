#ifndef PADSTACKDEFLISTMODEL_H
#define PADSTACKDEFLISTMODEL_H

#include <QAbstractListModel>

namespace Ipc2581b
{
    class PadstackDef;
}

class PadstackDefListModel : public QAbstractListModel
{
public:
    PadstackDefListModel(QObject *parent = nullptr);

    void setPadstackDefList(const QList<Ipc2581b::PadstackDef*> &list);
    Ipc2581b::PadstackDef *padstackDef(const QModelIndex &index) const;

private:
    QList<Ipc2581b::PadstackDef*> m_padstackDefList;

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
};

#endif // PADSTACKDEFLISTMODEL_H
