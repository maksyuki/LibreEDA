#include "PadUseListModel.h"

#include "enumtranslator.h"

class PadUseItem
{
public:
    PadUseItem(Ipc2581b::PadUse value, QString text, bool state):
        value(value), text(text), state(state)
    {}
    Ipc2581b::PadUse value;
    QString text;
    bool state;
};

PadUseListModel::PadUseListModel(QObject *parent):
    QAbstractListModel(parent)
{
    int row = 0;
    Ipc2581b::PadUse use = Ipc2581b::PadUse::Regular;
    m_padUseItemList.append(new PadUseItem(use,
                                       Ipc2581b::EnumTranslator::padUseText(use),
                                       false));
    m_padUseMap.insert(use, row);
    use = Ipc2581b::PadUse::Thermal;
    m_padUseItemList.append(new PadUseItem(use,
                                       Ipc2581b::EnumTranslator::padUseText(use),
                                       false));
    m_padUseMap.insert(use, row);
    use = Ipc2581b::PadUse::Antipad;
    m_padUseItemList.append(new PadUseItem(use,
                                       Ipc2581b::EnumTranslator::padUseText(use),
                                       false));
    m_padUseMap.insert(use, row);
    use = Ipc2581b::PadUse::Other;
    m_padUseItemList.append(new PadUseItem(use,
                                       Ipc2581b::EnumTranslator::padUseText(use),
                                       false));
    m_padUseMap.insert(use, row);
}

Ipc2581b::PadUse PadUseListModel::padUse(const QModelIndex &index) const
{
    return m_padUseItemList.value(index.row())->value;
}

void PadUseListModel::setAllStates(bool state)
{
    for (auto item: m_padUseItemList)
        setPadUseState(item->value, state);
}

void PadUseListModel::setPadUseState(Ipc2581b::PadUse use, bool state)
{
    setData(createIndex(m_padUseMap.value(use), 0), state ? Qt::Checked : Qt::Unchecked);
}

int PadUseListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_padUseItemList.count();
}

QVariant PadUseListModel::data(const QModelIndex &index, int role) const
{
    auto item = m_padUseItemList.value(index.row());

    if (role == Qt::DisplayRole)
        return item->text;

    if (role == Qt::CheckStateRole)
        return item->state ? Qt::Checked : Qt::Unchecked;

    return QVariant();

}

bool PadUseListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    if (index.column() != 0)
        return false;

    auto item = m_padUseItemList.value(index.row());
    switch (role)
    {
        case Qt::CheckStateRole:
            item->state = value.value<Qt::CheckState>() == Qt::Checked;
            emit padUseStateChanged(item->value, item->state);
            emit dataChanged(index, index);
            return true;
        default:
            return false;
    }
}

Qt::ItemFlags PadUseListModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractListModel::flags(index);
    return flags | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled;
}
