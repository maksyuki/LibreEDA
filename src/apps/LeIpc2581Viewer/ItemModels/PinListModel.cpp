#include "PinListModel.h"

#include "Pin.h"

enum
{
    PinNumberColumn = 0,
    PinNameColumn,
    ColumnCount
};

PinListModel::PinListModel(QObject *parent):
    QAbstractTableModel(parent)
{

}

void PinListModel::setPinList(const QList<Ipc2581b::Pin *> &list)
{
    beginResetModel();
    m_pinList = list;
    endResetModel();
}

Ipc2581b::Pin *PinListModel::pin(const QModelIndex &index)
{
    return m_pinList.value(index.row());
}

int PinListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_pinList.count();
}

int PinListModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ColumnCount;
}

QVariant PinListModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (index.column() == PinNumberColumn)
        return m_pinList.value(index.row())->number;

    if (index.column() == PinNameColumn)
    {
        auto pin = m_pinList.value(index.row());
        if (pin->nameOptional.hasValue)
            return pin->nameOptional.value;
        return QStringLiteral("<no name>");
    }

    return QVariant();
}

QVariant PinListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal)
        {
            if (section == PinNumberColumn)
                return QString("Pin Number"); // Pin ID
            if (section == PinNameColumn)
                return QString("Name");  // Text
        }
    }
    return QVariant();
}
