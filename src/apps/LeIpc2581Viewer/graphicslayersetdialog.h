#ifndef GRAPHICSLAYERSETDIALOG_H
#define GRAPHICSLAYERSETDIALOG_H

#include <QDialog>
#include <QPainterPath>

#include "LeGraphicsView/LeGraphicsItem.h"
#include "LeGraphicsView/LeGraphicsPalette.h"

class QDialogButtonBox;
class QDoubleSpinBox;
class QGroupBox;
class QTableView;
class QTabWidget;

class ColorComboBox;
class BrushStyleComboBox;
class PenStyleComboBox;

class LayerSetModel;
class GraphicsLayerSetCachingProxyModel;
class LeGraphicsScene;

class GraphicsLayerSetDialog: public QDialog
{
public:
    explicit GraphicsLayerSetDialog(QWidget *parent = nullptr);
    ~GraphicsLayerSetDialog();

    void setData(LayerSetModel *model, LeGraphicsScene *scene);

public slots:
    virtual void accept() override;
    virtual void reject() override;
    void reset();
    void apply();

private:
    QTabWidget *m_tabWidget;
    QTableView *m_view;
    QDialogButtonBox *m_buttonBox;

    LayerSetModel *m_model;
    LeGraphicsScene *m_scene;
    LeGraphicsPalette m_palette;

    ColorComboBox *m_backgroundColorComboBox;
    ColorComboBox *m_profileColorComboBox;

    ColorComboBox *m_highlightColorComboBox;
    ColorComboBox *m_selectionColorComboBox;

    ColorComboBox *m_originColorComboBox;
    ColorComboBox *m_handleColorComboBox;

    ColorComboBox *m_zoomBoxColorComboBox;
    ColorComboBox *m_selectionBoxColorComboBox;

    ColorComboBox *m_goodMarkColorComboBox;
    ColorComboBox *m_badMarkColorComboBox;

    void setApplyResetEnabled(bool enabled);
    QWidget *createLayerSetTab();
    QWidget *createOtherLayersTab();
    QWidget *createAdvancedTab();
    QDialogButtonBox *createButtonBox();

//    QGroupBox *createLayerGroupBox();
//    QGroupBox *createBackgroundGroupBox();
//    QGroupBox *createProfileGroupBox();
//    QGroupBox *createHighlightGroupBox();
//    QGroupBox *createSelectionGroupBox();
//    QGroupBox *createGoodMarkGroupBox();
//    QGroupBox *createBadMarkGroupBox();
//    QDoubleSpinBox *createOpacitySpinBox();
//    BrushStyleComboBox *createBrushStyleComboBox(GraphicsItemPalette *palette, GraphicsItemRole role);
//    BrushStyleComboBox *createBrushStyleComboBox(GraphicsItemPalette *palette, GraphicsItemState state);
//    ColorComboBox *createBrushColorComboBox(GraphicsItemPalette *palette, GraphicsItemRole role);
//    ColorComboBox *createBrushColorComboBox(GraphicsItemPalette *palette, GraphicsItemState state);
//    PenStyleComboBox *createPenStyleComboBox(GraphicsItemPalette *palette, GraphicsItemRole role);
//    PenStyleComboBox *createPenStyleComboBox(GraphicsItemPalette *palette, GraphicsItemState state);
//    ColorComboBox *createPenColorComboBox(GraphicsItemPalette *palette, GraphicsItemRole role);
//    ColorComboBox *createPenColorComboBox(GraphicsItemPalette *palette, GraphicsItemState state);

    // TODO: Could be taken from the model with as user data?
    static QPainterPath padPath();
    static QPainterPath pinPath();
    static QPainterPath fiducialPath();
    static QPainterPath holePath();
    static QPainterPath slotPath();
    static QPainterPath fillPath();
    static QPainterPath tracePath();
    static QPainterPath textPath();
    void restoreGuiState();
    void saveGuiState();
};

#endif // GRAPHICSLAYERSETDIALOG_H
