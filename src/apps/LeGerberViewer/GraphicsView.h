#pragma once

#include <QGraphicsView>

class GraphicsView : public QGraphicsView
{
public:
	GraphicsView(QWidget *parent = nullptr);

	// QWidget interface
protected:
	virtual void wheelEvent(QWheelEvent *event) override;
};

