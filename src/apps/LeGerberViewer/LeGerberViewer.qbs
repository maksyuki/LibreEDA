import qbs

LedaProduct {
    type: ["application"]
    name: "LeGerberViewer"
    targetName: "le-gerber-viewer"
    version: project.leda_version

    installDir: project.leda_bin_path

    Depends { name: "Qt"; submodules: ["core", "widgets", "network", "printsupport"] }
    Depends { name: "LeGerber" }
    Depends { name: "Data" }

    // FIXME: should rely on leda_library_path (LibreEDA.qbs)
    cpp.rpaths: qbs.targetOS.contains("osx") ? ["@executable_path/../Frameworks"] : ["$ORIGIN/../lib/leda", "$ORIGIN/../lib/Qt/lib"]

    cpp.includePaths: base.concat([sourceDirectory, buildDirectory])

    files : [
        "main.cpp",
        "MainWindow.cpp",
        "MainWindow.h",
        "MainWindow.ui",
        "GraphicsView.cpp",
        "GraphicsView.h"
    ]
}
