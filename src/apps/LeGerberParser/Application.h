#pragma once

#include <QApplication>
#include <QList>
#include <QFutureWatcher>


namespace LeGerber
{
	class Job;
}

class Application : public QApplication
{
	Q_OBJECT

public:
	Application(int &argc, char **argv);

	int runJobsConcurently(const QList<LeGerber::Job*> jobs);
	int runJobsSequentially(const QList<LeGerber::Job*> jobs);

private slots:
	void onProgressValueChanged();
	void onFinished();
	void onResultReadyAt(int index);
	void onResultsReadyAt(int beginIndex, int endIndex);

public:

	qint64 elapsedTime = 0;
	qint64 cpuTime = 0;
	qint64 readingTime = 0;
	qint64 processingTime = 0;
	qint64 parsingTime = 0;
	qint64 insertionTime = 0;
	qint64 printingTime = 0;
	qint64 savingTime = 0;
	qint64 fileCount = 0;
	qint64 warningCount = 0;
	qint64 errorCount = 0;
	qint64 byteCount = 0;


private:
	void warning(const QString &text);
	void error(const QString &text);
	void report(const QString &text);
	static LeGerber::Job *runJob(LeGerber::Job *job);
	QFuture<LeGerber::Job*> m_future;
	QFutureWatcher<LeGerber::Job*> *m_watcher;
	QList<LeGerber::Job*> m_jobs;
};
