import qbs

LedaProduct {
    type: ["application"]
    name: "LeGerberParser"
    targetName: "le-gerber-parser"
    version: project.leda_version

    installDir: project.leda_bin_path

    Depends { name: "Qt"; submodules: ["core", "widgets", "network", "printsupport", "concurrent"] }
    Depends { name: "LeGerber" }
    Depends { name: "Data" }

    // FIXME: should rely on leda_library_path (LibreEDA.qbs)
    cpp.rpaths: qbs.targetOS.contains("osx") ? ["@executable_path/../Frameworks"] : ["$ORIGIN/../lib/leda", "$ORIGIN/../lib/Qt/lib"]

    cpp.includePaths: base.concat([sourceDirectory, buildDirectory])

    files : [
        "Application.cpp",
        "Application.h",
        "main.cpp",
    ]
}
