#pragma once

#include <QWidget>
#include <QMap>

namespace LDO
{
    class Document;
    class DocumentTreeModel;
    class IDocumentObject;
    class ObjectPropertyBrowser;
}

class MainWindow;

class QTreeView;

class DocumentWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DocumentWidget(QWidget *parent = 0);

    void setDocument(MainWindow *gui, LDO::Document *document);

private:
    void executeItemContextMenu(const QPoint &pos);
    void editObject(LDO::IDocumentObject *object);
    void renameObject(LDO::IDocumentObject *object);
    void deleteObject(LDO::IDocumentObject *object);
    void cutObject(LDO::IDocumentObject *object);
    void copyObject(LDO::IDocumentObject *object);
    void pasteObject(LDO::IDocumentObject *object);
    void addPadStackObject();
    void addLandPatternObject();

private:
    MainWindow *m_gui;
    LDO::Document *m_document = nullptr;
    LDO::DocumentTreeModel *m_model = nullptr;
    QTreeView *m_view = nullptr;
    LDO::IDocumentObject *m_object = nullptr;
    LDO::ObjectPropertyBrowser *m_browser = nullptr;
};
