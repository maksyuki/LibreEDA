#pragma once

#include "PadStackTask.h"

#include "LeGraphicsView/LeGraphicsSceneEventFilter.h"

#include <QPointF>
#include <QLineF>

class CirclePrimitive;
class PadStackElement;

class LeGraphicsCircleItem;

class PlaceCircularPadTask: public PadStackTask, public LeGraphicsSceneEventFilter
{
    Q_OBJECT

    Q_PROPERTY(QPointF position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(qreal diameter READ diameter WRITE setDiameter NOTIFY diameterChanged)

public:
    explicit PlaceCircularPadTask(PadStackWidget *editor = nullptr);

    QPointF position() const;
    qreal diameter() const;

signals:
    void positionChanged(QPointF position);
    void diameterChanged(qreal diameter);

public slots:
    void setPosition(QPointF position);
    void setDiameter(qreal diameter);

private:
    QPointF m_position;
    qreal m_diameter = 0.0;
    PadStackElement *m_element = nullptr;
    CirclePrimitive *m_primitive = nullptr;
    LeGraphicsCircleItem *m_item = nullptr;
    QLineF m_radiusLine;

    // AbstractTask interface
public slots:
    virtual void start() override;
    virtual void accept() override;
    virtual void reject() override;

    // LeGraphicsSceneEventFilter interface
public:
    virtual void drawBackground(QPainter *painter, const QRectF &rect) override;
    virtual void drawForeground(QPainter *painter, const QRectF &rect) override;
    virtual bool keyPressEvent(QKeyEvent *event) override;
    virtual bool keyReleaseEvent(QKeyEvent *event) override;
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
};
