#include "PlaceRectangularPadTask.h"

#include "PadStackWidget.h"
#include "Gui.h"

#include "LeIpc7351/PadStackElement.h"
#include "LeIpc7351/Primitives/RectanglePrimitive.h"

#include "LeGraphicsView/LeGraphicsItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"
#include "LeGraphicsView/LeGraphicsStyle.h"
#include "LeGraphicsView/LeGraphicsPalette.h"

#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QRectF>

PlaceRectangularPadTask::PlaceRectangularPadTask(PadStackWidget *editor)
    : PadStackTask(editor)
    , LeGraphicsSceneEventFilter()
{
    setTile("Rectangular pad");
    setDescription("Place a rectangular pad");
    setIcon(Gui::icon("primitive-shape-rectangle"));
    setShortcut(QKeySequence("p,r"));
}


void PlaceRectangularPadTask::start()
{
    setSize(QSizeF());
    setPosition(QPointF());
    scene()->setEventFilter(this);
    emit started(this);
}

void PlaceRectangularPadTask::accept()
{
    if (m_item != nullptr)
    {
        scene()->currentLayer()->removeFromLayer(m_item);
        delete m_item;
        auto primitive = new RectanglePrimitive();
        primitive->setSize(m_size);
        primitive->setPosition(m_position);
        m_element->setPrimitive(primitive);
    }
    m_item = nullptr;
    scene()->setEventFilter(nullptr);
    emit accepted(this);
}

void PlaceRectangularPadTask::reject()
{
    if (m_item != nullptr)
    {
        scene()->currentLayer()->removeFromLayer(m_item);
        delete m_item;
    }
    m_item = nullptr;
    scene()->setEventFilter(nullptr);
    emit rejected(this);
}

void PlaceRectangularPadTask::setPosition(QPointF position)
{
    if (m_position == position)
        return;

    m_position = position;

    if (m_item != nullptr)
        m_item->setPos(m_position);

    emit positionChanged(position);
}

void PlaceRectangularPadTask::setSize(const QSizeF &diameter)
{
    if (m_size == diameter)
        return;

    m_size = diameter;

    if (m_item != nullptr)
        m_item->setSize(m_size);

    emit sizeChanged(diameter);
}

void PlaceRectangularPadTask::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(painter)
    Q_UNUSED(rect)
}

void PlaceRectangularPadTask::drawForeground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect)

    if (m_item == nullptr)
        return;

    painter->setBrush(Qt::NoBrush);
    painter->setPen(QPen(scene()->graphicsPalette().color(LeGraphicsPalette::Highlight), 0));
    painter->drawLine(m_radiusLine);
    painter->setPen(QPen(scene()->graphicsPalette().color(LeGraphicsPalette::Highlight), 0, Qt::DotLine));
    QRectF bRect = m_item->boundingRect();
    bRect.moveCenter(m_item->pos());
    painter->drawRect(bRect);
}

bool PlaceRectangularPadTask::keyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event)
    return true;
}

bool PlaceRectangularPadTask::keyReleaseEvent(QKeyEvent *event)
{
    Q_UNUSED(event)
    return true;
}

bool PlaceRectangularPadTask::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    return true;
}

bool PlaceRectangularPadTask::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_item == nullptr)
    {
        setPosition(event->scenePos());
        return true;
    }

    m_radiusLine.setP2(event->scenePos());
    setSize(QSizeF(qAbs(m_radiusLine.dx())*2.,
                   qAbs(m_radiusLine.dy())*2.0));
    return true;
}

bool PlaceRectangularPadTask::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_item == nullptr)
    {
        m_element = editor()->currentElement();
        m_item = new LeGraphicsRectItem(GftPad);
        m_item->setPos(event->scenePos());
        scene()->currentLayer()->addToLayer(m_item);
        m_radiusLine.setP1(event->scenePos());
        return true;
    }

    accept();
    return true;
}

bool PlaceRectangularPadTask::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    return true;
}

QPointF PlaceRectangularPadTask::position() const
{
    return m_position;
}

QSizeF PlaceRectangularPadTask::size() const
{
    return m_size;
}
