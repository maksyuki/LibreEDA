#include "PthPadCalculatorWidget.h"
#include "ui_PthPadCalculatorWidget.h"

#include "PthPadCalculator.h"

PthPadCalculatorWidget::PthPadCalculatorWidget(QWidget *parent) :
    QWidget(parent),
    m_ui(new Ui::PthPadCalculatorWidget),
    m_calculator(new PthPadCalculator(this))
{
    m_ui->setupUi(this);

    bind("leadThicknessMax", m_ui->maxLeadThickness, 0.5);
    bind("holeOverLead", m_ui->holeOverHead, 0.2);
    bind("padToHoleRatio", m_ui->padToHoleRatio, 1.5);
    bind("minimalAnularRing", m_ui->minAnnularRing, 0.2);
    bind("thermalIdOverHole", m_ui->thermalIdOverHole, 0.4);
    bind("minimalThermalOdOverId", m_ui->minThermalOdOverId, 0.3);
    bind("thermalOdToHoleRatio", m_ui->thermalOdToHoleRatio, 1.1);
    bind("spokeWidthToQuarterOdPercent", m_ui->spokeWidthPercent, 75);
    bind("roundOff", m_ui->roundOff, 0.01);

    m_calculator->calculate();
    showResult();
}

PthPadCalculatorWidget::~PthPadCalculatorWidget()
{
    delete m_ui;
}

void PthPadCalculatorWidget::bind(const QString &name, QDoubleSpinBox *spinBox, qreal value)
{
    spinBox->setValue(value);
    m_calculator->setProperty(name.toLocal8Bit().data(), QVariant(value));

    connect(spinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            [this, name](double value) {
        m_calculator->setProperty(name.toLocal8Bit().data(), QVariant(value));
        m_calculator->calculate();
        showResult();
    });
}

void PthPadCalculatorWidget::showResult()
{
    m_ui->holeDiameter->setValue(m_calculator->holeDiameter());
    m_ui->regularPadDiameter->setValue(m_calculator->regularPadDiameter());
    m_ui->thermalInnerDiameter->setValue(m_calculator->thermalPadInnderDiameter());
    m_ui->thermalOuterDiameter->setValue(m_calculator->thermalPadOuterDiameter());
    m_ui->thermalSpokeWidth->setValue(m_calculator->thermalPadSpokeWidth());
    m_ui->antiPadDiameter->setValue(m_calculator->antiPadDiameter());
}
