import qbs

LedaProduct {
    type: ["application"]
    name: "LePackageDesigner"
    targetName: "le-package-designer"
    version: project.leda_version

    installDir: project.leda_bin_path

    Depends { name: "Qt"; submodules: ["core", "widgets", "network", "printsupport"] }
    Depends { name: "LeIpc7351" }
    Depends { name: "LeDocumentObject" }
    Depends { name: "LeGraphicsView" }
    Depends { name: "QtPropertyBrowser" }
    Depends { name: "Data" }
    Depends { name: "Core" }

    // FIXME: should rely on leda_library_path (LibreEDA.qbs)
    cpp.rpaths: qbs.targetOS.contains("osx") ? ["@executable_path/../Frameworks"] : ["$ORIGIN/../lib/leda", "$ORIGIN/../lib/Qt/lib"]

    //cpp.defines: base.concat(["RELATIVE_PLUGIN_PATH=\" + "\""])
    cpp.includePaths: [sourceDirectory]

    files : [
        "AbstractTask.cpp",
        "AbstractTask.h",
        "DocumentWidget.cpp",
        "DocumentWidget.h",
        "EditorWidget.cpp",
        "EditorWidget.h",
        "GraphicsArcOfCircle.cpp",
        "GraphicsArcOfCircle.h",
        "GraphicsButterflyPrimitive.cpp",
        "GraphicsButterflyPrimitive.h",
        "GraphicsCircularShapeItem.cpp",
        "GraphicsCircularShapeItem.h",
        "GraphicsItems.cpp",
        "GraphicsItems.h",
        "GraphicsRectangularShapeItem.cpp",
        "GraphicsRectangularShapeItem.h",
        "GraphicsRegularPolygonPrimitive.cpp",
        "GraphicsRegularPolygonPrimitive.h",
        "GraphicsSceneInspector.cpp",
        "GraphicsSceneInspector.h",
        "GraphicsTrianglePrimitive.cpp",
        "GraphicsTrianglePrimitive.h",
        "Gui.h",
        "LePackageDesigner.qrc",
        "main.cpp",
        "MainWindow.cpp",
        "MainWindow.h",
        "PropertyManager.cpp",
        "PropertyManager.h",
        "TaskWidget.cpp",
        "TaskWidget.h",
        "LandPatternEditor/LandPatternWidget.cpp",
        "LandPatternEditor/LandPatternWidget.h",
        "LandPatternEditor/PlaceArrangementTask.cpp",
        "LandPatternEditor/PlaceArrangementTask.h",
        "LandPatternEditor/QuadInLineArrangementItem.cpp",
        "LandPatternEditor/QuadInLineArrangementItem.h",
        "LandPatternEditor/RenumberTerminalsTask.cpp",
        "LandPatternEditor/RenumberTerminalsTask.h",
        "PadStackEditor/GraphicsPadStackElementItem.cpp",
        "PadStackEditor/GraphicsPadStackElementItem.h",
        "PadStackEditor/PadStackTask.cpp",
        "PadStackEditor/PadStackTask.h",
        "PadStackEditor/PadStackWidget.cpp",
        "PadStackEditor/PadStackWidget.h",
        "PadStackEditor/PlaceArcOfCircle.cpp",
        "PadStackEditor/PlaceArcOfCircle.h",
        "PadStackEditor/PlaceButterflyPrimitive.cpp",
        "PadStackEditor/PlaceButterflyPrimitive.h",
        "PadStackEditor/PlaceCircularPadTask.cpp",
        "PadStackEditor/PlaceCircularPadTask.h",
        "PadStackEditor/PlaceRectangularPadTask.cpp",
        "PadStackEditor/PlaceRectangularPadTask.h",
        "PadStackEditor/PlaceRegularPolygonPrimitive.cpp",
        "PadStackEditor/PlaceRegularPolygonPrimitive.h",
        "PadStackEditor/PlaceTrianglePrimitive.cpp",
        "PadStackEditor/PlaceTrianglePrimitive.h",
        "PadStackEditor/PthPadCalculator.cpp",
        "PadStackEditor/PthPadCalculator.h",
        "PadStackEditor/PthPadCalculatorWidget.cpp",
        "PadStackEditor/PthPadCalculatorWidget.h",
        "PadStackEditor/PthPadCalculatorWidget.ui",
        "PadStackEditor/SmdPadCalculator.cpp",
        "PadStackEditor/SmdPadCalculator.h",
        "PadStackEditor/SmdPadCalculatorWidget.cpp",
        "PadStackEditor/SmdPadCalculatorWidget.h",
        "PadStackEditor/SmdPadCalculatorWidget.ui",
    ]
}
