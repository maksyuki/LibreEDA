#ifndef PLACEARRANGEMENTTASK_H
#define PLACEARRANGEMENTTASK_H

#include "AbstractTask.h"

#include "LeGraphicsView/LeGraphicsSceneEventFilter.h"

class QuadInLineArrangement;
class QuadInLineArrangementItem;

class PlaceArrangementTask : public AbstractTask, public LeGraphicsSceneEventFilter
{
    Q_OBJECT

    Q_PROPERTY(qreal vSpan READ vSpan WRITE setVSpan NOTIFY vSpanChanged)
    Q_PROPERTY(int vCount READ vCount WRITE setVCount NOTIFY vCountChanged)
    Q_PROPERTY(qreal vPitch READ vPitch WRITE setVPitch NOTIFY vPitchChanged)
    Q_PROPERTY(qreal hSpan READ hSpan WRITE setHSpan NOTIFY hSpanChanged)
    Q_PROPERTY(int hCount READ hCount WRITE setHCount NOTIFY hCountChanged)
    Q_PROPERTY(qreal hPitch READ hPitch WRITE setHPitch NOTIFY hPitchChanged)
    Q_PROPERTY(QString numberingPrefix READ numberingPrefix WRITE setNumberingPrefix NOTIFY numberingPrefixChanged)

public:
    explicit PlaceArrangementTask(QObject *parent = 0);

    qreal vSpan() const;
    int vCount() const;
    qreal vPitch() const;
    qreal hSpan() const;
    int hCount() const;
    qreal hPitch() const;
    QString numberingPrefix() const;

signals:
    void vSpanChanged(qreal vSpan);
    void vCountChanged(int vCount);
    void vPitchChanged(qreal vPitch);
    void hSpanChanged(qreal hSpan);
    void hCountChanged(int hCount);
    void hPitchChanged(qreal hPitch);
    void numberingPrefixChanged(QString numberingPrefix);

public slots:
    void setVSpan(qreal vSpan);
    void setVCount(int vCount);
    void setVPitch(qreal vPitch);
    void setHSpan(qreal hSpan);
    void setHCount(int hCount);
    void setHPitch(qreal hPitch);
    void setNumberingPrefix(QString numberingPrefix);

private:
    QuadInLineArrangement *m_arrangement = nullptr;
    QuadInLineArrangementItem *m_item = nullptr;
    QPointF m_lastMouseScenePos;
    qreal m_vSpan = 0;
    int m_vCount = 0;
    qreal m_vPitch = 0;
    qreal m_hSpan = 0;
    int m_hCount = 0;
    qreal m_hPitch = 0;
    QString m_numberingPrefix;

    //AbstractTask interface
public:
    virtual void start() override;
    virtual void accept() override;
    virtual void reject() override;

    // LeGraphicsSceneEventFilter interface
public:
    virtual void drawBackground(QPainter *painter, const QRectF &rect) override;
    virtual void drawForeground(QPainter *painter, const QRectF &rect) override;
    virtual bool keyPressEvent(QKeyEvent *event) override;
    virtual bool keyReleaseEvent(QKeyEvent *event) override;
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
};

#endif // PLACEARRANGEMENTTASK_H
