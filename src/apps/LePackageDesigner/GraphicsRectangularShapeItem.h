#pragma once

#include "LeGraphicsView/LeGraphicsItem.h"

#include "LeDocumentObject/IDocumentObjectListener.h"

class LeGraphicsHandleItem;

class RectanglePrimitive;

class GraphicsRectangleShapeItem: public LeGraphicsItem, public LDO::IDocumentObjectListener
{
    Q_OBJECT

public:
    enum
    {
        Type = UserType + 0x1003
    };

    explicit GraphicsRectangleShapeItem(RectanglePrimitive *primitive);
    ~GraphicsRectangleShapeItem();
    RectanglePrimitive *primitive() const;

private:
    void updateGeometry();
    void addHandles();

private:
    RectanglePrimitive *m_primitive;

    // QGraphicsItem interface
public:
    virtual int type() const override { return Type; }
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // LeGraphicsItem interface
public:
    virtual LeGraphicsItem *clone() const override;
protected:
    virtual QPointF handlePositionChangeFilter(LeGraphicsHandleRole role, const QPointF &value) override;

    // IDocumentObjectListener interface
public:
    virtual void documentObjectAboutToChangeProperty(const LDO::IDocumentObject *object, const QString &name, const QVariant &oldValue) override;
    virtual void documentObjectPropertyChanged(const LDO::IDocumentObject *object, const QString &name, const QVariant &newValue) override;
};
