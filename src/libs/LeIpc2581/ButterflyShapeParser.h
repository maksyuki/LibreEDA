
#pragma once

#include <QString>

class QXmlStreamReader;

#include "ButterflyShape.h"

namespace Ipc2581b
{

class ButterflyShapeParser
{
public:
    ButterflyShapeParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    ButterflyShape result();

private:
    ButterflyShape m_result;
};

}