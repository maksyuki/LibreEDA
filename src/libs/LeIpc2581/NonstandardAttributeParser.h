
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "NonstandardAttribute.h"

namespace Ipc2581b
{

class CadPropertyParser;
class QStringParser;

class NonstandardAttributeParser
{
public:
    NonstandardAttributeParser(
            QStringParser*&
            , CadPropertyParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    NonstandardAttribute *result();

private:
    QStringParser *&m_nameParser;
    CadPropertyParser *&m_typeParser;
    QStringParser *&m_valueParser;
    QScopedPointer<NonstandardAttribute> m_result;
};

}