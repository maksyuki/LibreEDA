
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Layer.h"

namespace Ipc2581b
{

class IdRefParser;
class LayerFunctionParser;
class SideParser;
class PolarityParser;
class QStringParser;

class LayerParser
{
public:
    LayerParser(
            QStringParser*&
            , LayerFunctionParser*&
            , SideParser*&
            , PolarityParser*&
            , IdRefParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Layer *result();

private:
    QStringParser *&m_nameParser;
    LayerFunctionParser *&m_layerFunctionParser;
    SideParser *&m_sideParser;
    PolarityParser *&m_polarityParser;
    IdRefParser *&m_specRefParser;
    QScopedPointer<Layer> m_result;
};

}