#include "PropertyUnitParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

PropertyUnitParser::PropertyUnitParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool PropertyUnitParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, PropertyUnit> map =
    {
        {QStringLiteral("FARANHEIT"), PropertyUnit::Faranheit},
        {QStringLiteral("SECTION"), PropertyUnit::Section},
        {QStringLiteral("DEGREES"), PropertyUnit::Degrees},
        {QStringLiteral("Hz"), PropertyUnit::Hz},
        {QStringLiteral("CLASS"), PropertyUnit::Class},
        {QStringLiteral("OHMS"), PropertyUnit::Ohms},
        {QStringLiteral("OHM"), PropertyUnit::Ohm},
        {QStringLiteral("MHO/CM"), PropertyUnit::MhoPerCm},
        {QStringLiteral("INCH"), PropertyUnit::Inch},
        {QStringLiteral("PERCENT"), PropertyUnit::Percent},
        {QStringLiteral("RMS"), PropertyUnit::Rms},
        {QStringLiteral("SIEMENS/M"), PropertyUnit::SiemensPerM},
        {QStringLiteral("MICRON"), PropertyUnit::Micron},
        {QStringLiteral("OTHER"), PropertyUnit::Other},
        {QStringLiteral("GAUGE"), PropertyUnit::Gauge},
        {QStringLiteral("ITEM"), PropertyUnit::Item},
        {QStringLiteral("RZ"), PropertyUnit::Rz},
        {QStringLiteral("RMAX"), PropertyUnit::Rmax},
        {QStringLiteral("CELCIUS"), PropertyUnit::Celcius},
        {QStringLiteral("MM"), PropertyUnit::Mm},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum PropertyUnit").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

PropertyUnit PropertyUnitParser::result()
{
    return m_result;
}

}