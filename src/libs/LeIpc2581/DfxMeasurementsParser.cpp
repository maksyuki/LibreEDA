#include "DfxMeasurementsParser.h"


#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

DfxMeasurementsParser::DfxMeasurementsParser(
){

}

bool DfxMeasurementsParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new DfxMeasurements());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

DfxMeasurements *DfxMeasurementsParser::result()
{
    return m_result.take();
}

}