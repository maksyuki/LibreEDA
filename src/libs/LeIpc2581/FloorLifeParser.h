
#pragma once

#include <QString>

class QXmlStreamReader;

#include "FloorLife.h"

namespace Ipc2581b
{

class FloorLifeParser
{
public:
    FloorLifeParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    FloorLife result();

private:
    FloorLife m_result;
};

}