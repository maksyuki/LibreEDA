
#pragma once

#include <QString>
class QXmlStreamReader;

#include "StandardPrimitive.h"

namespace Ipc2581b
{

class HexagonParser;
class OctagonParser;
class TriangleParser;
class DonutParser;
class EllipseParser;
class RectCornerParser;
class ThermalParser;
class RectChamParser;
class ButterflyParser;
class MoireParser;
class CircleParser;
class OvalParser;
class RectCenterParser;
class ContourParser;
class DiamondParser;

class StandardPrimitiveParser
{
public:
    StandardPrimitiveParser(
            ButterflyParser*&
            , CircleParser*&
            , ContourParser*&
            , DiamondParser*&
            , DonutParser*&
            , EllipseParser*&
            , HexagonParser*&
            , MoireParser*&
            , OctagonParser*&
            , OvalParser*&
            , RectCenterParser*&
            , RectChamParser*&
            , RectCornerParser*&
            , ThermalParser*&
            , TriangleParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    StandardPrimitive *result();

private:
    ButterflyParser *&m_butterflyParser;
    CircleParser *&m_circleParser;
    ContourParser *&m_contourParser;
    DiamondParser *&m_diamondParser;
    DonutParser *&m_donutParser;
    EllipseParser *&m_ellipseParser;
    HexagonParser *&m_hexagonParser;
    MoireParser *&m_moireParser;
    OctagonParser *&m_octagonParser;
    OvalParser *&m_ovalParser;
    RectCenterParser *&m_rectCenterParser;
    RectChamParser *&m_rectChamParser;
    RectCornerParser *&m_rectCornerParser;
    ThermalParser *&m_thermalParser;
    TriangleParser *&m_triangleParser;
    StandardPrimitive *m_result;
};

}