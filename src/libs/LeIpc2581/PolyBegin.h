
#pragma once

#include <QList>
#include "Optional.h"


#include "Double.h"

namespace Ipc2581b
{

class PolyBegin
{
public:
	virtual ~PolyBegin() {}

    Double x;
    Double y;

};

}