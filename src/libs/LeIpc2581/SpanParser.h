
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Span.h"

namespace Ipc2581b
{

class QStringParser;

class SpanParser
{
public:
    SpanParser(
            QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Span *result();

private:
    QStringParser *&m_fromLayerParser;
    QStringParser *&m_toLayerParser;
    QScopedPointer<Span> m_result;
};

}