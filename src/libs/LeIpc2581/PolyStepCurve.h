
#pragma once

#include <QList>
#include "Optional.h"

#include "PolyStep.h"

#include "Bool.h"
#include "Double.h"

namespace Ipc2581b
{

class PolyStepCurve: public PolyStep
{
public:
	virtual ~PolyStepCurve() {}

    Double x;
    Double y;
    Double centerX;
    Double centerY;
    Optional<Bool> clockwiseOptional;

    virtual PolyStepType polyStepType() const override
    {
        return PolyStepType::PolyStepCurve;
    }
};

}