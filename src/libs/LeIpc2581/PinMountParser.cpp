#include "PinMountParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

PinMountParser::PinMountParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool PinMountParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, PinMount> map =
    {
        {QStringLiteral("NONBOARD"), PinMount::Nonboard},
        {QStringLiteral("PRESSFIT"), PinMount::Pressfit},
        {QStringLiteral("HOLE"), PinMount::Hole},
        {QStringLiteral("SURFACE_MOUNT_PIN"), PinMount::SurfaceMountPin},
        {QStringLiteral("THROUGH_HOLE_HOLE"), PinMount::ThroughHoleHole},
        {QStringLiteral("SURFACE_MOUNT_PAD"), PinMount::SurfaceMountPad},
        {QStringLiteral("THROUGH_HOLE_PIN"), PinMount::ThroughHolePin},
        {QStringLiteral("UNDEFINED"), PinMount::Undefined},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum PinMount").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

PinMount PinMountParser::result()
{
    return m_result;
}

}