#include "FloorLifeParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

FloorLifeParser::FloorLifeParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool FloorLifeParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, FloorLife> map =
    {
        {QStringLiteral("1_YEAR"), FloorLife::_1Year},
        {QStringLiteral("48_HOURS"), FloorLife::_48Hours},
        {QStringLiteral("4_WEEKS"), FloorLife::_4Weeks},
        {QStringLiteral("24_HOURS"), FloorLife::_24Hours},
        {QStringLiteral("UNLIMITED"), FloorLife::Unlimited},
        {QStringLiteral("BAKE"), FloorLife::Bake},
        {QStringLiteral("72_HOURS"), FloorLife::_72Hours},
        {QStringLiteral("168_HOURS"), FloorLife::_168Hours},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum FloorLife").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

FloorLife FloorLifeParser::result()
{
    return m_result;
}

}