#include "CadPropertyParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

CadPropertyParser::CadPropertyParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool CadPropertyParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, CadProperty> map =
    {
        {QStringLiteral("DOUBLE"), CadProperty::Double},
        {QStringLiteral("INTEGER"), CadProperty::Integer},
        {QStringLiteral("BOOLEAN"), CadProperty::Boolean},
        {QStringLiteral("STRING"), CadProperty::String},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum CadProperty").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

CadProperty CadPropertyParser::result()
{
    return m_result;
}

}