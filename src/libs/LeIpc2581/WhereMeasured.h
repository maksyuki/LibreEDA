
#pragma once

namespace Ipc2581b
{

enum class WhereMeasured
{
    Other,
    Mask,
    Metal,
    Laminate,
};

}