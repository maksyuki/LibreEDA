
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Features.h"

namespace Ipc2581b
{

class LocationParser;
class XformParser;
class FeatureParser;

class FeaturesParser
{
public:
    FeaturesParser(
            XformParser*&
            , LocationParser*&
            , FeatureParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Features *result();

private:
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    FeatureParser *&m_featureParser;
    QScopedPointer<Features> m_result;
};

}