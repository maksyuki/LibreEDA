
#pragma once

#include <QString>

class QXmlStreamReader;

#include "CertificationCategory.h"

namespace Ipc2581b
{

class CertificationCategoryParser
{
public:
    CertificationCategoryParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    CertificationCategory result();

private:
    CertificationCategory m_result;
};

}