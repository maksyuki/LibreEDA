
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Package.h"

namespace Ipc2581b
{

class PinOneOrientationParser;
class PackageTypeParser;
class QStringParser;
class AssemblyDrawingParser;
class PinParser;
class OutlineParser;
class SilkScreenParser;
class DoubleParser;
class LandPatternParser;
class LocationParser;

class PackageParser
{
public:
    PackageParser(
            QStringParser*&
            , PackageTypeParser*&
            , QStringParser*&
            , PinOneOrientationParser*&
            , DoubleParser*&
            , QStringParser*&
            , OutlineParser*&
            , LocationParser*&
            , LandPatternParser*&
            , SilkScreenParser*&
            , AssemblyDrawingParser*&
            , PinParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Package *result();

private:
    QStringParser *&m_nameParser;
    PackageTypeParser *&m_typeParser;
    QStringParser *&m_pinOneParser;
    PinOneOrientationParser *&m_pinOneOrientationParser;
    DoubleParser *&m_heightParser;
    QStringParser *&m_commentParser;
    OutlineParser *&m_outlineParser;
    LocationParser *&m_pickupPointParser;
    LandPatternParser *&m_landPatternParser;
    SilkScreenParser *&m_silkScreenParser;
    AssemblyDrawingParser *&m_assemblyDrawingParser;
    PinParser *&m_pinParser;
    QScopedPointer<Package> m_result;
};

}