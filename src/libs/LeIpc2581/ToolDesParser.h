
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "ToolDes.h"

namespace Ipc2581b
{

class QStringParser;

class ToolDesParser
{
public:
    ToolDesParser(
            QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    ToolDes *result();

private:
    QStringParser *&m_nameParser;
    QStringParser *&m_layerRefParser;
    QScopedPointer<ToolDes> m_result;
};

}