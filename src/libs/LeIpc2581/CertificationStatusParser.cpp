#include "CertificationStatusParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

CertificationStatusParser::CertificationStatusParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool CertificationStatusParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, CertificationStatus> map =
    {
        {QStringLiteral("ALPHA"), CertificationStatus::Alpha},
        {QStringLiteral("SELFTEST"), CertificationStatus::Selftest},
        {QStringLiteral("CERTIFIED"), CertificationStatus::Certified},
        {QStringLiteral("BETA"), CertificationStatus::Beta},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum CertificationStatus").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

CertificationStatus CertificationStatusParser::result()
{
    return m_result;
}

}