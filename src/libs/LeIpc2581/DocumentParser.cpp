
#include <QXmlStreamReader>
#include "DocumentParser.h"

#include "Ipc2581Parser.h"
#include "ContentParser.h"
#include "FunctionModeParser.h"
#include "DictionaryStandardParser.h"
#include "EntryStandardParser.h"
#include "DictionaryUserParser.h"
#include "EntryUserParser.h"
#include "DictionaryFontParser.h"
#include "EntryFontParser.h"
#include "FontDefParser.h"
#include "FontDefEmbeddedParser.h"
#include "FontDefExternalParser.h"
#include "GlyphParser.h"
#include "FontRefParser.h"
#include "DictionaryLineDescParser.h"
#include "EntryLineDescParser.h"
#include "LineDescParser.h"
#include "LineDescGroupParser.h"
#include "LineDescRefParser.h"
#include "DictionaryFillDescParser.h"
#include "EntryFillDescParser.h"
#include "FillDescParser.h"
#include "FillDescGroupParser.h"
#include "FillDescRefParser.h"
#include "DictionaryColorParser.h"
#include "EntryColorParser.h"
#include "ColorGroupParser.h"
#include "ColorParser.h"
#include "ColorRefParser.h"
#include "DictionaryFirmwareParser.h"
#include "LogisticHeaderParser.h"
#include "RoleParser.h"
#include "EnterpriseParser.h"
#include "PersonParser.h"
#include "HistoryRecordParser.h"
#include "FileRevisionParser.h"
#include "SoftwarePackageParser.h"
#include "CertificationParser.h"
#include "ChangeRecParser.h"
#include "ApprovalParser.h"
#include "BomParser.h"
#include "BomHeaderParser.h"
#include "BomItemParser.h"
#include "BomDesParser.h"
#include "DocDesParser.h"
#include "MatDesParser.h"
#include "RefDesParser.h"
#include "ToolDesParser.h"
#include "CharacteristicsParser.h"
#include "ECadParser.h"
#include "CadHeaderParser.h"
#include "SpecParser.h"
#include "SpecificationTypeParser.h"
#include "BackdrillParser.h"
#include "ComplianceParser.h"
#include "ConductorParser.h"
#include "DielectricParser.h"
#include "GeneralParser.h"
#include "ImpedanceParser.h"
#include "TechnologyParser.h"
#include "TemperatureParser.h"
#include "ToolParser.h"
#include "PropertyParser.h"
#include "V_CutParser.h"
#include "CadDataParser.h"
#include "LayerParser.h"
#include "StackupParser.h"
#include "StackupGroupParser.h"
#include "StackupLayerParser.h"
#include "StepParser.h"
#include "PadStackParser.h"
#include "LayerHoleParser.h"
#include "LayerPadParser.h"
#include "PadstackDefParser.h"
#include "PadstackHoleDefParser.h"
#include "PadstackPadDefParser.h"
#include "RouteParser.h"
#include "StepRepeatParser.h"
#include "PackageParser.h"
#include "LandPatternParser.h"
#include "TargetParser.h"
#include "SilkScreenParser.h"
#include "AssemblyDrawingParser.h"
#include "MarkingParser.h"
#include "PinParser.h"
#include "ComponentParser.h"
#include "LogicalNetParser.h"
#include "PinRefParser.h"
#include "PhyNetGroupParser.h"
#include "PhyNetParser.h"
#include "PhyNetPointParser.h"
#include "LayerFeatureParser.h"
#include "SetParser.h"
#include "PadParser.h"
#include "FiducialParser.h"
#include "BadBoardMarkParser.h"
#include "GlobalFiducialParser.h"
#include "GoodPanelMarkParser.h"
#include "LocalFiducialParser.h"
#include "HoleParser.h"
#include "SlotCavityParser.h"
#include "ZAxisDimParser.h"
#include "MaterialCutParser.h"
#include "MaterialLeftParser.h"
#include "FeaturesParser.h"
#include "DfxMeasurementsParser.h"
#include "AvlParser.h"
#include "FeatureParser.h"
#include "StandardShapeParser.h"
#include "UserShapeParser.h"
#include "StandardPrimitiveRefParser.h"
#include "UserPrimitiveRefParser.h"
#include "StandardPrimitiveParser.h"
#include "ButterflyParser.h"
#include "CircleParser.h"
#include "ContourParser.h"
#include "DiamondParser.h"
#include "DonutParser.h"
#include "EllipseParser.h"
#include "HexagonParser.h"
#include "MoireParser.h"
#include "OctagonParser.h"
#include "OvalParser.h"
#include "RectCenterParser.h"
#include "RectChamParser.h"
#include "RectCornerParser.h"
#include "RectRoundParser.h"
#include "ThermalParser.h"
#include "TriangleParser.h"
#include "UserPrimitiveParser.h"
#include "SimpleParser.h"
#include "ArcParser.h"
#include "LineParser.h"
#include "OutlineParser.h"
#include "PolylineParser.h"
#include "TextParser.h"
#include "UserSpecialParser.h"
#include "XformParser.h"
#include "LocationParser.h"
#include "PolygonParser.h"
#include "PolyBeginParser.h"
#include "PolyStepParser.h"
#include "PolyStepSegmentParser.h"
#include "PolyStepCurveParser.h"
#include "BoundingBoxParser.h"
#include "NameRefParser.h"
#include "IdRefParser.h"
#include "CADDataLayerRefParser.h"
#include "SpanParser.h"
#include "NonstandardAttributeParser.h"
#include "RevisionParser.h"
#include "BackdrillListParser.h"
#include "BomCategoryParser.h"
#include "BoardTechnologyParser.h"
#include "ButterflyShapeParser.h"
#include "CadPinParser.h"
#include "CadPropertyParser.h"
#include "CertificationCategoryParser.h"
#include "CertificationStatusParser.h"
#include "ComplianceListParser.h"
#include "ConductorListParser.h"
#include "ContextParser.h"
#include "DfxCategoryParser.h"
#include "DielectricListParser.h"
#include "DonutShapeParser.h"
#include "ToolListParser.h"
#include "ToolPropertyListParser.h"
#include "EnterpriseCodeParser.h"
#include "ExposureParser.h"
#include "FloorLifeParser.h"
#include "GeneralListParser.h"
#include "ImpedanceListParser.h"
#include "IsoCodeParser.h"
#include "LayerFunctionParser.h"
#include "LineEndParser.h"
#include "FillPropertyParser.h"
#include "LinePropertyParser.h"
#include "MarkingUsageParser.h"
#include "ModeParser.h"
#include "ModRefParser.h"
#include "MountParser.h"
#include "NetClassParser.h"
#include "NetPointParser.h"
#include "PackageTypeParser.h"
#include "PadUsageParser.h"
#include "PadUseParser.h"
#include "PinElectricalParser.h"
#include "PinMountParser.h"
#include "PinOneOrientationParser.h"
#include "PolarityParser.h"
#include "PropertyUnitParser.h"
#include "RoleFunctionParser.h"
#include "SideParser.h"
#include "PlatingStatusParser.h"
#include "StructureListParser.h"
#include "TechnologyListParser.h"
#include "TemperatureListParser.h"
#include "ThermalShapeParser.h"
#include "TransmissionListParser.h"
#include "UnitModeParser.h"
#include "UnitsParser.h"
#include "VCutListParser.h"
#include "WhereMeasuredParser.h"
#include "DoubleParser.h"
#include "IntParser.h"
#include "BoolParser.h"
#include "QStringParser.h"

namespace Ipc2581b
{

DocumentParser::DocumentParser()
{
    m_Ipc2581Parser = new Ipc2581Parser(
                m_RevisionParser
                , m_ContentParser
                , m_LogisticHeaderParser
                , m_HistoryRecordParser
                , m_BomParser
                , m_ECadParser
                , m_AvlParser
                );
    m_ContentParser = new ContentParser(
                m_QStringParser
                , m_FunctionModeParser
                , m_NameRefParser
                , m_NameRefParser
                , m_NameRefParser
                , m_NameRefParser
                , m_DictionaryStandardParser
                , m_DictionaryUserParser
                , m_DictionaryFontParser
                , m_DictionaryLineDescParser
                , m_DictionaryFillDescParser
                , m_DictionaryColorParser
                , m_DictionaryFirmwareParser
                );
    m_FunctionModeParser = new FunctionModeParser(
                m_ModeParser
                , m_IntParser
                , m_QStringParser
                );
    m_DictionaryStandardParser = new DictionaryStandardParser(
                m_UnitsParser
                , m_EntryStandardParser
                );
    m_EntryStandardParser = new EntryStandardParser(
                m_QStringParser
                , m_StandardPrimitiveParser
                );
    m_DictionaryUserParser = new DictionaryUserParser(
                m_UnitsParser
                , m_EntryUserParser
                );
    m_EntryUserParser = new EntryUserParser(
                m_QStringParser
                , m_UserPrimitiveParser
                );
    m_DictionaryFontParser = new DictionaryFontParser(
                m_UnitsParser
                , m_EntryFontParser
                );
    m_EntryFontParser = new EntryFontParser(
                m_QStringParser
                , m_FontDefParser
                );
    m_FontDefParser = new FontDefParser(
                m_FontDefEmbeddedParser
                , m_FontDefExternalParser
                );
    m_FontDefEmbeddedParser = new FontDefEmbeddedParser(
                m_QStringParser
                , m_LineDescGroupParser
                , m_GlyphParser
                );
    m_FontDefExternalParser = new FontDefExternalParser(
                m_QStringParser
                , m_QStringParser
                );
    m_GlyphParser = new GlyphParser(
                m_QStringParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_SimpleParser
                );
    m_FontRefParser = new FontRefParser(
                m_QStringParser
                );
    m_DictionaryLineDescParser = new DictionaryLineDescParser(
                m_UnitsParser
                , m_EntryLineDescParser
                );
    m_EntryLineDescParser = new EntryLineDescParser(
                m_QStringParser
                , m_LineDescParser
                );
    m_LineDescParser = new LineDescParser(
                m_LineEndParser
                , m_DoubleParser
                , m_LinePropertyParser
                );
    m_LineDescGroupParser = new LineDescGroupParser(
                m_LineDescParser
                , m_LineDescRefParser
                );
    m_LineDescRefParser = new LineDescRefParser(
                m_QStringParser
                );
    m_DictionaryFillDescParser = new DictionaryFillDescParser(
                m_UnitsParser
                , m_EntryFillDescParser
                );
    m_EntryFillDescParser = new EntryFillDescParser(
                m_QStringParser
                , m_FillDescParser
                );
    m_FillDescParser = new FillDescParser(
                m_FillPropertyParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_ColorGroupParser
                );
    m_FillDescGroupParser = new FillDescGroupParser(
                m_FillDescParser
                , m_FillDescRefParser
                );
    m_FillDescRefParser = new FillDescRefParser(
                m_QStringParser
                );
    m_DictionaryColorParser = new DictionaryColorParser(
                m_EntryColorParser
                );
    m_EntryColorParser = new EntryColorParser(
                m_QStringParser
                , m_ColorParser
                );
    m_ColorGroupParser = new ColorGroupParser(
                m_ColorParser
                , m_ColorRefParser
                );
    m_ColorParser = new ColorParser(
                m_IntParser
                , m_IntParser
                , m_IntParser
                );
    m_ColorRefParser = new ColorRefParser(
                m_QStringParser
                );
    m_DictionaryFirmwareParser = new DictionaryFirmwareParser(
                );
    m_LogisticHeaderParser = new LogisticHeaderParser(
                m_RoleParser
                , m_EnterpriseParser
                , m_PersonParser
                );
    m_RoleParser = new RoleParser(
                m_QStringParser
                , m_RoleFunctionParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                );
    m_EnterpriseParser = new EnterpriseParser(
                m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_EnterpriseCodeParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_IsoCodeParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                );
    m_PersonParser = new PersonParser(
                m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                );
    m_HistoryRecordParser = new HistoryRecordParser(
                m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_FileRevisionParser
                , m_ChangeRecParser
                );
    m_FileRevisionParser = new FileRevisionParser(
                m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_SoftwarePackageParser
                );
    m_SoftwarePackageParser = new SoftwarePackageParser(
                m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_CertificationParser
                );
    m_CertificationParser = new CertificationParser(
                m_CertificationStatusParser
                , m_CertificationStatusParser
                , m_CertificationCategoryParser
                );
    m_ChangeRecParser = new ChangeRecParser(
                m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_ApprovalParser
                );
    m_ApprovalParser = new ApprovalParser(
                m_QStringParser
                , m_QStringParser
                );
    m_BomParser = new BomParser(
                m_QStringParser
                , m_BomHeaderParser
                , m_BomItemParser
                );
    m_BomHeaderParser = new BomHeaderParser(
                m_QStringParser
                , m_QStringParser
                , m_BoolParser
                , m_NameRefParser
                );
    m_BomItemParser = new BomItemParser(
                m_QStringParser
                , m_QStringParser
                , m_IntParser
                , m_BomCategoryParser
                , m_QStringParser
                , m_QStringParser
                , m_BomDesParser
                , m_CharacteristicsParser
                );
    m_BomDesParser = new BomDesParser(
                m_DocDesParser
                , m_MatDesParser
                , m_RefDesParser
                , m_ToolDesParser
                );
    m_DocDesParser = new DocDesParser(
                m_QStringParser
                , m_QStringParser
                );
    m_MatDesParser = new MatDesParser(
                m_QStringParser
                , m_QStringParser
                );
    m_RefDesParser = new RefDesParser(
                m_QStringParser
                , m_QStringParser
                , m_BoolParser
                , m_QStringParser
                );
    m_ToolDesParser = new ToolDesParser(
                m_QStringParser
                , m_QStringParser
                );
    m_CharacteristicsParser = new CharacteristicsParser(
                );
    m_ECadParser = new ECadParser(
                m_QStringParser
                , m_CadHeaderParser
                , m_CadDataParser
                );
    m_CadHeaderParser = new CadHeaderParser(
                m_UnitsParser
                , m_SpecParser
                , m_ChangeRecParser
                );
    m_SpecParser = new SpecParser(
                m_QStringParser
                , m_SpecificationTypeParser
                , m_XformParser
                , m_LocationParser
                , m_OutlineParser
                );
    m_SpecificationTypeParser = new SpecificationTypeParser(
                m_BackdrillParser
                , m_ComplianceParser
                , m_ConductorParser
                , m_DielectricParser
                , m_GeneralParser
                , m_ImpedanceParser
                , m_TechnologyParser
                , m_TemperatureParser
                , m_ToolParser
                , m_V_CutParser
                );
    m_BackdrillParser = new BackdrillParser(
                m_BackdrillListParser
                , m_QStringParser
                , m_PropertyParser
                );
    m_ComplianceParser = new ComplianceParser(
                m_ComplianceListParser
                , m_QStringParser
                , m_PropertyParser
                );
    m_ConductorParser = new ConductorParser(
                m_ConductorListParser
                , m_QStringParser
                , m_PropertyParser
                );
    m_DielectricParser = new DielectricParser(
                m_DielectricListParser
                , m_QStringParser
                , m_PropertyParser
                );
    m_GeneralParser = new GeneralParser(
                m_GeneralListParser
                , m_QStringParser
                , m_PropertyParser
                );
    m_ImpedanceParser = new ImpedanceParser(
                m_ImpedanceListParser
                , m_QStringParser
                , m_TransmissionListParser
                , m_StructureListParser
                , m_PropertyParser
                );
    m_TechnologyParser = new TechnologyParser(
                m_TechnologyListParser
                , m_QStringParser
                , m_PropertyParser
                );
    m_TemperatureParser = new TemperatureParser(
                m_TemperatureListParser
                , m_QStringParser
                , m_PropertyParser
                );
    m_ToolParser = new ToolParser(
                m_ToolListParser
                , m_ToolPropertyListParser
                , m_QStringParser
                , m_PropertyParser
                );
    m_PropertyParser = new PropertyParser(
                m_DoubleParser
                , m_QStringParser
                , m_PropertyUnitParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_BoolParser
                , m_DoubleParser
                , m_QStringParser
                , m_PropertyUnitParser
                , m_QStringParser
                , m_QStringParser
                );
    m_V_CutParser = new V_CutParser(
                m_VCutListParser
                , m_QStringParser
                , m_PropertyParser
                );
    m_CadDataParser = new CadDataParser(
                m_LayerParser
                , m_StackupParser
                , m_StepParser
                );
    m_LayerParser = new LayerParser(
                m_QStringParser
                , m_LayerFunctionParser
                , m_SideParser
                , m_PolarityParser
                , m_IdRefParser
                );
    m_StackupParser = new StackupParser(
                m_QStringParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_WhereMeasuredParser
                , m_QStringParser
                , m_MatDesParser
                , m_IdRefParser
                , m_StackupGroupParser
                );
    m_StackupGroupParser = new StackupGroupParser(
                m_QStringParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_QStringParser
                , m_MatDesParser
                , m_IdRefParser
                , m_StackupLayerParser
                , m_CADDataLayerRefParser
                );
    m_StackupLayerParser = new StackupLayerParser(
                m_QStringParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_QStringParser
                , m_MatDesParser
                , m_IdRefParser
                );
    m_StepParser = new StepParser(
                m_QStringParser
                , m_NonstandardAttributeParser
                , m_PadStackParser
                , m_PadstackDefParser
                , m_RouteParser
                , m_LocationParser
                , m_ContourParser
                , m_StepRepeatParser
                , m_PackageParser
                , m_ComponentParser
                , m_LogicalNetParser
                , m_PhyNetGroupParser
                , m_LayerFeatureParser
                , m_DfxMeasurementsParser
                );
    m_PadStackParser = new PadStackParser(
                m_QStringParser
                , m_LayerHoleParser
                , m_LayerPadParser
                );
    m_LayerHoleParser = new LayerHoleParser(
                );
    m_LayerPadParser = new LayerPadParser(
                );
    m_PadstackDefParser = new PadstackDefParser(
                m_QStringParser
                , m_PadstackHoleDefParser
                , m_PadstackPadDefParser
                );
    m_PadstackHoleDefParser = new PadstackHoleDefParser(
                m_QStringParser
                , m_DoubleParser
                , m_PlatingStatusParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                );
    m_PadstackPadDefParser = new PadstackPadDefParser(
                m_QStringParser
                , m_PadUseParser
                , m_QStringParser
                , m_XformParser
                , m_LocationParser
                , m_FeatureParser
                );
    m_RouteParser = new RouteParser(
                m_QStringParser
                , m_LayerFeatureParser
                );
    m_StepRepeatParser = new StepRepeatParser(
                m_QStringParser
                , m_DoubleParser
                , m_DoubleParser
                , m_IntParser
                , m_IntParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_BoolParser
                );
    m_PackageParser = new PackageParser(
                m_QStringParser
                , m_PackageTypeParser
                , m_QStringParser
                , m_PinOneOrientationParser
                , m_DoubleParser
                , m_QStringParser
                , m_OutlineParser
                , m_LocationParser
                , m_LandPatternParser
                , m_SilkScreenParser
                , m_AssemblyDrawingParser
                , m_PinParser
                );
    m_LandPatternParser = new LandPatternParser(
                m_PadParser
                , m_TargetParser
                );
    m_TargetParser = new TargetParser(
                m_XformParser
                , m_LocationParser
                , m_StandardShapeParser
                );
    m_SilkScreenParser = new SilkScreenParser(
                m_OutlineParser
                , m_MarkingParser
                );
    m_AssemblyDrawingParser = new AssemblyDrawingParser(
                m_OutlineParser
                , m_MarkingParser
                );
    m_MarkingParser = new MarkingParser(
                m_MarkingUsageParser
                , m_XformParser
                , m_LocationParser
                , m_FeatureParser
                );
    m_PinParser = new PinParser(
                m_QStringParser
                , m_QStringParser
                , m_CadPinParser
                , m_PinElectricalParser
                , m_PinMountParser
                , m_XformParser
                , m_LocationParser
                , m_StandardShapeParser
                );
    m_ComponentParser = new ComponentParser(
                m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_QStringParser
                , m_MountParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_NonstandardAttributeParser
                , m_XformParser
                , m_LocationParser
                );
    m_LogicalNetParser = new LogicalNetParser(
                m_QStringParser
                , m_NetClassParser
                , m_NonstandardAttributeParser
                , m_PinRefParser
                );
    m_PinRefParser = new PinRefParser(
                m_QStringParser
                , m_QStringParser
                , m_QStringParser
                );
    m_PhyNetGroupParser = new PhyNetGroupParser(
                m_QStringParser
                , m_BoolParser
                , m_PhyNetParser
                );
    m_PhyNetParser = new PhyNetParser(
                m_QStringParser
                , m_PhyNetPointParser
                );
    m_PhyNetPointParser = new PhyNetPointParser(
                m_DoubleParser
                , m_DoubleParser
                , m_QStringParser
                , m_QStringParser
                , m_NetPointParser
                , m_ExposureParser
                , m_QStringParser
                , m_QStringParser
                , m_BoolParser
                , m_BoolParser
                , m_BoolParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                , m_FeatureParser
                );
    m_LayerFeatureParser = new LayerFeatureParser(
                m_QStringParser
                , m_SetParser
                );
    m_SetParser = new SetParser(
                m_QStringParser
                , m_PolarityParser
                , m_PadUsageParser
                , m_BoolParser
                , m_QStringParser
                , m_BoolParser
                , m_QStringParser
                , m_NonstandardAttributeParser
                , m_PadParser
                , m_FiducialParser
                , m_HoleParser
                , m_SlotCavityParser
                , m_IdRefParser
                , m_FeaturesParser
                , m_ColorGroupParser
                , m_LineDescGroupParser
                );
    m_PadParser = new PadParser(
                m_QStringParser
                , m_XformParser
                , m_LocationParser
                , m_FeatureParser
                , m_PinRefParser
                );
    m_FiducialParser = new FiducialParser(
                m_BadBoardMarkParser
                , m_GlobalFiducialParser
                , m_GoodPanelMarkParser
                , m_LocalFiducialParser
                );
    m_BadBoardMarkParser = new BadBoardMarkParser(
                m_XformParser
                , m_LocationParser
                , m_StandardShapeParser
                );
    m_GlobalFiducialParser = new GlobalFiducialParser(
                m_XformParser
                , m_LocationParser
                , m_StandardShapeParser
                );
    m_GoodPanelMarkParser = new GoodPanelMarkParser(
                m_XformParser
                , m_LocationParser
                , m_StandardShapeParser
                );
    m_LocalFiducialParser = new LocalFiducialParser(
                m_XformParser
                , m_LocationParser
                , m_StandardShapeParser
                );
    m_HoleParser = new HoleParser(
                m_QStringParser
                , m_DoubleParser
                , m_PlatingStatusParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_IdRefParser
                );
    m_SlotCavityParser = new SlotCavityParser(
                m_QStringParser
                , m_PlatingStatusParser
                , m_DoubleParser
                , m_DoubleParser
                , m_SimpleParser
                , m_ZAxisDimParser
                );
    m_ZAxisDimParser = new ZAxisDimParser(
                m_MaterialCutParser
                , m_MaterialLeftParser
                );
    m_MaterialCutParser = new MaterialCutParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                );
    m_MaterialLeftParser = new MaterialLeftParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                );
    m_FeaturesParser = new FeaturesParser(
                m_XformParser
                , m_LocationParser
                , m_FeatureParser
                );
    m_DfxMeasurementsParser = new DfxMeasurementsParser(
                );
    m_AvlParser = new AvlParser(
                );
    m_FeatureParser = new FeatureParser(
                m_StandardShapeParser
                , m_UserShapeParser
                );
    m_StandardShapeParser = new StandardShapeParser(
                m_StandardPrimitiveParser
                , m_StandardPrimitiveRefParser
                );
    m_UserShapeParser = new UserShapeParser(
                m_UserPrimitiveParser
                , m_UserPrimitiveRefParser
                );
    m_StandardPrimitiveRefParser = new StandardPrimitiveRefParser(
                m_QStringParser
                );
    m_UserPrimitiveRefParser = new UserPrimitiveRefParser(
                m_QStringParser
                );
    m_StandardPrimitiveParser = new StandardPrimitiveParser(
                m_ButterflyParser
                , m_CircleParser
                , m_ContourParser
                , m_DiamondParser
                , m_DonutParser
                , m_EllipseParser
                , m_HexagonParser
                , m_MoireParser
                , m_OctagonParser
                , m_OvalParser
                , m_RectCenterParser
                , m_RectChamParser
                , m_RectCornerParser
                , m_ThermalParser
                , m_TriangleParser
                );
    m_ButterflyParser = new ButterflyParser(
                m_ButterflyShapeParser
                , m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_CircleParser = new CircleParser(
                m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_ContourParser = new ContourParser(
                m_PolygonParser
                , m_PolygonParser
                );
    m_DiamondParser = new DiamondParser(
                m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_DonutParser = new DonutParser(
                m_DonutShapeParser
                , m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_EllipseParser = new EllipseParser(
                m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_HexagonParser = new HexagonParser(
                m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_MoireParser = new MoireParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_IntParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                );
    m_OctagonParser = new OctagonParser(
                m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_OvalParser = new OvalParser(
                m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_RectCenterParser = new RectCenterParser(
                m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_RectChamParser = new RectChamParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_BoolParser
                , m_BoolParser
                , m_BoolParser
                , m_BoolParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_RectCornerParser = new RectCornerParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_RectRoundParser = new RectRoundParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_BoolParser
                , m_BoolParser
                , m_BoolParser
                , m_BoolParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_ThermalParser = new ThermalParser(
                m_ThermalShapeParser
                , m_DoubleParser
                , m_DoubleParser
                , m_IntParser
                , m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_TriangleParser = new TriangleParser(
                m_DoubleParser
                , m_DoubleParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_UserPrimitiveParser = new UserPrimitiveParser(
                m_SimpleParser
                , m_TextParser
                , m_UserSpecialParser
                );
    m_SimpleParser = new SimpleParser(
                m_ArcParser
                , m_LineParser
                , m_OutlineParser
                , m_PolylineParser
                );
    m_ArcParser = new ArcParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_BoolParser
                , m_LineDescGroupParser
                );
    m_LineParser = new LineParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_LineDescGroupParser
                );
    m_OutlineParser = new OutlineParser(
                m_PolygonParser
                , m_LineDescGroupParser
                );
    m_PolylineParser = new PolylineParser(
                m_PolyBeginParser
                , m_PolyStepParser
                , m_LineDescGroupParser
                );
    m_TextParser = new TextParser(
                m_QStringParser
                , m_IntParser
                , m_XformParser
                , m_BoundingBoxParser
                , m_FontRefParser
                , m_ColorGroupParser
                );
    m_UserSpecialParser = new UserSpecialParser(
                m_FeatureParser
                );
    m_XformParser = new XformParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_BoolParser
                , m_DoubleParser
                );
    m_LocationParser = new LocationParser(
                m_DoubleParser
                , m_DoubleParser
                );
    m_PolygonParser = new PolygonParser(
                m_PolyBeginParser
                , m_PolyStepParser
                , m_XformParser
                , m_LineDescGroupParser
                , m_FillDescGroupParser
                );
    m_PolyBeginParser = new PolyBeginParser(
                m_DoubleParser
                , m_DoubleParser
                );
    m_PolyStepParser = new PolyStepParser(
                m_PolyStepSegmentParser
                , m_PolyStepCurveParser
                );
    m_PolyStepSegmentParser = new PolyStepSegmentParser(
                m_DoubleParser
                , m_DoubleParser
                );
    m_PolyStepCurveParser = new PolyStepCurveParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_BoolParser
                );
    m_BoundingBoxParser = new BoundingBoxParser(
                m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                , m_DoubleParser
                );
    m_NameRefParser = new NameRefParser(
                m_QStringParser
                );
    m_IdRefParser = new IdRefParser(
                m_QStringParser
                );
    m_CADDataLayerRefParser = new CADDataLayerRefParser(
                m_QStringParser
                );
    m_SpanParser = new SpanParser(
                m_QStringParser
                , m_QStringParser
                );
    m_NonstandardAttributeParser = new NonstandardAttributeParser(
                m_QStringParser
                , m_CadPropertyParser
                , m_QStringParser
                );
    m_RevisionParser = new RevisionParser(
                );
    m_BackdrillListParser = new BackdrillListParser(
                );
    m_BomCategoryParser = new BomCategoryParser(
                );
    m_BoardTechnologyParser = new BoardTechnologyParser(
                );
    m_ButterflyShapeParser = new ButterflyShapeParser(
                );
    m_CadPinParser = new CadPinParser(
                );
    m_CadPropertyParser = new CadPropertyParser(
                );
    m_CertificationCategoryParser = new CertificationCategoryParser(
                );
    m_CertificationStatusParser = new CertificationStatusParser(
                );
    m_ComplianceListParser = new ComplianceListParser(
                );
    m_ConductorListParser = new ConductorListParser(
                );
    m_ContextParser = new ContextParser(
                );
    m_DfxCategoryParser = new DfxCategoryParser(
                );
    m_DielectricListParser = new DielectricListParser(
                );
    m_DonutShapeParser = new DonutShapeParser(
                );
    m_ToolListParser = new ToolListParser(
                );
    m_ToolPropertyListParser = new ToolPropertyListParser(
                );
    m_EnterpriseCodeParser = new EnterpriseCodeParser(
                );
    m_ExposureParser = new ExposureParser(
                );
    m_FloorLifeParser = new FloorLifeParser(
                );
    m_GeneralListParser = new GeneralListParser(
                );
    m_ImpedanceListParser = new ImpedanceListParser(
                );
    m_IsoCodeParser = new IsoCodeParser(
                );
    m_LayerFunctionParser = new LayerFunctionParser(
                );
    m_LineEndParser = new LineEndParser(
                );
    m_FillPropertyParser = new FillPropertyParser(
                );
    m_LinePropertyParser = new LinePropertyParser(
                );
    m_MarkingUsageParser = new MarkingUsageParser(
                );
    m_ModeParser = new ModeParser(
                );
    m_ModRefParser = new ModRefParser(
                );
    m_MountParser = new MountParser(
                );
    m_NetClassParser = new NetClassParser(
                );
    m_NetPointParser = new NetPointParser(
                );
    m_PackageTypeParser = new PackageTypeParser(
                );
    m_PadUsageParser = new PadUsageParser(
                );
    m_PadUseParser = new PadUseParser(
                );
    m_PinElectricalParser = new PinElectricalParser(
                );
    m_PinMountParser = new PinMountParser(
                );
    m_PinOneOrientationParser = new PinOneOrientationParser(
                );
    m_PolarityParser = new PolarityParser(
                );
    m_PropertyUnitParser = new PropertyUnitParser(
                );
    m_RoleFunctionParser = new RoleFunctionParser(
                );
    m_SideParser = new SideParser(
                );
    m_PlatingStatusParser = new PlatingStatusParser(
                );
    m_StructureListParser = new StructureListParser(
                );
    m_TechnologyListParser = new TechnologyListParser(
                );
    m_TemperatureListParser = new TemperatureListParser(
                );
    m_ThermalShapeParser = new ThermalShapeParser(
                );
    m_TransmissionListParser = new TransmissionListParser(
                );
    m_UnitModeParser = new UnitModeParser(
                );
    m_UnitsParser = new UnitsParser(
                );
    m_VCutListParser = new VCutListParser(
                );
    m_WhereMeasuredParser = new WhereMeasuredParser(
                );
    m_DoubleParser = new DoubleParser(
                );
    m_IntParser = new IntParser(
                );
    m_BoolParser = new BoolParser(
                );
    m_QStringParser = new QStringParser(
                );
}



bool DocumentParser::parse(QXmlStreamReader *reader)
{
    return m_Ipc2581Parser->parse(reader);
}

Ipc2581 *DocumentParser::result()
{
    return m_Ipc2581Parser->result();
}

}