#include "DiamondParser.h"

#include "LineDescGroupParser.h"
#include "FillDescGroupParser.h"
#include "XformParser.h"
#include "DoubleParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

DiamondParser::DiamondParser(
    DoubleParser *&_widthParser
    , DoubleParser *&_heightParser
    , XformParser *&_xformParser
    , LineDescGroupParser *&_lineDescGroupParser
    , FillDescGroupParser *&_fillDescGroupParser
):    m_widthParser(_widthParser)
    , m_heightParser(_heightParser)
    , m_xformParser(_xformParser)
    , m_lineDescGroupParser(_lineDescGroupParser)
    , m_fillDescGroupParser(_fillDescGroupParser)
{

}

bool DiamondParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Diamond());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("width")))
    {
        data = reader->attributes().value(QStringLiteral("width"));
        if (!m_widthParser->parse(reader, data))
            return false;
        m_result->width = m_widthParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("width: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("height")))
    {
        data = reader->attributes().value(QStringLiteral("height"));
        if (!m_heightParser->parse(reader, data))
            return false;
        m_result->height = m_heightParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("height: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (m_lineDescGroupParser->isSubstitution(name))
        {
            if (!m_lineDescGroupParser->parse(reader))
                return false;
            auto result = m_lineDescGroupParser->result();
            m_result->lineDescGroupOptional = Optional<LineDescGroup*>(result);
        }
        else if (m_fillDescGroupParser->isSubstitution(name))
        {
            if (!m_fillDescGroupParser->parse(reader))
                return false;
            auto result = m_fillDescGroupParser->result();
            m_result->fillDescGroupOptional = Optional<FillDescGroup*>(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Diamond *DiamondParser::result()
{
    return m_result.take();
}

}