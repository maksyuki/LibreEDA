
#pragma once

#include <QList>
#include "Optional.h"

#include "SpecificationType.h"

#include "QString.h"
#include "ToolList.h"
#include "Property.h"
#include "ToolPropertyList.h"

namespace Ipc2581b
{

class Tool: public SpecificationType
{
public:
	virtual ~Tool() {}

    ToolList type;
    ToolPropertyList toolProperty;
    Optional<QString> commentOptional;
    QList<Property*> propertyList;

    virtual SpecificationTypeType specificationTypeType() const override
    {
        return SpecificationTypeType::Tool;
    }
};

}