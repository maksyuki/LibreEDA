
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardShape.h"

#include "QString.h"

namespace Ipc2581b
{

class StandardPrimitiveRef: public StandardShape
{
public:
	virtual ~StandardPrimitiveRef() {}

    QString id;

    virtual StandardShapeType standardShapeType() const override
    {
        return StandardShapeType::StandardPrimitiveRef;
    }
};

}