#include "StepParser.h"

#include "ComponentParser.h"
#include "LayerFeatureParser.h"
#include "PackageParser.h"
#include "QStringParser.h"
#include "StepRepeatParser.h"
#include "LogicalNetParser.h"
#include "RouteParser.h"
#include "PadStackParser.h"
#include "PhyNetGroupParser.h"
#include "PadstackDefParser.h"
#include "DfxMeasurementsParser.h"
#include "NonstandardAttributeParser.h"
#include "ContourParser.h"
#include "LocationParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

StepParser::StepParser(
    QStringParser *&_nameParser
    , NonstandardAttributeParser *&_nonstandardAttributeParser
    , PadStackParser *&_padStackParser
    , PadstackDefParser *&_padStackDefParser
    , RouteParser *&_routeParser
    , LocationParser *&_datumParser
    , ContourParser *&_profileParser
    , StepRepeatParser *&_stepRepeatParser
    , PackageParser *&_packageParser
    , ComponentParser *&_componentParser
    , LogicalNetParser *&_logicalNetParser
    , PhyNetGroupParser *&_phyNetGroupParser
    , LayerFeatureParser *&_layerFeatureParser
    , DfxMeasurementsParser *&_dfxMeasurementListParser
):    m_nameParser(_nameParser)
    , m_nonstandardAttributeParser(_nonstandardAttributeParser)
    , m_padStackParser(_padStackParser)
    , m_padStackDefParser(_padStackDefParser)
    , m_routeParser(_routeParser)
    , m_datumParser(_datumParser)
    , m_profileParser(_profileParser)
    , m_stepRepeatParser(_stepRepeatParser)
    , m_packageParser(_packageParser)
    , m_componentParser(_componentParser)
    , m_logicalNetParser(_logicalNetParser)
    , m_phyNetGroupParser(_phyNetGroupParser)
    , m_layerFeatureParser(_layerFeatureParser)
    , m_dfxMeasurementListParser(_dfxMeasurementListParser)
{

}

bool StepParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Step());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->name = m_nameParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("name: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("NonstandardAttribute"))
        {
            if (!m_nonstandardAttributeParser->parse(reader))
                return false;
            auto result = m_nonstandardAttributeParser->result();
            m_result->nonstandardAttributeList.append(result);
        }
        else if (name == QStringLiteral("PadStack"))
        {
            if (!m_padStackParser->parse(reader))
                return false;
            auto result = m_padStackParser->result();
            m_result->padStackList.append(result);
        }
        else if (name == QStringLiteral("PadStackDef"))
        {
            if (!m_padStackDefParser->parse(reader))
                return false;
            auto result = m_padStackDefParser->result();
            m_result->padStackDefList.append(result);
        }
        else if (name == QStringLiteral("Route"))
        {
            if (!m_routeParser->parse(reader))
                return false;
            auto result = m_routeParser->result();
            m_result->routeList.append(result);
        }
        else if (name == QStringLiteral("Datum"))
        {
            if (!m_datumParser->parse(reader))
                return false;
            auto result = m_datumParser->result();
            m_result->datum = result;
        }
        else if (name == QStringLiteral("Profile"))
        {
            if (!m_profileParser->parse(reader))
                return false;
            auto result = m_profileParser->result();
            m_result->profile = result;
        }
        else if (name == QStringLiteral("StepRepeat"))
        {
            if (!m_stepRepeatParser->parse(reader))
                return false;
            auto result = m_stepRepeatParser->result();
            m_result->stepRepeatList.append(result);
        }
        else if (name == QStringLiteral("Package"))
        {
            if (!m_packageParser->parse(reader))
                return false;
            auto result = m_packageParser->result();
            m_result->packageList.append(result);
        }
        else if (name == QStringLiteral("Component"))
        {
            if (!m_componentParser->parse(reader))
                return false;
            auto result = m_componentParser->result();
            m_result->componentList.append(result);
        }
        else if (name == QStringLiteral("LogicalNet"))
        {
            if (!m_logicalNetParser->parse(reader))
                return false;
            auto result = m_logicalNetParser->result();
            m_result->logicalNetList.append(result);
        }
        else if (name == QStringLiteral("PhyNetGroup"))
        {
            if (!m_phyNetGroupParser->parse(reader))
                return false;
            auto result = m_phyNetGroupParser->result();
            m_result->phyNetGroupList.append(result);
        }
        else if (name == QStringLiteral("LayerFeature"))
        {
            if (!m_layerFeatureParser->parse(reader))
                return false;
            auto result = m_layerFeatureParser->result();
            m_result->layerFeatureList.append(result);
        }
        else if (name == QStringLiteral("DfxMeasurementList"))
        {
            if (!m_dfxMeasurementListParser->parse(reader))
                return false;
            auto result = m_dfxMeasurementListParser->result();
            m_result->dfxMeasurementListList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Step *StepParser::result()
{
    return m_result.take();
}

}