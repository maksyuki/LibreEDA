
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "LogisticHeader.h"

namespace Ipc2581b
{

class PersonParser;
class RoleParser;
class EnterpriseParser;

class LogisticHeaderParser
{
public:
    LogisticHeaderParser(
            RoleParser*&
            , EnterpriseParser*&
            , PersonParser*&
            );

    bool parse(QXmlStreamReader *reader);
    LogisticHeader *result();

private:
    RoleParser *&m_roleParser;
    EnterpriseParser *&m_enterpriseParser;
    PersonParser *&m_personParser;
    QScopedPointer<LogisticHeader> m_result;
};

}