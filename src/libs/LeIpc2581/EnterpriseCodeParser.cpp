#include "EnterpriseCodeParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

EnterpriseCodeParser::EnterpriseCodeParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool EnterpriseCodeParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, EnterpriseCode> map =
    {
        {QStringLiteral("CAGE"), EnterpriseCode::Cage},
        {QStringLiteral("DUNNS"), EnterpriseCode::Dunns},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum EnterpriseCode").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

EnterpriseCode EnterpriseCodeParser::result()
{
    return m_result;
}

}