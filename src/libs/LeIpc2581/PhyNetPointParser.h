
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PhyNetPoint.h"

namespace Ipc2581b
{

class XformParser;
class QStringParser;
class NetPointParser;
class ExposureParser;
class FeatureParser;
class DoubleParser;
class BoolParser;

class PhyNetPointParser
{
public:
    PhyNetPointParser(
            DoubleParser*&
            , DoubleParser*&
            , QStringParser*&
            , QStringParser*&
            , NetPointParser*&
            , ExposureParser*&
            , QStringParser*&
            , QStringParser*&
            , BoolParser*&
            , BoolParser*&
            , BoolParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            , FeatureParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PhyNetPoint *result();

private:
    DoubleParser *&m_xParser;
    DoubleParser *&m_yParser;
    QStringParser *&m_layerRefParser;
    QStringParser *&m_secondaryLayerRefParser;
    NetPointParser *&m_netNodeParser;
    ExposureParser *&m_exposureParser;
    QStringParser *&m_layerIndexParser;
    QStringParser *&m_commentParser;
    BoolParser *&m_viaParser;
    BoolParser *&m_fiducialParser;
    BoolParser *&m_testParser;
    DoubleParser *&m_staggerXParser;
    DoubleParser *&m_staggerYParser;
    DoubleParser *&m_staggerRadiusParser;
    XformParser *&m_xformParser;
    FeatureParser *&m_featureParser;
    QScopedPointer<PhyNetPoint> m_result;
};

}