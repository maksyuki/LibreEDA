
#pragma once

#include <QList>
#include "Optional.h"


#include "BomCategory.h"
#include "BomDes.h"
#include "Characteristics.h"
#include "Int.h"
#include "QString.h"

namespace Ipc2581b
{

class BomItem
{
public:
	virtual ~BomItem() {}

    QString oEMDesignNumberRef;
    QString quantity;
    Optional<Int> pinCountOptional;
    BomCategory category;
    Optional<QString> internalPartNumberOptional;
    Optional<QString> descriptionOptional;
    QList<BomDes*> bomDesList;
    Characteristics *characteristics;

};

}