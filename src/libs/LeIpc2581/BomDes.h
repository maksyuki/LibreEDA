
#pragma once


namespace Ipc2581b
{

class DocDes;
class MatDes;
class RefDes;
class ToolDes;

class BomDes
{
public:
	virtual ~BomDes() {}

    enum class BomDesType
    {
        DocDes
        ,MatDes
        ,RefDes
        ,ToolDes
    };

    virtual BomDesType bomDesType() const = 0;

    inline bool isDocDes() const
    {
        return bomDesType() == BomDesType::DocDes;
    }

    inline DocDes *toDocDes()
    {
        return reinterpret_cast<DocDes*>(this);
    }

    inline const DocDes *toDocDes() const
    {
        return reinterpret_cast<const DocDes*>(this);
    }

    inline bool isMatDes() const
    {
        return bomDesType() == BomDesType::MatDes;
    }

    inline MatDes *toMatDes()
    {
        return reinterpret_cast<MatDes*>(this);
    }

    inline const MatDes *toMatDes() const
    {
        return reinterpret_cast<const MatDes*>(this);
    }

    inline bool isRefDes() const
    {
        return bomDesType() == BomDesType::RefDes;
    }

    inline RefDes *toRefDes()
    {
        return reinterpret_cast<RefDes*>(this);
    }

    inline const RefDes *toRefDes() const
    {
        return reinterpret_cast<const RefDes*>(this);
    }

    inline bool isToolDes() const
    {
        return bomDesType() == BomDesType::ToolDes;
    }

    inline ToolDes *toToolDes()
    {
        return reinterpret_cast<ToolDes*>(this);
    }

    inline const ToolDes *toToolDes() const
    {
        return reinterpret_cast<const ToolDes*>(this);
    }


};

}

// For user convenience
#include "DocDes.h"
#include "MatDes.h"
#include "RefDes.h"
#include "ToolDes.h"
