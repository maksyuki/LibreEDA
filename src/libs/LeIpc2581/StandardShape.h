
#pragma once

#include "Feature.h"

namespace Ipc2581b
{

class StandardPrimitiveRef;
class StandardPrimitive;

class StandardShape: public Feature
{
public:
	virtual ~StandardShape() {}

    enum class StandardShapeType
    {
        StandardPrimitiveRef
        ,StandardPrimitive
    };

    virtual StandardShapeType standardShapeType() const = 0;

    inline bool isStandardPrimitiveRef() const
    {
        return standardShapeType() == StandardShapeType::StandardPrimitiveRef;
    }

    inline StandardPrimitiveRef *toStandardPrimitiveRef()
    {
        return reinterpret_cast<StandardPrimitiveRef*>(this);
    }

    inline const StandardPrimitiveRef *toStandardPrimitiveRef() const
    {
        return reinterpret_cast<const StandardPrimitiveRef*>(this);
    }

    inline bool isStandardPrimitive() const
    {
        return standardShapeType() == StandardShapeType::StandardPrimitive;
    }

    inline StandardPrimitive *toStandardPrimitive()
    {
        return reinterpret_cast<StandardPrimitive*>(this);
    }

    inline const StandardPrimitive *toStandardPrimitive() const
    {
        return reinterpret_cast<const StandardPrimitive*>(this);
    }

    virtual FeatureType featureType() const override
    {
        return FeatureType::StandardShape;
    }

};

}

// For user convenience
#include "StandardPrimitiveRef.h"
#include "StandardPrimitive.h"
