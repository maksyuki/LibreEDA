
#pragma once

namespace Ipc2581b
{

enum class BoardTechnology
{
    Hdi,
    EmbeddedComponent,
    Flex,
    Rigid,
    Other,
    RigidFlex,
};

}