
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Thermal.h"

namespace Ipc2581b
{

class XformParser;
class LineDescGroupParser;
class DoubleParser;
class ThermalShapeParser;
class FillDescGroupParser;
class IntParser;

class ThermalParser
{
public:
    ThermalParser(
            ThermalShapeParser*&
            , DoubleParser*&
            , DoubleParser*&
            , IntParser*&
            , DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Thermal *result();

private:
    ThermalShapeParser *&m_shapeParser;
    DoubleParser *&m_outerDiameterParser;
    DoubleParser *&m_innerDiameterParser;
    IntParser *&m_spokeCountParser;
    DoubleParser *&m_gapParser;
    DoubleParser *&m_spokeStartAngleParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Thermal> m_result;
};

}