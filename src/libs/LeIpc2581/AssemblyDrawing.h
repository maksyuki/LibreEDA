
#pragma once

#include <QList>
#include "Optional.h"


#include "Outline.h"
#include "Marking.h"

namespace Ipc2581b
{

class AssemblyDrawing
{
public:
	virtual ~AssemblyDrawing() {}

    Outline *outline;
    QList<Marking*> markingList;

};

}