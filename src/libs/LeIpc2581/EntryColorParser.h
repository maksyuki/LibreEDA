
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "EntryColor.h"

namespace Ipc2581b
{

class ColorParser;
class QStringParser;

class EntryColorParser
{
public:
    EntryColorParser(
            QStringParser*&
            , ColorParser*&
            );

    bool parse(QXmlStreamReader *reader);
    EntryColor *result();

private:
    QStringParser *&m_idParser;
    ColorParser *&m_colorParser;
    QScopedPointer<EntryColor> m_result;
};

}