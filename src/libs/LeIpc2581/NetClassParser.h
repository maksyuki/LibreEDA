
#pragma once

#include <QString>

class QXmlStreamReader;

#include "NetClass.h"

namespace Ipc2581b
{

class NetClassParser
{
public:
    NetClassParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    NetClass result();

private:
    NetClass m_result;
};

}