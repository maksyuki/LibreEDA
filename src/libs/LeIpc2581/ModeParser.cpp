#include "ModeParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ModeParser::ModeParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ModeParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, Mode> map =
    {
        {QStringLiteral("USERDEF"), Mode::Userdef},
        {QStringLiteral("ASSEMBLY"), Mode::Assembly},
        {QStringLiteral("FABRICATION"), Mode::Fabrication},
        {QStringLiteral("DESIGN"), Mode::Design},
        {QStringLiteral("TEST"), Mode::Test},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum Mode").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

Mode ModeParser::result()
{
    return m_result;
}

}