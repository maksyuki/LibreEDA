
#pragma once

#include <QList>
#include "Optional.h"


#include "IsoCode.h"
#include "EnterpriseCode.h"
#include "QString.h"

namespace Ipc2581b
{

class Enterprise
{
public:
	virtual ~Enterprise() {}

    QString id;
    Optional<QString> nameOptional;
    QString code;
    Optional<EnterpriseCode> codeTypeOptional;
    Optional<QString> address1Optional;
    Optional<QString> address2Optional;
    Optional<QString> cityOptional;
    Optional<QString> stateProvinceOptional;
    Optional<IsoCode> countryOptional;
    Optional<QString> postalCodeOptional;
    Optional<QString> phoneOptional;
    Optional<QString> faxOptional;
    Optional<QString> emailOptional;
    Optional<QString> urlOptional;

};

}