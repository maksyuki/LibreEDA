
#pragma once

#include <QList>
#include "Optional.h"


#include "Xform.h"
#include "QString.h"
#include "Mount.h"
#include "Double.h"
#include "NonstandardAttribute.h"
#include "Location.h"

namespace Ipc2581b
{

class Component
{
public:
	virtual ~Component() {}

    QString refDes;
    QString packageRef;
    QString part;
    QString layerRef;
    Mount mountType;
    Optional<Double> weightOptional;
    Optional<Double> heightOptional;
    Optional<Double> standoffOptional;
    QList<NonstandardAttribute*> nonstandardAttributeList;
    Optional<Xform*> xformOptional;
    Location *location;

};

}