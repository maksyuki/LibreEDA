#include "ComponentParser.h"

#include "XformParser.h"
#include "QStringParser.h"
#include "MountParser.h"
#include "DoubleParser.h"
#include "NonstandardAttributeParser.h"
#include "LocationParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

ComponentParser::ComponentParser(
    QStringParser *&_refDesParser
    , QStringParser *&_packageRefParser
    , QStringParser *&_partParser
    , QStringParser *&_layerRefParser
    , MountParser *&_mountTypeParser
    , DoubleParser *&_weightParser
    , DoubleParser *&_heightParser
    , DoubleParser *&_standoffParser
    , NonstandardAttributeParser *&_nonstandardAttributeParser
    , XformParser *&_xformParser
    , LocationParser *&_locationParser
):    m_refDesParser(_refDesParser)
    , m_packageRefParser(_packageRefParser)
    , m_partParser(_partParser)
    , m_layerRefParser(_layerRefParser)
    , m_mountTypeParser(_mountTypeParser)
    , m_weightParser(_weightParser)
    , m_heightParser(_heightParser)
    , m_standoffParser(_standoffParser)
    , m_nonstandardAttributeParser(_nonstandardAttributeParser)
    , m_xformParser(_xformParser)
    , m_locationParser(_locationParser)
{

}

bool ComponentParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Component());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("refDes")))
    {
        data = reader->attributes().value(QStringLiteral("refDes"));
        if (!m_refDesParser->parse(reader, data))
            return false;
        m_result->refDes = m_refDesParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("refDes: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("packageRef")))
    {
        data = reader->attributes().value(QStringLiteral("packageRef"));
        if (!m_packageRefParser->parse(reader, data))
            return false;
        m_result->packageRef = m_packageRefParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("packageRef: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("part")))
    {
        data = reader->attributes().value(QStringLiteral("part"));
        if (!m_partParser->parse(reader, data))
            return false;
        m_result->part = m_partParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("part: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("layerRef")))
    {
        data = reader->attributes().value(QStringLiteral("layerRef"));
        if (!m_layerRefParser->parse(reader, data))
            return false;
        m_result->layerRef = m_layerRefParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("layerRef: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("mountType")))
    {
        data = reader->attributes().value(QStringLiteral("mountType"));
        if (!m_mountTypeParser->parse(reader, data))
            return false;
        m_result->mountType = m_mountTypeParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("mountType: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("weight")))
    {
        data = reader->attributes().value(QStringLiteral("weight"));
        if (!m_weightParser->parse(reader, data))
            return false;
        m_result->weightOptional = Optional<Double>(m_weightParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("height")))
    {
        data = reader->attributes().value(QStringLiteral("height"));
        if (!m_heightParser->parse(reader, data))
            return false;
        m_result->heightOptional = Optional<Double>(m_heightParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("standoff")))
    {
        data = reader->attributes().value(QStringLiteral("standoff"));
        if (!m_standoffParser->parse(reader, data))
            return false;
        m_result->standoffOptional = Optional<Double>(m_standoffParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("NonstandardAttribute"))
        {
            if (!m_nonstandardAttributeParser->parse(reader))
                return false;
            auto result = m_nonstandardAttributeParser->result();
            m_result->nonstandardAttributeList.append(result);
        }
        else if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (name == QStringLiteral("Location"))
        {
            if (!m_locationParser->parse(reader))
                return false;
            auto result = m_locationParser->result();
            m_result->location = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Component *ComponentParser::result()
{
    return m_result.take();
}

}