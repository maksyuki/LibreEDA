#include "ContourParser.h"

#include "PolygonParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

ContourParser::ContourParser(
    PolygonParser *&_polygonParser
    , PolygonParser *&_cutoutParser
):    m_polygonParser(_polygonParser)
    , m_cutoutParser(_cutoutParser)
{

}

bool ContourParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Contour());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Polygon"))
        {
            if (!m_polygonParser->parse(reader))
                return false;
            auto result = m_polygonParser->result();
            m_result->polygon = result;
        }
        else if (name == QStringLiteral("Cutout"))
        {
            if (!m_cutoutParser->parse(reader))
                return false;
            auto result = m_cutoutParser->result();
            m_result->cutoutList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Contour *ContourParser::result()
{
    return m_result.take();
}

}