
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Glyph.h"

namespace Ipc2581b
{

class SimpleParser;
class DoubleParser;
class QStringParser;

class GlyphParser
{
public:
    GlyphParser(
            QStringParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , SimpleParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Glyph *result();

private:
    QStringParser *&m_charCodeParser;
    DoubleParser *&m_lowerLeftXParser;
    DoubleParser *&m_lowerLeftYParser;
    DoubleParser *&m_upperRightXParser;
    DoubleParser *&m_upperRightYParser;
    SimpleParser *&m_simpleParser;
    QScopedPointer<Glyph> m_result;
};

}