
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "UserPrimitiveRef.h"

namespace Ipc2581b
{

class QStringParser;

class UserPrimitiveRefParser
{
public:
    UserPrimitiveRefParser(
            QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    UserPrimitiveRef *result();

private:
    QStringParser *&m_idParser;
    QScopedPointer<UserPrimitiveRef> m_result;
};

}