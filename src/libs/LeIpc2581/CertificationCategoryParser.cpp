#include "CertificationCategoryParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

CertificationCategoryParser::CertificationCategoryParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool CertificationCategoryParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, CertificationCategory> map =
    {
        {QStringLiteral("FABRICATIONDRAWING"), CertificationCategory::Fabricationdrawing},
        {QStringLiteral("GENERALASSEMBLY"), CertificationCategory::Generalassembly},
        {QStringLiteral("GLUEDOT"), CertificationCategory::Gluedot},
        {QStringLiteral("EMBEDDEDCOMPONENT"), CertificationCategory::Embeddedcomponent},
        {QStringLiteral("DETAILEDDRAWING"), CertificationCategory::Detaileddrawing},
        {QStringLiteral("SPECSOURCECONTROLDRAWING"), CertificationCategory::Specsourcecontroldrawing},
        {QStringLiteral("ASSEMBLYPANEL"), CertificationCategory::Assemblypanel},
        {QStringLiteral("COMPONENTPLACEMENT"), CertificationCategory::Componentplacement},
        {QStringLiteral("OTHER"), CertificationCategory::Other},
        {QStringLiteral("ASSEMBLYTESTGENERATION"), CertificationCategory::Assemblytestgeneration},
        {QStringLiteral("MECHANICALHARDWARE"), CertificationCategory::Mechanicalhardware},
        {QStringLiteral("MULTIBOARDPARTSLIST"), CertificationCategory::Multiboardpartslist},
        {QStringLiteral("BOARDFABRICATION"), CertificationCategory::Boardfabrication},
        {QStringLiteral("BOARDPANEL"), CertificationCategory::Boardpanel},
        {QStringLiteral("SINGLEBOARDPARTSLIST"), CertificationCategory::Singleboardpartslist},
        {QStringLiteral("ASSEMBLYTESTFIXTUREGENERATION"), CertificationCategory::Assemblytestfixturegeneration},
        {QStringLiteral("ASSEMBLYDRAWING"), CertificationCategory::Assemblydrawing},
        {QStringLiteral("SOLDERSTENCILPASTE"), CertificationCategory::Solderstencilpaste},
        {QStringLiteral("PHOTOTOOLS"), CertificationCategory::Phototools},
        {QStringLiteral("BOARDTESTGENERATION"), CertificationCategory::Boardtestgeneration},
        {QStringLiteral("ASSEMBLYFIXTUREGENERATION"), CertificationCategory::Assemblyfixturegeneration},
        {QStringLiteral("SCHEMATICDRAWINGS"), CertificationCategory::Schematicdrawings},
        {QStringLiteral("ASSEMBLYPREPTOOLS"), CertificationCategory::Assemblypreptools},
        {QStringLiteral("BOARDFIXTUREGENERATION"), CertificationCategory::Boardfixturegeneration},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum CertificationCategory").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

CertificationCategory CertificationCategoryParser::result()
{
    return m_result;
}

}