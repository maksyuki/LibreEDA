#include "LandPatternParser.h"

#include "PadParser.h"
#include "TargetParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

LandPatternParser::LandPatternParser(
    PadParser *&_padParser
    , TargetParser *&_targetParser
):    m_padParser(_padParser)
    , m_targetParser(_targetParser)
{

}

bool LandPatternParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new LandPattern());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Pad"))
        {
            if (!m_padParser->parse(reader))
                return false;
            auto result = m_padParser->result();
            m_result->padList.append(result);
        }
        else if (name == QStringLiteral("Target"))
        {
            if (!m_targetParser->parse(reader))
                return false;
            auto result = m_targetParser->result();
            m_result->targetList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

LandPattern *LandPatternParser::result()
{
    return m_result.take();
}

}