
#pragma once

#include <QList>
#include "Optional.h"


#include "Outline.h"
#include "Marking.h"

namespace Ipc2581b
{

class SilkScreen
{
public:
	virtual ~SilkScreen() {}

    QList<Outline*> outlineList;
    QList<Marking*> markingList;

};

}