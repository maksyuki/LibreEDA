#include "UserPrimitiveParser.h"

#include "SimpleParser.h"
#include "UserSpecialParser.h"
#include "TextParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

UserPrimitiveParser::UserPrimitiveParser(
    SimpleParser *&_simpleParser
    , TextParser *&_textParser
    , UserSpecialParser *&_userSpecialParser
):    m_simpleParser(_simpleParser)
    , m_textParser(_textParser)
    , m_userSpecialParser(_userSpecialParser)
{

}

bool UserPrimitiveParser::parse(QXmlStreamReader *reader)
{
    if (m_simpleParser->isSubstitution(reader->name()))
    {
        if(m_simpleParser->parse(reader))
        {
            m_result = m_simpleParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Text"))
    {
        if(m_textParser->parse(reader))
        {
            m_result = m_textParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("UserSpecial"))
    {
        if(m_userSpecialParser->parse(reader))
        {
            m_result = m_userSpecialParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"UserPrimitive\"").arg(reader->name().toString()));
    return false;
}

UserPrimitive *UserPrimitiveParser::result()
{
    return m_result;
}

bool UserPrimitiveParser::isSubstitution(const QStringRef &name) const
{
    if (m_simpleParser->isSubstitution(name))
        return true;
    if (name == QStringLiteral("Text"))
        return true;
    if (name == QStringLiteral("UserSpecial"))
        return true;
    return false;
}

}