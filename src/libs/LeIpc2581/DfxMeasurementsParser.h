
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "DfxMeasurements.h"

namespace Ipc2581b
{


class DfxMeasurementsParser
{
public:
    DfxMeasurementsParser(
            );

    bool parse(QXmlStreamReader *reader);
    DfxMeasurements *result();

private:
    QScopedPointer<DfxMeasurements> m_result;
};

}