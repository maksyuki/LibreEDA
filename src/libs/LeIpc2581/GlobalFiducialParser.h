
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "GlobalFiducial.h"

namespace Ipc2581b
{

class LocationParser;
class StandardShapeParser;
class XformParser;

class GlobalFiducialParser
{
public:
    GlobalFiducialParser(
            XformParser*&
            , LocationParser*&
            , StandardShapeParser*&
            );

    bool parse(QXmlStreamReader *reader);
    GlobalFiducial *result();

private:
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    StandardShapeParser *&m_standardShapeParser;
    QScopedPointer<GlobalFiducial> m_result;
};

}