#include "PhyNetParser.h"

#include "PhyNetPointParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

PhyNetParser::PhyNetParser(
    QStringParser *&_nameParser
    , PhyNetPointParser *&_phyNetPointParser
):    m_nameParser(_nameParser)
    , m_phyNetPointParser(_phyNetPointParser)
{

}

bool PhyNetParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new PhyNet());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->name = m_nameParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("name: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("PhyNetPoint"))
        {
            if (!m_phyNetPointParser->parse(reader))
                return false;
            auto result = m_phyNetPointParser->result();
            m_result->phyNetPointList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

PhyNet *PhyNetParser::result()
{
    return m_result.take();
}

}