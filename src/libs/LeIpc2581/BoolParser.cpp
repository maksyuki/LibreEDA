#include "BoolParser.h"
#include <QXmlStreamReader>

namespace Ipc2581b
{

BoolParser::BoolParser()
{

}

bool BoolParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    Q_UNUSED(reader);
    if (data.toString().toLower() == QStringLiteral("true") || data == QStringLiteral("1"))
    {
        m_result = true;
        return true;
    }
    if (data.toString().toLower() == QStringLiteral("false") || data == QStringLiteral("0"))
    {
        m_result = false;
        return true;
    }
    reader->raiseError(QString("\"%1\": Invalid value for a boolean").arg(data.toString()));
    return false;
}

bool BoolParser::result()
{
    return m_result;
}

}
