
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardPrimitive.h"

#include "LineDescGroup.h"
#include "FillDescGroup.h"
#include "Xform.h"
#include "Double.h"

namespace Ipc2581b
{

class Circle: public StandardPrimitive
{
public:
	virtual ~Circle() {}

    Double diameter;
    Optional<Xform*> xformOptional;
    Optional<LineDescGroup*> lineDescGroupOptional;
    Optional<FillDescGroup*> fillDescGroupOptional;

    virtual StandardPrimitiveType standardPrimitiveType() const override
    {
        return StandardPrimitiveType::Circle;
    }
};

}