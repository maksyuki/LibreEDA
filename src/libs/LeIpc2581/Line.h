
#pragma once

#include <QList>
#include "Optional.h"

#include "Simple.h"

#include "LineDescGroup.h"
#include "Double.h"

namespace Ipc2581b
{

class Line: public Simple
{
public:
	virtual ~Line() {}

    Double startX;
    Double startY;
    Double endX;
    Double endY;
    LineDescGroup *lineDescGroup;

    virtual SimpleType simpleType() const override
    {
        return SimpleType::Line;
    }
};

}