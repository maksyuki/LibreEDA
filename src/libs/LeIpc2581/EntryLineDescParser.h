
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "EntryLineDesc.h"

namespace Ipc2581b
{

class LineDescParser;
class QStringParser;

class EntryLineDescParser
{
public:
    EntryLineDescParser(
            QStringParser*&
            , LineDescParser*&
            );

    bool parse(QXmlStreamReader *reader);
    EntryLineDesc *result();

private:
    QStringParser *&m_idParser;
    LineDescParser *&m_lineDescParser;
    QScopedPointer<EntryLineDesc> m_result;
};

}