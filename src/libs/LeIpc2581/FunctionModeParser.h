
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "FunctionMode.h"

namespace Ipc2581b
{

class QStringParser;
class IntParser;
class ModeParser;

class FunctionModeParser
{
public:
    FunctionModeParser(
            ModeParser*&
            , IntParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    FunctionMode *result();

private:
    ModeParser *&m_modeParser;
    IntParser *&m_levelParser;
    QStringParser *&m_commentParser;
    QScopedPointer<FunctionMode> m_result;
};

}