
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Butterfly.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class ButterflyShapeParser;
class XformParser;
class DoubleParser;

class ButterflyParser
{
public:
    ButterflyParser(
            ButterflyShapeParser*&
            , DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Butterfly *result();

private:
    ButterflyShapeParser *&m_shapeParser;
    DoubleParser *&m_diameterParser;
    DoubleParser *&m_sideParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Butterfly> m_result;
};

}