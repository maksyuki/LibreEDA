#include "PlatingStatusParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

PlatingStatusParser::PlatingStatusParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool PlatingStatusParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, PlatingStatus> map =
    {
        {QStringLiteral("VIA"), PlatingStatus::Via},
        {QStringLiteral("NONPLATED"), PlatingStatus::Nonplated},
        {QStringLiteral("PLATED"), PlatingStatus::Plated},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum PlatingStatus").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

PlatingStatus PlatingStatusParser::result()
{
    return m_result;
}

}