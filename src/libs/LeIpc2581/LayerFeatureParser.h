
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "LayerFeature.h"

namespace Ipc2581b
{

class SetParser;
class QStringParser;

class LayerFeatureParser
{
public:
    LayerFeatureParser(
            QStringParser*&
            , SetParser*&
            );

    bool parse(QXmlStreamReader *reader);
    LayerFeature *result();

private:
    QStringParser *&m_layerRefParser;
    SetParser *&m_setParser;
    QScopedPointer<LayerFeature> m_result;
};

}