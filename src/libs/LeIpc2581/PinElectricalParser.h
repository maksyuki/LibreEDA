
#pragma once

#include <QString>

class QXmlStreamReader;

#include "PinElectrical.h"

namespace Ipc2581b
{

class PinElectricalParser
{
public:
    PinElectricalParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    PinElectrical result();

private:
    PinElectrical m_result;
};

}