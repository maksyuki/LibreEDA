#include "ZAxisDimParser.h"

#include "MaterialLeftParser.h"
#include "MaterialCutParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

ZAxisDimParser::ZAxisDimParser(
    MaterialCutParser *&_materialCutParser
    , MaterialLeftParser *&_materialLeftParser
):    m_materialCutParser(_materialCutParser)
    , m_materialLeftParser(_materialLeftParser)
{

}

bool ZAxisDimParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("MaterialCut"))
    {
        if(m_materialCutParser->parse(reader))
        {
            m_result = m_materialCutParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("MaterialLeft"))
    {
        if(m_materialLeftParser->parse(reader))
        {
            m_result = m_materialLeftParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"ZAxisDim\"").arg(reader->name().toString()));
    return false;
}

ZAxisDim *ZAxisDimParser::result()
{
    return m_result;
}

bool ZAxisDimParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("MaterialCut"))
        return true;
    if (name == QStringLiteral("MaterialLeft"))
        return true;
    return false;
}

}