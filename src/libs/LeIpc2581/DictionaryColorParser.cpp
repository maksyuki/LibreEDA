#include "DictionaryColorParser.h"

#include "EntryColorParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

DictionaryColorParser::DictionaryColorParser(
    EntryColorParser *&_entryColorParser
):    m_entryColorParser(_entryColorParser)
{

}

bool DictionaryColorParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new DictionaryColor());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("EntryColor"))
        {
            if (!m_entryColorParser->parse(reader))
                return false;
            auto result = m_entryColorParser->result();
            m_result->entryColorList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

DictionaryColor *DictionaryColorParser::result()
{
    return m_result.take();
}

}