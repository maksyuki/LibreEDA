
#pragma once

namespace Ipc2581b
{

enum class TechnologyList
{
    Hdi,
    EmbeddedComponent,
    Flex,
    Rigid,
    Other,
    RigidFlex,
};

}