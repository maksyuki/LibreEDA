
#pragma once

#include <QList>
#include "Optional.h"

#include "FontDef.h"

#include "QString.h"

namespace Ipc2581b
{

class FontDefExternal: public FontDef
{
public:
	virtual ~FontDefExternal() {}

    QString name;
    QString urn;

    virtual FontDefType fontDefType() const override
    {
        return FontDefType::FontDefExternal;
    }
};

}