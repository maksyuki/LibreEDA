
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Enterprise.h"

namespace Ipc2581b
{

class IsoCodeParser;
class EnterpriseCodeParser;
class QStringParser;

class EnterpriseParser
{
public:
    EnterpriseParser(
            QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , EnterpriseCodeParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , IsoCodeParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Enterprise *result();

private:
    QStringParser *&m_idParser;
    QStringParser *&m_nameParser;
    QStringParser *&m_codeParser;
    EnterpriseCodeParser *&m_codeTypeParser;
    QStringParser *&m_address1Parser;
    QStringParser *&m_address2Parser;
    QStringParser *&m_cityParser;
    QStringParser *&m_stateProvinceParser;
    IsoCodeParser *&m_countryParser;
    QStringParser *&m_postalCodeParser;
    QStringParser *&m_phoneParser;
    QStringParser *&m_faxParser;
    QStringParser *&m_emailParser;
    QStringParser *&m_urlParser;
    QScopedPointer<Enterprise> m_result;
};

}