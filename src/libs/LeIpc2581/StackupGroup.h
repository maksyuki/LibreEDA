
#pragma once

#include <QList>
#include "Optional.h"


#include "StackupLayer.h"
#include "IdRef.h"
#include "QString.h"
#include "MatDes.h"
#include "CADDataLayerRef.h"
#include "Double.h"

namespace Ipc2581b
{

class StackupGroup
{
public:
	virtual ~StackupGroup() {}

    QString name;
    Double thickness;
    Double tolPlus;
    Double tolMinus;
    Optional<QString> commentOptional;
    Optional<MatDes*> matDesOptional;
    QList<IdRef*> specRefList;
    QList<StackupLayer*> stackupLayerList;
    QList<CADDataLayerRef*> cADDataLayerRefList;

};

}