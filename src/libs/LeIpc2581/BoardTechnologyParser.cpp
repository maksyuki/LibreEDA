#include "BoardTechnologyParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

BoardTechnologyParser::BoardTechnologyParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool BoardTechnologyParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, BoardTechnology> map =
    {
        {QStringLiteral("HDI"), BoardTechnology::Hdi},
        {QStringLiteral("EMBEDDED_COMPONENT"), BoardTechnology::EmbeddedComponent},
        {QStringLiteral("FLEX"), BoardTechnology::Flex},
        {QStringLiteral("RIGID"), BoardTechnology::Rigid},
        {QStringLiteral("OTHER"), BoardTechnology::Other},
        {QStringLiteral("RIGID_FLEX"), BoardTechnology::RigidFlex},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum BoardTechnology").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

BoardTechnology BoardTechnologyParser::result()
{
    return m_result;
}

}