
#pragma once

#include <QList>
#include "Optional.h"


#include "EntryColor.h"

namespace Ipc2581b
{

class DictionaryColor
{
public:
	virtual ~DictionaryColor() {}

    QList<EntryColor*> entryColorList;

};

}