#include "DictionaryFirmwareParser.h"


#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

DictionaryFirmwareParser::DictionaryFirmwareParser(
){

}

bool DictionaryFirmwareParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new DictionaryFirmware());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

DictionaryFirmware *DictionaryFirmwareParser::result()
{
    return m_result.take();
}

}