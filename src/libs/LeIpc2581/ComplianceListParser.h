
#pragma once

#include <QString>

class QXmlStreamReader;

#include "ComplianceList.h"

namespace Ipc2581b
{

class ComplianceListParser
{
public:
    ComplianceListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    ComplianceList result();

private:
    ComplianceList m_result;
};

}