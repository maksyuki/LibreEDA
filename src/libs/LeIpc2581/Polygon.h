
#pragma once

#include <QList>
#include "Optional.h"


#include "LineDescGroup.h"
#include "Xform.h"
#include "FillDescGroup.h"
#include "PolyBegin.h"
#include "PolyStep.h"

namespace Ipc2581b
{

class Polygon
{
public:
	virtual ~Polygon() {}

    PolyBegin *polyBegin;
    QList<PolyStep*> polyStepList;
    Optional<Xform*> xformOptional;
    Optional<LineDescGroup*> lineDescGroupOptional;
    Optional<FillDescGroup*> fillDescGroupOptional;

};

}