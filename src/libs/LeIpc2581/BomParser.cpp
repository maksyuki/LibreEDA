#include "BomParser.h"

#include "BomItemParser.h"
#include "BomHeaderParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

BomParser::BomParser(
    QStringParser *&_nameParser
    , BomHeaderParser *&_bomHeaderParser
    , BomItemParser *&_bomItemParser
):    m_nameParser(_nameParser)
    , m_bomHeaderParser(_bomHeaderParser)
    , m_bomItemParser(_bomItemParser)
{

}

bool BomParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Bom());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->name = m_nameParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("name: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("BomHeader"))
        {
            if (!m_bomHeaderParser->parse(reader))
                return false;
            auto result = m_bomHeaderParser->result();
            m_result->bomHeader = result;
        }
        else if (name == QStringLiteral("BomItem"))
        {
            if (!m_bomItemParser->parse(reader))
                return false;
            auto result = m_bomItemParser->result();
            m_result->bomItemList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Bom *BomParser::result()
{
    return m_result.take();
}

}