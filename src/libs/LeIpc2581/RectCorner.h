
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardPrimitive.h"

#include "LineDescGroup.h"
#include "FillDescGroup.h"
#include "Xform.h"
#include "Double.h"

namespace Ipc2581b
{

class RectCorner: public StandardPrimitive
{
public:
	virtual ~RectCorner() {}

    Double lowerLeftX;
    Double lowerLeftY;
    Double upperRightX;
    Double upperRightY;
    Optional<Xform*> xformOptional;
    Optional<LineDescGroup*> lineDescGroupOptional;
    Optional<FillDescGroup*> fillDescGroupOptional;

    virtual StandardPrimitiveType standardPrimitiveType() const override
    {
        return StandardPrimitiveType::RectCorner;
    }
};

}