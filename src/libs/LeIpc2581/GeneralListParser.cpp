#include "GeneralListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

GeneralListParser::GeneralListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool GeneralListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, GeneralList> map =
    {
        {QStringLiteral("ELECTRICAL"), GeneralList::Electrical},
        {QStringLiteral("THERMAL"), GeneralList::Thermal},
        {QStringLiteral("MATERIAL"), GeneralList::Material},
        {QStringLiteral("OTHER"), GeneralList::Other},
        {QStringLiteral("INSTRUCTION"), GeneralList::Instruction},
        {QStringLiteral("STANDARD"), GeneralList::Standard},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum GeneralList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

GeneralList GeneralListParser::result()
{
    return m_result;
}

}