
#pragma once

#include <QList>
#include "Optional.h"

#include "SpecificationType.h"

#include "QString.h"
#include "Property.h"
#include "VCutList.h"

namespace Ipc2581b
{

class V_Cut: public SpecificationType
{
public:
	virtual ~V_Cut() {}

    VCutList type;
    Optional<QString> commentOptional;
    QList<Property*> propertyList;

    virtual SpecificationTypeType specificationTypeType() const override
    {
        return SpecificationTypeType::V_Cut;
    }
};

}