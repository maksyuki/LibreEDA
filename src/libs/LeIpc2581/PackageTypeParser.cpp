#include "PackageTypeParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

PackageTypeParser::PackageTypeParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool PackageTypeParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, PackageType> map =
    {
        {QStringLiteral("CHIP"), PackageType::Chip},
        {QStringLiteral("SOT143"), PackageType::Sot143},
        {QStringLiteral("FLIPCHIP"), PackageType::Flipchip},
        {QStringLiteral("TO_TYPE"), PackageType::ToType},
        {QStringLiteral("FINEPITCH_BGA"), PackageType::FinepitchBga},
        {QStringLiteral("NETWORK"), PackageType::Network},
        {QStringLiteral("OTHER"), PackageType::Other},
        {QStringLiteral("PLASTIC_SIP"), PackageType::PlasticSip},
        {QStringLiteral("RECTANGULAR_QUAD_FLATPACK"), PackageType::RectangularQuadFlatpack},
        {QStringLiteral("MOLDED"), PackageType::Molded},
        {QStringLiteral("SWITCH_TH"), PackageType::SwitchTh},
        {QStringLiteral("CERAMIC_DIP"), PackageType::CeramicDip},
        {QStringLiteral("BARE_DIE"), PackageType::BareDie},
        {QStringLiteral("SOT23"), PackageType::Sot23},
        {QStringLiteral("RELAY_SM"), PackageType::RelaySm},
        {QStringLiteral("TRANSFORMER"), PackageType::Transformer},
        {QStringLiteral("CERAMIC_SIP"), PackageType::CeramicSip},
        {QStringLiteral("CHIP_SCALE"), PackageType::ChipScale},
        {QStringLiteral("SQUARE_QUAD_FLATPACK"), PackageType::SquareQuadFlatpack},
        {QStringLiteral("SOPIC"), PackageType::Sopic},
        {QStringLiteral("EMBEDDED"), PackageType::Embedded},
        {QStringLiteral("SOIC"), PackageType::Soic},
        {QStringLiteral("SOD123"), PackageType::Sod123},
        {QStringLiteral("CERAMIC_QUAD_FLATPACK"), PackageType::CeramicQuadFlatpack},
        {QStringLiteral("TRIMPOT_TH"), PackageType::TrimpotTh},
        {QStringLiteral("SOJ"), PackageType::Soj},
        {QStringLiteral("MCM"), PackageType::Mcm},
        {QStringLiteral("SOT52"), PackageType::Sot52},
        {QStringLiteral("MELF"), PackageType::Melf},
        {QStringLiteral("TANTALUM"), PackageType::Tantalum},
        {QStringLiteral("SSOIC"), PackageType::Ssoic},
        {QStringLiteral("TRIMPOT_SM"), PackageType::TrimpotSm},
        {QStringLiteral("CERAMIC_BGA"), PackageType::CeramicBga},
        {QStringLiteral("POWER_TRANSISTOR"), PackageType::PowerTransistor},
        {QStringLiteral("RADIAL_LEADED"), PackageType::RadialLeaded},
        {QStringLiteral("COIL"), PackageType::Coil},
        {QStringLiteral("PGA"), PackageType::Pga},
        {QStringLiteral("CONNECTOR_SM"), PackageType::ConnectorSm},
        {QStringLiteral("CHOKE_SWITCH_SM"), PackageType::ChokeSwitchSm},
        {QStringLiteral("HERMETIC_HYBRED"), PackageType::HermeticHybred},
        {QStringLiteral("CONNECTOR_TH"), PackageType::ConnectorTh},
        {QStringLiteral("LEADLESS_CERAMIC_CHIP_CARRIER"), PackageType::LeadlessCeramicChipCarrier},
        {QStringLiteral("PLASTIC_CHIP_CARRIER"), PackageType::PlasticChipCarrier},
        {QStringLiteral("RELAY_TH"), PackageType::RelayTh},
        {QStringLiteral("PLASTIC_BGA"), PackageType::PlasticBga},
        {QStringLiteral("PLASTIC_DIP"), PackageType::PlasticDip},
        {QStringLiteral("CERAMIC_FLATPACK"), PackageType::CeramicFlatpack},
        {QStringLiteral("AXIAL_LEADED"), PackageType::AxialLeaded},
        {QStringLiteral("SOT89"), PackageType::Sot89},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum PackageType").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

PackageType PackageTypeParser::result()
{
    return m_result;
}

}