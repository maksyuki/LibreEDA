
#pragma once

#include <QList>
#include "Optional.h"


#include "Fiducial.h"
#include "ColorGroup.h"
#include "IdRef.h"
#include "QString.h"
#include "SlotCavity.h"
#include "Hole.h"
#include "Polarity.h"
#include "LineDescGroup.h"
#include "PadUsage.h"
#include "Pad.h"
#include "Bool.h"
#include "Features.h"
#include "NonstandardAttribute.h"

namespace Ipc2581b
{

class Set
{
public:
	virtual ~Set() {}

    Optional<QString> netOptional;
    Optional<Polarity> polarityOptional;
    Optional<PadUsage> padUsageOptional;
    Optional<Bool> testPointOptional;
    Optional<QString> geometryOptional;
    Optional<Bool> plateOptional;
    Optional<QString> componentRefOptional;
    QList<NonstandardAttribute*> nonstandardAttributeList;
    QList<Pad*> padList;
    QList<Fiducial*> fiducialList;
    QList<Hole*> holeList;
    QList<SlotCavity*> slotCavityList;
    QList<IdRef*> specRefList;
    QList<Features*> featuresList;
    QList<ColorGroup*> colorGroupList;
    QList<LineDescGroup*> lineDescGroupList;

};

}