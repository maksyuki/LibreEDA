
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Certification.h"

namespace Ipc2581b
{

class CertificationStatusParser;
class CertificationCategoryParser;

class CertificationParser
{
public:
    CertificationParser(
            CertificationStatusParser*&
            , CertificationStatusParser*&
            , CertificationCategoryParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Certification *result();

private:
    CertificationStatusParser *&m_certificationStatusParser;
    CertificationStatusParser *&m_certificationParser;
    CertificationCategoryParser *&m_certificationCategoryParser;
    QScopedPointer<Certification> m_result;
};

}