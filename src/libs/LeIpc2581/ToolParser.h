
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Tool.h"

namespace Ipc2581b
{

class QStringParser;
class ToolListParser;
class PropertyParser;
class ToolPropertyListParser;

class ToolParser
{
public:
    ToolParser(
            ToolListParser*&
            , ToolPropertyListParser*&
            , QStringParser*&
            , PropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Tool *result();

private:
    ToolListParser *&m_typeParser;
    ToolPropertyListParser *&m_toolPropertyParser;
    QStringParser *&m_commentParser;
    PropertyParser *&m_propertyParser;
    QScopedPointer<Tool> m_result;
};

}