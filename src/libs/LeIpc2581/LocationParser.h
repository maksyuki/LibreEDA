
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Location.h"

namespace Ipc2581b
{

class DoubleParser;

class LocationParser
{
public:
    LocationParser(
            DoubleParser*&
            , DoubleParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Location *result();

private:
    DoubleParser *&m_xParser;
    DoubleParser *&m_yParser;
    QScopedPointer<Location> m_result;
};

}