
#pragma once

#include <QList>
#include "Optional.h"


#include "IdRef.h"
#include "LayerFunction.h"
#include "Side.h"
#include "Polarity.h"
#include "QString.h"

namespace Ipc2581b
{

class Layer
{
public:
	virtual ~Layer() {}

    QString name;
    LayerFunction layerFunction;
    Side side;
    Polarity polarity;
    QList<IdRef*> specRefList;

};

}