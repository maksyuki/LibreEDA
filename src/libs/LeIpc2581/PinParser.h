
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Pin.h"

namespace Ipc2581b
{

class PinElectricalParser;
class XformParser;
class QStringParser;
class PinMountParser;
class StandardShapeParser;
class CadPinParser;
class LocationParser;

class PinParser
{
public:
    PinParser(
            QStringParser*&
            , QStringParser*&
            , CadPinParser*&
            , PinElectricalParser*&
            , PinMountParser*&
            , XformParser*&
            , LocationParser*&
            , StandardShapeParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Pin *result();

private:
    QStringParser *&m_numberParser;
    QStringParser *&m_nameParser;
    CadPinParser *&m_typeParser;
    PinElectricalParser *&m_electricalTypeParser;
    PinMountParser *&m_mountTypeParser;
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    StandardShapeParser *&m_standardShapeParser;
    QScopedPointer<Pin> m_result;
};

}