
#pragma once

#include <QString>

class QXmlStreamReader;

namespace Ipc2581b
{

class QStringParser
{
public:
    QStringParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    QString result();

private:
    QString m_result;
};

}