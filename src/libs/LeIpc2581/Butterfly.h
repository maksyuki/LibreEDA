
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardPrimitive.h"

#include "LineDescGroup.h"
#include "FillDescGroup.h"
#include "ButterflyShape.h"
#include "Xform.h"
#include "Double.h"

namespace Ipc2581b
{

class Butterfly: public StandardPrimitive
{
public:
	virtual ~Butterfly() {}

    ButterflyShape shape;
    Optional<Double> diameterOptional;
    Optional<Double> sideOptional;
    Optional<Xform*> xformOptional;
    Optional<LineDescGroup*> lineDescGroupOptional;
    Optional<FillDescGroup*> fillDescGroupOptional;

    virtual StandardPrimitiveType standardPrimitiveType() const override
    {
        return StandardPrimitiveType::Butterfly;
    }
};

}