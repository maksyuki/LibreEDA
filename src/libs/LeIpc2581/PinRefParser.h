
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PinRef.h"

namespace Ipc2581b
{

class QStringParser;

class PinRefParser
{
public:
    PinRefParser(
            QStringParser*&
            , QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PinRef *result();

private:
    QStringParser *&m_componentRefParser;
    QStringParser *&m_pinParser;
    QStringParser *&m_titleParser;
    QScopedPointer<PinRef> m_result;
};

}