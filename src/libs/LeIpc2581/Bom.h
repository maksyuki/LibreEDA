
#pragma once

#include <QList>
#include "Optional.h"


#include "BomItem.h"
#include "BomHeader.h"
#include "QString.h"

namespace Ipc2581b
{

class Bom
{
public:
	virtual ~Bom() {}

    QString name;
    BomHeader *bomHeader;
    QList<BomItem*> bomItemList;

};

}