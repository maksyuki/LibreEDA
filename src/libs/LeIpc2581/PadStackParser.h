
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PadStack.h"

namespace Ipc2581b
{

class LayerPadParser;
class LayerHoleParser;
class QStringParser;

class PadStackParser
{
public:
    PadStackParser(
            QStringParser*&
            , LayerHoleParser*&
            , LayerPadParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PadStack *result();

private:
    QStringParser *&m_netParser;
    LayerHoleParser *&m_layerHoleParser;
    LayerPadParser *&m_layerPadParser;
    QScopedPointer<PadStack> m_result;
};

}