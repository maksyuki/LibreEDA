#include "MatDesParser.h"

#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

MatDesParser::MatDesParser(
    QStringParser *&_nameParser
    , QStringParser *&_layerRefParser
):    m_nameParser(_nameParser)
    , m_layerRefParser(_layerRefParser)
{

}

bool MatDesParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new MatDes());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->name = m_nameParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("name: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("layerRef")))
    {
        data = reader->attributes().value(QStringLiteral("layerRef"));
        if (!m_layerRefParser->parse(reader, data))
            return false;
        m_result->layerRefOptional = Optional<QString>(m_layerRefParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

MatDes *MatDesParser::result()
{
    return m_result.take();
}

}