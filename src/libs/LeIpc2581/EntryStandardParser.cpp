#include "EntryStandardParser.h"

#include "StandardPrimitiveParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

EntryStandardParser::EntryStandardParser(
    QStringParser *&_idParser
    , StandardPrimitiveParser *&_standardPrimitiveParser
):    m_idParser(_idParser)
    , m_standardPrimitiveParser(_standardPrimitiveParser)
{

}

bool EntryStandardParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new EntryStandard());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("id")))
    {
        data = reader->attributes().value(QStringLiteral("id"));
        if (!m_idParser->parse(reader, data))
            return false;
        m_result->id = m_idParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("id: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (m_standardPrimitiveParser->isSubstitution(name))
        {
            if (!m_standardPrimitiveParser->parse(reader))
                return false;
            auto result = m_standardPrimitiveParser->result();
            m_result->standardPrimitive = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

EntryStandard *EntryStandardParser::result()
{
    return m_result.take();
}

}