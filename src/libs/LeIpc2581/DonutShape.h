
#pragma once

namespace Ipc2581b
{

enum class DonutShape
{
    Round,
    Hexagon,
    Octagon,
    Square,
};

}