
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Technology.h"

namespace Ipc2581b
{

class TechnologyListParser;
class PropertyParser;
class QStringParser;

class TechnologyParser
{
public:
    TechnologyParser(
            TechnologyListParser*&
            , QStringParser*&
            , PropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Technology *result();

private:
    TechnologyListParser *&m_typeParser;
    QStringParser *&m_commentParser;
    PropertyParser *&m_propertyParser;
    QScopedPointer<Technology> m_result;
};

}