
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "DictionaryColor.h"

namespace Ipc2581b
{

class EntryColorParser;

class DictionaryColorParser
{
public:
    DictionaryColorParser(
            EntryColorParser*&
            );

    bool parse(QXmlStreamReader *reader);
    DictionaryColor *result();

private:
    EntryColorParser *&m_entryColorParser;
    QScopedPointer<DictionaryColor> m_result;
};

}