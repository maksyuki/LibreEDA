
#pragma once

namespace Ipc2581b
{

enum class CadPin
{
    Blind,
    Surface,
    Thru,
};

}