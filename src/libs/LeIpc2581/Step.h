
#pragma once

#include <QList>
#include "Optional.h"


#include "Component.h"
#include "LayerFeature.h"
#include "Package.h"
#include "QString.h"
#include "StepRepeat.h"
#include "LogicalNet.h"
#include "Route.h"
#include "PadStack.h"
#include "PhyNetGroup.h"
#include "PadstackDef.h"
#include "DfxMeasurements.h"
#include "NonstandardAttribute.h"
#include "Contour.h"
#include "Location.h"

namespace Ipc2581b
{

class Step
{
public:
	virtual ~Step() {}

    QString name;
    QList<NonstandardAttribute*> nonstandardAttributeList;
    QList<PadStack*> padStackList;
    QList<PadstackDef*> padStackDefList;
    QList<Route*> routeList;
    Location *datum;
    Contour *profile;
    QList<StepRepeat*> stepRepeatList;
    QList<Package*> packageList;
    QList<Component*> componentList;
    QList<LogicalNet*> logicalNetList;
    QList<PhyNetGroup*> phyNetGroupList;
    QList<LayerFeature*> layerFeatureList;
    QList<DfxMeasurements*> dfxMeasurementListList;

};

}