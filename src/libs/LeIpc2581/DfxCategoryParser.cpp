#include "DfxCategoryParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

DfxCategoryParser::DfxCategoryParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool DfxCategoryParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, DfxCategory> map =
    {
        {QStringLiteral("COMPONENT"), DfxCategory::Component},
        {QStringLiteral("TESTING"), DfxCategory::Testing},
        {QStringLiteral("DATAQUALITY"), DfxCategory::Dataquality},
        {QStringLiteral("ASSEMBLY"), DfxCategory::Assembly},
        {QStringLiteral("BOARDFAB"), DfxCategory::Boardfab},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum DfxCategory").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

DfxCategory DfxCategoryParser::result()
{
    return m_result;
}

}