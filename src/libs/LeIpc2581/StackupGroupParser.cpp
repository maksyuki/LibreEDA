#include "StackupGroupParser.h"

#include "StackupLayerParser.h"
#include "IdRefParser.h"
#include "QStringParser.h"
#include "MatDesParser.h"
#include "CADDataLayerRefParser.h"
#include "DoubleParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

StackupGroupParser::StackupGroupParser(
    QStringParser *&_nameParser
    , DoubleParser *&_thicknessParser
    , DoubleParser *&_tolPlusParser
    , DoubleParser *&_tolMinusParser
    , QStringParser *&_commentParser
    , MatDesParser *&_matDesParser
    , IdRefParser *&_specRefParser
    , StackupLayerParser *&_stackupLayerParser
    , CADDataLayerRefParser *&_cADDataLayerRefParser
):    m_nameParser(_nameParser)
    , m_thicknessParser(_thicknessParser)
    , m_tolPlusParser(_tolPlusParser)
    , m_tolMinusParser(_tolMinusParser)
    , m_commentParser(_commentParser)
    , m_matDesParser(_matDesParser)
    , m_specRefParser(_specRefParser)
    , m_stackupLayerParser(_stackupLayerParser)
    , m_cADDataLayerRefParser(_cADDataLayerRefParser)
{

}

bool StackupGroupParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new StackupGroup());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->name = m_nameParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("name: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("thickness")))
    {
        data = reader->attributes().value(QStringLiteral("thickness"));
        if (!m_thicknessParser->parse(reader, data))
            return false;
        m_result->thickness = m_thicknessParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("thickness: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("tolPlus")))
    {
        data = reader->attributes().value(QStringLiteral("tolPlus"));
        if (!m_tolPlusParser->parse(reader, data))
            return false;
        m_result->tolPlus = m_tolPlusParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("tolPlus: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("tolMinus")))
    {
        data = reader->attributes().value(QStringLiteral("tolMinus"));
        if (!m_tolMinusParser->parse(reader, data))
            return false;
        m_result->tolMinus = m_tolMinusParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("tolMinus: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("comment")))
    {
        data = reader->attributes().value(QStringLiteral("comment"));
        if (!m_commentParser->parse(reader, data))
            return false;
        m_result->commentOptional = Optional<QString>(m_commentParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("MatDes"))
        {
            if (!m_matDesParser->parse(reader))
                return false;
            auto result = m_matDesParser->result();
            m_result->matDesOptional = Optional<MatDes*>(result);
        }
        else if (name == QStringLiteral("SpecRef"))
        {
            if (!m_specRefParser->parse(reader))
                return false;
            auto result = m_specRefParser->result();
            m_result->specRefList.append(result);
        }
        else if (name == QStringLiteral("StackupLayer"))
        {
            if (!m_stackupLayerParser->parse(reader))
                return false;
            auto result = m_stackupLayerParser->result();
            m_result->stackupLayerList.append(result);
        }
        else if (name == QStringLiteral("CADDataLayerRef"))
        {
            if (!m_cADDataLayerRefParser->parse(reader))
                return false;
            auto result = m_cADDataLayerRefParser->result();
            m_result->cADDataLayerRefList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

StackupGroup *StackupGroupParser::result()
{
    return m_result.take();
}

}