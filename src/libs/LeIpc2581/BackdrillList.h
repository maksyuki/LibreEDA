
#pragma once

namespace Ipc2581b
{

enum class BackdrillList
{
    MustNotCutLayer,
    StartLayer,
    MaxStubLength,
    Other,
};

}