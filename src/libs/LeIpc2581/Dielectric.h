
#pragma once

#include <QList>
#include "Optional.h"

#include "SpecificationType.h"

#include "Property.h"
#include "DielectricList.h"
#include "QString.h"

namespace Ipc2581b
{

class Dielectric: public SpecificationType
{
public:
	virtual ~Dielectric() {}

    DielectricList type;
    Optional<QString> commentOptional;
    QList<Property*> propertyList;

    virtual SpecificationTypeType specificationTypeType() const override
    {
        return SpecificationTypeType::Dielectric;
    }
};

}