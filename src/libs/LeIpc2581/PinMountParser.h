
#pragma once

#include <QString>

class QXmlStreamReader;

#include "PinMount.h"

namespace Ipc2581b
{

class PinMountParser
{
public:
    PinMountParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    PinMount result();

private:
    PinMount m_result;
};

}