
#pragma once

namespace Ipc2581b
{

enum class TemperatureList
{
    ExpansionXYAxis,
    ExpansionZAxis,
    ThermalDelamination,
    Other,
};

}