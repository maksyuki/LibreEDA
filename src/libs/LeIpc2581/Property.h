
#pragma once

#include <QList>
#include "Optional.h"


#include "QString.h"
#include "Bool.h"
#include "PropertyUnit.h"
#include "Double.h"

namespace Ipc2581b
{

class Property
{
public:
	virtual ~Property() {}

    Optional<Double> valueOptional;
    Optional<QString> textOptional;
    Optional<PropertyUnit> unitOptional;
    Optional<Double> tolPlusOptional;
    Optional<Double> tolMinusOptional;
    Optional<Double> plusTolOptional;
    Optional<Double> minusTolOptional;
    Optional<Bool> tolPercentOptional;
    Optional<Double> refValueOptional;
    Optional<QString> refTextOptional;
    Optional<PropertyUnit> refUnitOptional;
    Optional<QString> layerOrGroupRefOptional;
    Optional<QString> commentOptional;

};

}