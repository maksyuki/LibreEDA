
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "LineDesc.h"

namespace Ipc2581b
{

class LineEndParser;
class LinePropertyParser;
class DoubleParser;

class LineDescParser
{
public:
    LineDescParser(
            LineEndParser*&
            , DoubleParser*&
            , LinePropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    LineDesc *result();

private:
    LineEndParser *&m_lineEndParser;
    DoubleParser *&m_lineWidthParser;
    LinePropertyParser *&m_linePropertyParser;
    QScopedPointer<LineDesc> m_result;
};

}