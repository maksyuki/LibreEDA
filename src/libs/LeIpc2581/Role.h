
#pragma once

#include <QList>
#include "Optional.h"


#include "RoleFunction.h"
#include "QString.h"

namespace Ipc2581b
{

class Role
{
public:
	virtual ~Role() {}

    QString id;
    RoleFunction roleFunction;
    Optional<QString> descriptionOptional;
    Optional<QString> publicKeyOptional;
    Optional<QString> authorityOptional;

};

}