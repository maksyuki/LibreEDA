
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "LocalFiducial.h"

namespace Ipc2581b
{

class LocationParser;
class StandardShapeParser;
class XformParser;

class LocalFiducialParser
{
public:
    LocalFiducialParser(
            XformParser*&
            , LocationParser*&
            , StandardShapeParser*&
            );

    bool parse(QXmlStreamReader *reader);
    LocalFiducial *result();

private:
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    StandardShapeParser *&m_standardShapeParser;
    QScopedPointer<LocalFiducial> m_result;
};

}