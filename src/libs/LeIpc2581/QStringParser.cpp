#include "QStringParser.h"

namespace Ipc2581b
{

QStringParser::QStringParser()
{

}

bool QStringParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    Q_UNUSED(reader);
    m_result = data.toString();
    return true;
}

QString QStringParser::result()
{
    return m_result;
}

}