
#pragma once

#include <QList>
#include "Optional.h"

#include "LineDescGroup.h"

#include "LineEnd.h"
#include "LineProperty.h"
#include "Double.h"

namespace Ipc2581b
{

class LineDesc: public LineDescGroup
{
public:
	virtual ~LineDesc() {}

    LineEnd lineEnd;
    Double lineWidth;
    Optional<LineProperty> linePropertyOptional;

    virtual LineDescGroupType lineDescGroupType() const override
    {
        return LineDescGroupType::LineDesc;
    }
};

}