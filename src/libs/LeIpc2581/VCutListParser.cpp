#include "VCutListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

VCutListParser::VCutListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool VCutListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, VCutList> map =
    {
        {QStringLiteral("OTHER"), VCutList::Other},
        {QStringLiteral("OFFSET"), VCutList::Offset},
        {QStringLiteral("THICKNESS_REMAINING"), VCutList::ThicknessRemaining},
        {QStringLiteral("ANGLE"), VCutList::Angle},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum VCutList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

VCutList VCutListParser::result()
{
    return m_result;
}

}