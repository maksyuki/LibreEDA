
#pragma once

#include <QList>
#include "Optional.h"


#include "PadstackPadDef.h"
#include "PadstackHoleDef.h"
#include "QString.h"

namespace Ipc2581b
{

class PadstackDef
{
public:
	virtual ~PadstackDef() {}

    Optional<QString> nameOptional;
    QList<PadstackHoleDef*> padstackHoleDefList;
    QList<PadstackPadDef*> padstackPadDefList;

};

}