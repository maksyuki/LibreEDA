
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "RectCenter.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class RectCenterParser
{
public:
    RectCenterParser(
            DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    RectCenter *result();

private:
    DoubleParser *&m_widthParser;
    DoubleParser *&m_heightParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<RectCenter> m_result;
};

}