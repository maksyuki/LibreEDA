#include "WhereMeasuredParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

WhereMeasuredParser::WhereMeasuredParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool WhereMeasuredParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, WhereMeasured> map =
    {
        {QStringLiteral("OTHER"), WhereMeasured::Other},
        {QStringLiteral("MASK"), WhereMeasured::Mask},
        {QStringLiteral("METAL"), WhereMeasured::Metal},
        {QStringLiteral("LAMINATE"), WhereMeasured::Laminate},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum WhereMeasured").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

WhereMeasured WhereMeasuredParser::result()
{
    return m_result;
}

}