
#pragma once

#include <QList>
#include "Optional.h"


#include "PhyNetPoint.h"
#include "QString.h"

namespace Ipc2581b
{

class PhyNet
{
public:
	virtual ~PhyNet() {}

    QString name;
    QList<PhyNetPoint*> phyNetPointList;

};

}