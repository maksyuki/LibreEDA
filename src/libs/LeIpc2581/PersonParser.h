
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Person.h"

namespace Ipc2581b
{

class QStringParser;

class PersonParser
{
public:
    PersonParser(
            QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Person *result();

private:
    QStringParser *&m_nameParser;
    QStringParser *&m_enterpriseRefParser;
    QStringParser *&m_titleParser;
    QStringParser *&m_emailParser;
    QStringParser *&m_phoneParser;
    QStringParser *&m_faxParser;
    QStringParser *&m_mailStopParser;
    QStringParser *&m_publicKeyParser;
    QScopedPointer<Person> m_result;
};

}