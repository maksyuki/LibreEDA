#pragma once

#include <QWidget>
#include <QMap>


class QtTreePropertyBrowser;
class QtVariantPropertyManager;
class QtVariantEditorFactory;
class QtProperty;

namespace LDO
{

    class ObjectPropertyBrowser : public QWidget
    {
        Q_OBJECT
    public:
        explicit ObjectPropertyBrowser(QWidget *parent = 0);

        void setObject(QObject *object);

    signals:


    private:
        void populateBrowser();
        void populateBrowser(const QMetaObject *metaObject, QObject *object);
        void unpopulateBrowser();
        QString labelFromPropertyName(const QString &name);

    private slots:
        void onObjectPropertyValueChanged();
        void onManagerPropertyValueChanged(QtProperty *property, const QVariant &value);

    private:
        QtTreePropertyBrowser *m_propertyBrowser = nullptr;
        QtVariantPropertyManager *m_propertyManager = nullptr;
        QtVariantEditorFactory *m_propertyEditorFactory = nullptr;
        QMap<int, QtProperty*> m_objectSignalIndexToManagerProperty;
        QMap<QtProperty*, const char *> m_managerPropertyToObjectPropertyName;
        QObject *m_object = nullptr;

        // QWidget interface
    protected:
        virtual void focusInEvent(QFocusEvent *event) override;
    };

}
