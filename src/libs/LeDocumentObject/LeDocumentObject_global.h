#pragma once

#include <qglobal.h>

#if defined(LDO_LIBRARY)
#  define LDO_EXPORT Q_DECL_EXPORT
#else
#  define LDO_EXPORT Q_DECL_IMPORT
#endif
