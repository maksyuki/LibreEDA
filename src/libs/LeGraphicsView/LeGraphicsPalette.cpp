#include "LeGraphicsPalette.h"


// TODO: Investigate if we can actually customise the QGraphicsScene's QPalette,
// and QStyleOptionGraphicsItem. And then use them for our own GraphicsStyle
//
// QPalette contains color groups for each widget state
//  Color group: Disabled, Active, Inactive, Normal
//  Color role: Window, Window Text, Base, Text, ...
// QStyle encapsulates the look and feel of a GUI (draw & metrics)
//  define StateFlag: Editing, HasFocus, ReadOnly, Selected, ...
//  define control and elements.
//  draws control/elements (params: style option and painter)
//  provides metrics about control/elements
// QStyleOption stores the parameters used by QStyle functions:
//  QPalette + QStyle::State + rect + ...
//
// TODO: Use layer function, side and polarity in out Palette/Style?
// TODO: Try to define role more than 'what has to be painted'
// TODO: minor grid, major grid, Text, BrightText, selectedText


LeGraphicsPalette::LeGraphicsPalette()
{

}
