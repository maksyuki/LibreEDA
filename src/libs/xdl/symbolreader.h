﻿#pragma once

#include "xdl_global.h"
#include "symbol.h"

#include <QString>

class QXmlStreamReader;

namespace SymbolEditor
{

    class XDL_EXPORT Reader
    {
    public:
        Reader();

        Symbol *read(const QString &filename);
        QString errorString() const;

        bool read(QIODevice *device);
        Symbol *symbol() const;

    private:
        QXmlStreamReader *xml;
        QString m_errorString;
        Symbol *m_symbol;
        Item *m_item;

        void readDrawing();
        void readItem(Item *item);
        Item *readCircle();
        Item *readCircularArc();
        Item *readEllipse();
        Item *readEllipticalArc();
        Item *readRectangle();
        Item *readPolyline();
        Item *readPolygon();
        Item *readLabel();
        Item *readPin();
        Item *readGroup();

        QPointF readPoint();
        QList<QPointF> readPointList();
        LineStyle readLineStyle();
        LineWidth readLineWidth();
        Color readColor(Color defaultColor);
        FillStyle readFillStyle();
        qreal readReal();
        qreal readPercentage();
        qreal readAngle();
        qreal readLength();
        bool readBoolean();
    };

}

