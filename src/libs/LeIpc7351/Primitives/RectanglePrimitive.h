#pragma once

#include "LeIpc7351/Primitive.h"

#include <QSizeF>
#include <QPointF>

// TODO: Split b/w "simple" rect and "fancy" rect
// TBD: D-Shape

class RectanglePrimitive : public Primitive
{
    Q_OBJECT

    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(qreal cornerRadius READ cornerRadius WRITE setCornerRadius NOTIFY cornerRadiusChanged)
    Q_PROPERTY(qreal chamferSize READ chamferSize WRITE setChamferSize NOTIFY chamferSizeChanged)
    Q_PROPERTY(L7::CornerLocations roundedCorners READ roundedCorners WRITE setRoundedCorners NOTIFY roundedCornersChanged)
    Q_PROPERTY(L7::CornerLocations chamferedCorners READ chamferedCorners WRITE setChamferedCorners NOTIFY chamferedCornersChanged)
public:
    enum
    {
        Type = GeometryBaseType + 0x02
    };

    RectanglePrimitive();
    ~RectanglePrimitive();

    QSizeF size() const;
    qreal cornerRadius() const;
    L7::CornerLocations roundedCorners() const;
    qreal chamferSize() const;
    L7::CornerLocations chamferedCorners() const;

public slots:
    void setSize(QSizeF size);
    void setCornerRadius(qreal cornerRadius);
    void setChamferSize(qreal chamferSize);
    void setRoundedCorners(L7::CornerLocations roundedCorners);
    void setChamferedCorners(L7::CornerLocations chamferedCorners);

signals:
    void sizeChanged(QSizeF size);
    void cornerRadiusChanged(qreal cornerRadius);
    void chamferSizeChanged(qreal chamferSize);
    void roundedCornersChanged(L7::CornerLocations roundedCorners);
    void chamferedCornersChanged(L7::CornerLocations chamferedCorners);

private:
    QSizeF m_size;
    qreal m_cornerRadius;
    qreal m_chamferSize;
    L7::CornerLocations m_roundedCorners;
    L7::CornerLocations m_chamferedCorners;

    // IDocumentObject interface
public:
    int objectType() const { return Type; }
};
