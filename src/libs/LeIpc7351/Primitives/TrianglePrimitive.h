#pragma once

#include "LeIpc7351/Primitive.h"

#include <QSizeF>

class TrianglePrimitive : public Primitive
{
    Q_OBJECT

    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)

    QSizeF m_size;

public:
    TrianglePrimitive();
    ~TrianglePrimitive();

    QSizeF size() const;

public slots:
    void setSize(QSizeF size);

signals:
    void sizeChanged(QSizeF size);
};

