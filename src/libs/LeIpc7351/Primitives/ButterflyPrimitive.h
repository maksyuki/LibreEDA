#pragma once

#include "LeIpc7351/Primitive.h"


class ButterflyPrimitive : public Primitive
{
    Q_OBJECT

    Q_PROPERTY(qreal size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(Shape shape READ shape WRITE setShape NOTIFY shapeChanged)

public:
    enum Shape
    {
        Round,
        Square
    };
    Q_ENUM(Shape)

    ButterflyPrimitive();
    ~ButterflyPrimitive();

    qreal size() const;
    Shape shape() const;

signals:
    void sizeChanged(qreal size);
    void shapeChanged(Shape shape);

public slots:
    void setSize(qreal size);
    void setShape(Shape shape);

private:
    qreal m_size;
    Shape m_shape = Shape::Round;
};

