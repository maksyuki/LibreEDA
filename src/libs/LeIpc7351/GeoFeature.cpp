#include "GeoFeature.h"


GeoFeature::GeoFeature()
    : LDO::IDocumentObject()
{
}
GeoFeature::~GeoFeature()
{
}

Transform GeoFeature::placement() const
{
    return m_placement;
}

QPointF GeoFeature::position() const
{
    return m_placement.translation;
}

qreal GeoFeature::rotation() const
{
    return m_placement.rotation;
}

bool GeoFeature::isMirrored() const
{
    return m_placement.mirror;
}

void GeoFeature::setPlacement(Transform placement)
{
    if (m_placement == placement)
        return;

    beginSetProperty("position");
    beginSetProperty("rotation");
    beginSetProperty("mirrored");
    beginSetProperty("placement");
    m_placement = placement;
    endSetProperty("placement");
    endSetProperty("position");
    endSetProperty("rotation");
    endSetProperty("mirrored");
}

void GeoFeature::setPosition(QPointF position)
{
    if (m_placement.translation == position)
        return;

    beginSetProperty("position");
    beginSetProperty("placement");
    m_placement.translation = position;
    endSetProperty("placement");
    endSetProperty("position");
}

void GeoFeature::setRotation(qreal rotation)
{
    if (m_placement.rotation == rotation)
        return;

    beginSetProperty("rotation");
    beginSetProperty("placement");
    m_placement.rotation = rotation;
    endSetProperty("placement");
    endSetProperty("rotation");
}

void GeoFeature::setMirrored(bool mirrored)
{
    if (m_placement.mirror == mirrored)
        return;

    beginSetProperty("mirrored");
    beginSetProperty("placement");
    m_placement.mirror = mirrored;
    endSetProperty("placement");
    endSetProperty("mirrored");
}
