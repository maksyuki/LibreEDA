#include "GraphicsCrossHair.h"

#include <QPainter>
#include <QStyleOptionGraphicsItem>

namespace LeGraphicsView
{

    const qreal GraphicsCrossHair::CrossRadiusPx = 10.0F;
    const qreal GraphicsCrossHair::SmallCrossLengthPx = 40.0F;

    GraphicsCrossHair::GraphicsCrossHair():
        m_style(NoCrossHair)
    {
    }

    GraphicsCrossHair::~GraphicsCrossHair()
    {

    }

    void GraphicsCrossHair::setPos(const QPointF &pos)
    {
        m_pos = pos;
    }

    QPointF GraphicsCrossHair::pos() const
    {
        return m_pos;
    }

    void GraphicsCrossHair::setStyle(GraphicsCrossHair::Style style)
    {
        m_style = style;
    }

    GraphicsCrossHair::Style GraphicsCrossHair::style() const
    {
        return m_style;
    }

    void GraphicsCrossHair::setColor(QColor color)
    {
        m_color = color;
    }

    QColor GraphicsCrossHair::color() const
    {
        return m_color;
    }

    void GraphicsCrossHair::paint(QPainter *painter, const QRectF &exposedRect)
    {
        if (m_style == NoCrossHair)
        {
            return;
        }

        qreal levelOfDetail = QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform());

        painter->setPen(QPen(m_color, 0, Qt::SolidLine));
        QRectF rect(0, 0, CrossRadiusPx / levelOfDetail, CrossRadiusPx / levelOfDetail);
        rect.moveCenter(m_pos);
        painter->drawEllipse(rect);

        painter->setPen(QPen(m_color, 0, Qt::DashDotLine));
        if (style() == LargeCrossHair)
        {
            rect = exposedRect;
        }
        else
        {
            rect = QRectF(0, 0, SmallCrossLengthPx / levelOfDetail, SmallCrossLengthPx / levelOfDetail);
            rect.moveCenter(m_pos);
        }

        QLineF vertical(m_pos.x(), rect.top(), m_pos.x(), rect.bottom());
        QLineF horizontal(rect.left(), m_pos.y(), rect.right(), m_pos.y());
        painter->drawLine(vertical);
        painter->drawLine(horizontal);
    }

}
