import qbs 1.0

LedaLibrary {
    name: "OldGraphicsView"

    Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }

    cpp.defines: base.concat(["OLDGRAPHICSVIEW_LIBRARY"])

    files: [
        "GraphicsCrossHair.cpp",
        "GraphicsCrossHair.h",
        "GraphicsGrid.cpp",
        "GraphicsGrid.h",
        "GridModel.cpp",
        "GridModel.h",
        "LeGraphicsView.h",
        "NavigationView.cpp",
        "NavigationView.h",
        "Palette.cpp",
        "Palette.h",
        "PaletteLoader.cpp",
        "PaletteLoader.h",
        "Ruler.cpp",
        "Ruler.h",
        "RulerBar.cpp",
        "RulerBar.h",
        "Scene.cpp",
        "Scene.h",
        "Settings.cpp",
        "Settings.h",
        "View.cpp",
        "View.h",
    ]

    Export {
        Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    }
}
