#pragma once

#include "liblegerber_global.h"

#include <QString>
#include <QList>
#include <QPainterPath>
#include <QMap>

namespace LeGerber {

	class Aperture;

	class LEGERBER_EXPORT ApertureTemplate
	{
	public:
		ApertureTemplate(const QString &name, const QString &friendlyName = QString(),
		                 bool isBuiltIn = false);
		virtual ~ApertureTemplate();

		QString name;
		QString friendlyName;
		bool isBuiltIn;

		virtual Aperture *instanciate(const QList<qreal> parameters) const = 0;
	};

	class LEGERBER_EXPORT AperturePrimitive
	{
	public:
        AperturePrimitive();
		explicit AperturePrimitive(int type, bool exposure, QList<qreal> modifiers);

		int type;
		bool exposure;
		QList<qreal> modifiers;
	};

    inline bool operator==(const AperturePrimitive& lhs, const AperturePrimitive& rhs)
    {
        return lhs.type == rhs.type &&
                lhs.exposure == rhs.exposure &&
                lhs.modifiers == rhs.modifiers;

    }

    inline bool operator!=(const AperturePrimitive& lhs, const AperturePrimitive rhs)
    {
        return !(lhs == rhs);
    }

	class LEGERBER_EXPORT CustomApertureTemplate: public ApertureTemplate
	{
	public:
		CustomApertureTemplate(const QString &name, QList<AperturePrimitive> primitives);
		virtual ~CustomApertureTemplate();

		QList<AperturePrimitive> primitives;

		// ApertureTemplate interface
	public:
		virtual Aperture *instanciate(const QList<qreal> parameters) const override;
	};

	class LEGERBER_EXPORT CircleApertureTemplate: public ApertureTemplate
	{
	public:
		CircleApertureTemplate();

		// ApertureTemplate interface
	public:
		virtual Aperture *instanciate(const QList<qreal> parameters) const override;
	};

	class LEGERBER_EXPORT RectApertureTemplate: public ApertureTemplate
	{
	public:
		RectApertureTemplate();

		// ApertureTemplate interface
	public:
		virtual Aperture *instanciate(const QList<qreal> parameters) const override;
	};

	class LEGERBER_EXPORT ObroundApertureTemplate: public ApertureTemplate
	{
	public:
		ObroundApertureTemplate();

		// ApertureTemplate interface
	public:
		virtual Aperture *instanciate(const QList<qreal> parameters) const override;
	};

	class LEGERBER_EXPORT PolygonApertureTemplate: public ApertureTemplate
	{
	public:
		PolygonApertureTemplate();

		// ApertureTemplate interface
	public:
		virtual Aperture *instanciate(const QList<qreal> parameters) const override;
	};

	class LEGERBER_EXPORT Aperture
	{
	public:
		const ApertureTemplate *apertureTemplate;
		int apertureNumber;
		QPainterPath shape;
		qreal width = 0.0; // FIXME
	};

	typedef QMap<QString, ApertureTemplate*> ApertureTemplateDictionary;
	typedef QMap<quint32, Aperture*> ApertureDictionary;

}
Q_DECLARE_METATYPE(LeGerber::AperturePrimitive)