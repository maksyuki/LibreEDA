#include "command.h"
#include "processor.h"

namespace LeGerber
{

	SelectCoordinateNotationCommand::SelectCoordinateNotationCommand(CoordinateNotation notation):
	    Command(),
	    notation(notation)
	{}

	CommentCommand::CommentCommand(const QString &code, const QString &text):
	    Command(),
	    code(code), text(text)
	{}

	SelectApertureCommand::SelectApertureCommand(quint32 number):
	    Command(),
	    apertureNumber(number)
	{}

	AddApertureCommand::AddApertureCommand(int number, const QString &templateName,
	                                       const QList<qreal> &templateParameters):
	    Command(),
	    apertureNumber(number),
	    templateName(templateName),
	    parameters(templateParameters)
	{}

	SelectUnitCommand::SelectUnitCommand(Unit unit):
	    Command(),
	    unit(unit)
	{}

	SelectPolarityCommand::SelectPolarityCommand(Polarity polarity):
	    Command(),
	    polarity(polarity)
	{}

	SelectQuadrantCommand::SelectQuadrantCommand(Quadrant quadrant):
	    quadrant(quadrant)
	{}

	MoveCommand::MoveCommand(qreal x, qreal y):
	    xCoordinate(x), yCoordinate(y)
	{}

	Command::Command()
	{}

	Command::~Command()
	{}

	NullCommand::NullCommand():
	    Command()
	{}

	void Command::execute(Processor *processor)
	{
		Q_UNUSED(processor);
	}

	SelectInterpolationCommand::SelectInterpolationCommand(Interpolation interpolation):
	    Command(),
	    interpolation(interpolation)
	{

	}

	void SelectInterpolationCommand::execute(Processor *processor)
	{
		processor->setInterpolation(interpolation);
	}

	InterpolateCommand::InterpolateCommand(qreal x, qreal y, qreal i, qreal j):
	    xCoordinate(x), yCoordinate(y), xDistance(i), yDistance(j)
	{

	}

	void InterpolateCommand::execute(Processor *processor)
	{
		processor->stroke(QPointF(xCoordinate, yCoordinate), QPointF(xDistance, yDistance));
	}

	void MoveCommand::execute(Processor *processor)
	{
		processor->moveTo(QPointF(xCoordinate, yCoordinate));
	}

	FlashCommand::FlashCommand(qreal x, qreal y):
	    xCoordinate(x), yCoordinate(y)
	{

	}

	void FlashCommand::execute(Processor *processor)
	{
		processor->flash(QPointF(xCoordinate, yCoordinate));
	}

	void SelectQuadrantCommand::execute(Processor *processor)
	{
		processor->setQuadrant(quadrant);
	}

	void BeginRegionCommand::execute(Processor *processor)
	{
		processor->beginRegion();
	}

	void EndRegionCommand::execute(Processor *processor)
	{
		processor->endRegion();
	}

	void EndCommand::execute(Processor *processor)
	{
		processor->endSession();
	}

	void SelectUnitCommand::execute(Processor *processor)
	{
		processor->setUnit(unit);
	}

	void SelectPolarityCommand::execute(Processor *processor)
	{
		processor->setPolarity(polarity);
	}

	void SetApertureAttributeCommand::execute(Processor *processor)
	{
		Q_UNUSED(processor);
	}

	void ClearApertureAttributeCommand::execute(Processor *processor)
	{
		Q_UNUSED(processor);
	}

	void SetFileAttributeCommand::execute(Processor *processor)
	{
		Q_UNUSED(processor);
	}

	void AddApertureCommand::execute(Processor *processor)
	{
		processor->addAperture(apertureNumber, templateName, parameters);
	}

	void SelectApertureCommand::execute(Processor *processor)
	{
		processor->setAperture(apertureNumber);
	}

	void CommentCommand::execute(Processor *processor)
	{
		processor->addComment(text);
	}

	AddApertureTemplateCommand::AddApertureTemplateCommand(const QString &templateName,
	                                                       const QList<AperturePrimitive> &primitives):
	    Command(),
	    templateName(templateName),
	    primitives(primitives)
	{

	}

	void AddApertureTemplateCommand::execute(Processor *processor)
	{
		auto custom = new CustomApertureTemplate(templateName, primitives);
		processor->addCustomApertureTemplate(custom);
	}

}