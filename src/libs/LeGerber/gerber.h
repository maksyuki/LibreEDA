#pragma once

#include "liblegerber_global.h"

#include <QString>
#include <QMap>

namespace LeGerber
{

	enum GerberVersion
	{
		GerberVersionUnknown = 0,
		GerberVersion1,
		GerberVersion2, // X
		GerberVersion3 // X3
	};

	enum Interpolation
	{
		InvalidInterpolation = 0,
		LinearInterpolation,
		ClockWiseInterpolation,
		CounterClockWiseInterpolation
	};

	enum Quadrant
	{
		InvalidQuadrant = 0,
		SingleQuadrant,
		MultipleQuadrant,
	};

	enum Unit // Design unit?
	{
		InvalidUnit = 0,
		MetricUnit,
		ImperialUnit
	};

	enum Polarity // Object polarity?
	{
		InvalidPolarity = 0,
		ClearPolarity,
		DarkPolarity
	};

	enum CoordinateNotation
	{
		InvalidCoordinateNotation = 0,
		AbsoluteCoordinateNotation,
		IncrementalCoordinateNotation
	};

	enum ZeroBehaviour
	{
		InvalidZeroBehaviour = 0,
		LeadingZeroOmmited,
		TrailingZeroOmmited,
	};

	enum GerberCode
	{
		G01 = 0,
		G02,
		G03,
		G04,
		G36,
		G37,
		G70,
		G71,
		G74,
		G75,
		G90,
		G91,
		D01,
		D02,
		D03,
		Dnn,
		FS,
		MO,
		QD,
		AM,
		LP,
		SR,
		TF,
		TA,
		TD,
		AS,
		IN,
		IP,
		IR,
		LN,
		MI,
		OF,
		SF
	};

	struct GerberCodeStatistics
	{
		int G01 = 0;
		int G02 = 0;
		int G03 = 0;
		int G04 = 0;
		int G36 = 0;
		int G37 = 0;
		int G54 = 0;
		int G55 = 0;
		int G70 = 0;
		int G71 = 0;
		int G74 = 0;
		int G75 = 0;
		int G90 = 0;
		int G91 = 0;
		int D01 = 0;
		int D02 = 0;
		int D03 = 0;
		int Dnn = 0;
		int M00 = 0;
		int M01 = 0;
		int M02 = 0;
		int FS = 0;
		int MO = 0;
		int AD = 0;
		int AM = 0;
		int LP = 0;
		int SR = 0;
		int TF = 0;
		int TA = 0;
		int TD = 0;
		int AS = 0;
		int IN = 0;
		int IP = 0;
		int IR = 0;
		int LN = 0;
		int MI = 0;
		int OF = 0;
		int SF = 0;
		int GARBAGE = 0;

		QMap<QString, int> asMap() const
		{
			QMap<QString, int> m;
			m["G01"] = G01;
			m["G02"] = G02;
			m["G03"] = G03;
			m["G04"] = G04;
			m["G36"] = G36;
			m["G37"] = G37;
			m["G54"] = G54;
			m["G55"] = G55;
			m["G70"] = G70;
			m["G71"] = G71;
			m["G74"] = G74;
			m["G75"] = G75;
			m["G90"] = G90;
			m["G91"] = G91;
			m["D01"] = D01;
			m["D02"] = D02;
			m["D03"] = D03;
			m["Dnn"] = Dnn;
			m["M00"] = M00;
			m["M01"] = M01;
			m["M02"] = M02;
			m["FS"] = FS;
			m["MO"] = MO;
			m["AD"] = AD;
			m["AM"] = AM;
			m["LP"] = LP;
			m["SR"] = SR;
			m["TF"] = TF;
			m["TA"] = TA;
			m["TD"] = TD;
			m["AS"] = AS;
			m["IN"] = IN;
			m["IP"] = IP;
			m["IR"] = IR;
			m["LN"] = LN;
			m["MI"] = MI;
			m["OF"] = OF;
			m["SF"] = SF;
			m["GARBAGE"] = GARBAGE;
			return m;
		}

		quint64 totalCount() const
		{
			quint64 total = 0;
			total += G01 + G02 + G03 + G04;
			total += G36 + G37 + G54 + G55;
			total += G70 + G71 + G74 + G75;
			total += G90 + G91;
			total += D01 + D02 + D03 + Dnn;
			total += M00 + M01 + M02;
			total += FS + MO + AD + AM;
			total += LP + SR + TF + TA;
			total += TD + AS + IN + IP;
			total += IR + LN + MI + OF;
			total += SF + GARBAGE;
			return total;
		}
	};

}
