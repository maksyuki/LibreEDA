import qbs 1.0

LedaLibrary {
    name: "LeGerber"

    Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    Depends { name: "OldGraphicsView"; }

    cpp.defines: base.concat(["LEGERBER_LIBRARY"])

    files: [
        "aperturetemplate.cpp",
        "aperturetemplate.h",
        "command.cpp",
        "command.h",
        "filereader.cpp",
        "filereader.h",
        "gerber.h",
        "graphicsitem.cpp",
        "graphicsitem.h",
        "graphicsscene.cpp",
        "graphicsscene.h",
        "job.cpp",
        "job.h",
        "liblegerber_global.h",
        "object.cpp",
        "object.h",
        "processor.cpp",
        "processor.h",
        "statement.cpp",
        "statement.h",
    ]

    Export {
        Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    }
}
