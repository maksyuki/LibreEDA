#pragma once

#include "liblegerber_global.h"

#include <QGraphicsPathItem>
#include <QPen>
#include <QBrush>

#include "gerber.h"

namespace LeGerber
{

	class GraphicsScene;

	class LEGERBER_EXPORT GraphicsItem : public QGraphicsPathItem
	{
	public:
		enum
		{
			Type = QGraphicsItem::UserType + 1
		};

		GraphicsItem(QGraphicsItem *parent = nullptr);

		void setPolarity(Polarity polarity);
		Polarity polarity() const;

		QPen penForPolarity(Polarity polarity) const;
		QBrush brushForPolarity(Polarity polarity) const;

	private:
		Polarity m_polarity;


		// QGraphicsItem interface
	public:
		virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
		virtual int type() const override;
	};

}