#include "graphicsscene.h"
#include "graphicsitem.h"

namespace LeGerber
{

	GraphicsScene::GraphicsScene(QObject *parent):
        LeGraphicsView::Scene (parent)
	{

	}

	void GraphicsScene::addGerberObject(const Object *object)
	{
		auto item = new GraphicsItem;
		item->setPath(object->painterPath);
		item->setPolarity(object->polarity);
		addItem(item);
		item->setPen(item->penForPolarity(item->polarity()));
		item->setBrush(item->brushForPolarity(item->polarity()));
	}

	void GraphicsScene::setDarkBrush(const QBrush &brush)
	{
		if (m_darkBrush == brush)
			return;
		m_darkBrush = brush;
		invalidate(itemsBoundingRect(), QGraphicsScene::ItemLayer);
		emit darkBrushChanged(m_darkBrush);
	}

	QBrush GraphicsScene::darkBrush() const
	{
		return m_darkBrush;
	}

	void GraphicsScene::setDarkPen(const QPen &pen)
	{
		if (m_darkPen == pen)
			return;
		m_darkPen = pen;
		invalidate(itemsBoundingRect(), QGraphicsScene::ItemLayer);
		emit darkPenChanged(m_darkPen);
	}

	QPen GraphicsScene::darkPen() const
	{
		return m_darkPen;
	}

	void GraphicsScene::setClearBrush(const QBrush &brush)
	{
		if (m_clearBrush == brush)
			return;
		m_clearBrush = brush;
		invalidate(itemsBoundingRect(), QGraphicsScene::ItemLayer);
		emit clearBrushChanged(m_clearBrush);
	}

	QBrush GraphicsScene::clearBrush() const
	{
		return m_clearBrush;
	}

	void GraphicsScene::setClearPen(const QPen &pen)
	{
		if (m_clearPen == pen)
			return;
		m_clearPen = pen;
		invalidate(itemsBoundingRect(), QGraphicsScene::ItemLayer);
		emit clearPenChanged(m_darkPen);
	}

	QPen GraphicsScene::clearPen() const
	{
		return m_clearPen;
	}

}
