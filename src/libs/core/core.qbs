import qbs 1.0
import qbs.Process
import qbs.Environment

LedaLibrary {
    name: "Core"

    Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    Export {
        Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    }

    cpp.defines: base.concat(["CORE_LIBRARY",
                              'CI_SOURCE_ID="' + CiProbe.sourceId + '"',
                              'CI_SOURCE_TAINTED=' + CiProbe.sourceIsTainted,
                              'CI_SOURCE_URL="' + CiProbe.sourceUrl + '"',
                              'CI_BUILD_ID="' + CiProbe.buildId + '"',
                              'CI_DATETIME="' + CiProbe.dateTime + '"', // BUILD_ISO_DATETIME
                              'EDA_MAJOR_VERSION=' + project.leda_version_major,
                              'EDA_MINOR_VERSION=' + project.leda_version_minor,
                              'EDA_RELEASE_VERSION=' + project.leda_version_release,
                              'EDA_COMPAT_MAJOR_VERSION=' + project.leda_compat_version_major,
                              'EDA_COMPAT_MINOR_VERSION=' + project.leda_compat_version_minor,
                              'EDA_COMPAT_RELEASE_VERSION=' + project.leda_compat_version_release,
                              'EDA_APP_TARGET="' + project.leda_app_target + '"',
                              'EDA_APP_PATH="' + project.leda_app_path + '"',
                              'EDA_RELATIVE_ROOT_PATH="' + project.leda_relative_root_path + '"',
                              'EDA_BIN_PATH="' + project.leda_bin_path + '"',
                              'EDA_LIB_PATH="' + project.leda_library_path + '"',
                              'EDA_LIBEXEC_PATH="' + project.leda_libexec_path + '"',
                              'EDA_PLUGIN_PATH="' + project.leda_plugin_path + '"',
                              'EDA_DATA_PATH="' + project.leda_data_path + '"',
                              'EDA_DOC_PATH="' + project.leda_doc_path + '"',
                              'EDA_SOURCE_PATH="' + project.leda_source_path + '"',
                             ])

    files: [
        "constants.h",
        "core.cpp",
        "core.h",
        "core_global.h",
        "extension/extension.h",
        "extension/iplugin.cpp",
        "extension/iplugin.h",
        "extension/plugincollection.cpp",
        "extension/plugincollection.h",
        "extension/pluginmanager.cpp",
        "extension/pluginmanager.h",
        "extension/pluginspec.cpp",
        "extension/pluginspec.h",
        "json.cpp",
        "json.h",
        "actionmanager/actionmanager.cpp",
        "actionmanager/actionmanager.h",
        "editormanager/documentmanager.cpp",
        "editormanager/documentmanager.h",
        "editormanager/editormanager.cpp",
        "editormanager/editormanager.h",
        "editormanager/editorview.cpp",
        "editormanager/editorview.h",
        "editormanager/idocument.cpp",
        "editormanager/idocument.h",
        "editormanager/ieditor.cpp",
        "editormanager/ieditorfactory.cpp",
        "editormanager/ieditorfactory.h",
        "editormanager/ieditor.h",
        "navigationview/inavigationviewfactory.cpp",
        "navigationview/inavigationviewfactory.h",
        "navigationview/navigationdockwidget.cpp",
        "navigationview/navigationdockwidget.h",
        "outputpane/ioutputpanefactory.cpp",
        "outputpane/ioutputpanefactory.h",
        "outputpane/outputdockwidget.cpp",
        "outputpane/outputdockwidget.h",
        "settings/isettingspage.cpp",
        "settings/isettingspage.h",
        "settings/settingsdialog.cpp",
        "settings/settingsdialog.h",
    ]

    Probe {
        id: CiProbe

        property string sourceId: "<none>";
        property string sourceUrl: "<none>"
        property string sourceIsTainted: "true";
        property string buildId: "<none>";
        property string dateTime: "1970-01-01T00:00:00.000"
        property string sourceDirectory: product.sourceDirectory;

        configure: {

            var gitProcess = new Process();
            gitProcess.setWorkingDirectory(sourceDirectory);
            if (gitProcess.exec("git", ["rev-parse", "--quiet", "HEAD"]) === 0)
            {
                var result = gitProcess.readStdOut().trim();
                if (result)
                {
                    sourceUrl = "https://gitlab.com/chgans/LibreEDA/tree/" + result
                    sourceId = result.substring(0,8);
                }
            }
            if (gitProcess.exec("git", ["diff-index", "--quiet", "HEAD"]) === 0)
            {
                sourceIsTainted = "false";
            }

            var result = Environment.getEnv("CI_BUILD_ID");
            if (result)
                buildId = result;

            var dateProcess = new Process();
            dateProcess.setWorkingDirectory(sourceDirectory);
            if (dateProcess.exec("date", ["--iso-8601=second"]) === 0)
            {
                var result = dateProcess.readStdOut().trim();
                if (result)
                {
                    dateTime = result;
                }
            }
        }
    }
}
