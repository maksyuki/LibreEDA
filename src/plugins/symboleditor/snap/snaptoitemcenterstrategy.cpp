#include "snap/snaptoitemcenterstrategy.h"
#include "item/item.h"

#include <QMultiMap>
#include <QtGlobal>

namespace SymbolEditor
{

    SnapToItemCenterStrategy::SnapToItemCenterStrategy(QObject *parent):
        AbstractSnapStrategy(parent)
    {
        setName("center");
        setLabel("<b>S</b>nap to <b>C</b>enter-points");
        setShortcut(QKeySequence("s,c"));
        setIcon(QIcon(":/icons/snap/snap-center.svg"));
        updateAction();
    }

    SnapResult SnapToItemCenterStrategy::snap(QPointF mousePos, qreal maxDistance)
    {
        SnapCandidates candidates;

        for (auto item: items(mousePos, maxDistance))
        {
            candidates.insert(item, item->centerPoints());
        }

        return AbstractSnapStrategy::snap(mousePos, candidates);
    }
}
