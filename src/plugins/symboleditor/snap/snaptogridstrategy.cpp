#include "snap/snaptogridstrategy.h"

#include "view/view.h"
#include "OldGraphicsView/GridModel.h"

namespace SymbolEditor
{

    SnapToGridStrategy::SnapToGridStrategy(QObject *parent):
        AbstractSnapStrategy(parent)
    {
        setName("grid");
        setLabel("<b>S</b>nap to <b>G</b>rid");
        setShortcut(QKeySequence("s,g"));
        setIcon(QIcon(":/icons/snap/snap-grid.svg")); // FIXME: use theme
        updateAction();
    }

    SnapResult SnapToGridStrategy::snap(QPointF mousePos, qreal maxDistance)
    {
        Q_UNUSED(maxDistance);

        SnapResult result;
        result.pos = view()->gridModel()->round(mousePos);
        result.distance = QLineF(mousePos, result.pos).length();
        result.item = nullptr;

        return result;
    }
}
