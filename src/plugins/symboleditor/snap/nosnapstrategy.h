#pragma once

#include "abstractsnapstrategy.h"

namespace SymbolEditor
{
    class NoSnapStrategy: public AbstractSnapStrategy
    {
    public:
        explicit NoSnapStrategy(QObject *parent = nullptr);

        // AbstractSnapStrategy interface
    public:
        SnapResult snap(QPointF mousePos, qreal maxDistance) override;
    };

}
