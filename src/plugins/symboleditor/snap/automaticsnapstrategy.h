#pragma once

#include "abstractsnapstrategy.h"

namespace SymbolEditor
{
    class AutomaticSnapStrategy: public AbstractSnapStrategy
    {
    public:
        explicit AutomaticSnapStrategy(QObject *parent = nullptr);

        // AbstractSnapStrategy interface
    public:
        SnapResult snap(QPointF mousePos, qreal maxDistance) override;
    };

}
