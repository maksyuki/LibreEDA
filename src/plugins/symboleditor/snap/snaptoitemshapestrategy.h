#pragma once

#include "abstractsnapstrategy.h"

namespace SymbolEditor
{

    class SnapToItemShapeStrategy: public AbstractSnapStrategy
    {
    public:
        explicit SnapToItemShapeStrategy(QObject *parent = nullptr);

        // AbstractSnapStrategy interface
    public:
        SnapResult snap(QPointF mousePos, qreal maxDistance) override;
    };

}
