#include "snap/automaticsnapstrategy.h"

namespace SymbolEditor
{

    AutomaticSnapStrategy::AutomaticSnapStrategy(QObject *parent):
        AbstractSnapStrategy(parent)
    {
        setLabel("<b>A</b>uto snap");
        setShortcut(QKeySequence("s,a"));
        // TODO: Set display name "Free"
        setIcon(QIcon(":/icons/snap/snap-reference.svg")); // FIXME: use theme
        updateAction();
    }

    SnapResult AutomaticSnapStrategy::snap(QPointF mousePos, qreal maxDistance)
    {
        Q_UNUSED(maxDistance);

        SnapResult result;
        result.pos = mousePos;
        result.distance = 0;
        result.item = nullptr;

        return result;
    }
}
