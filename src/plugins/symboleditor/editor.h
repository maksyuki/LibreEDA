#pragma once

#include "symboleditor_global.h"
#include "core/editormanager/ieditor.h"
#include "OldGraphicsView/Settings.h"

class QMainWindow;
class QActionGroup;
class QToolBar;
class QComboBox;
class QUndoStack;

namespace SymbolEditor
{
    class Document;
    class Item;
    class View;
    class Scene;
    class TaskDockWidget;
    class UndoDockWidget;
    class SnapManager;
    class ToolManager;
    class UndoCommand;

    class SYMBOLEDITOR_EXPORT Editor : public IEditor
    {
        Q_OBJECT
    public:
        explicit Editor(QObject *parent = nullptr);
        ~Editor();

    public slots:
        void applySettings(const LeGraphicsView::Settings &settings);

    private:
        Scene *m_scene = nullptr;
        View *m_view = nullptr;
        Document *m_document = nullptr;
        QUndoStack *m_undoStack = nullptr;

        void addInteractiveTools();
        void removeInteractiveTools();
        ToolManager *m_interactiveToolManager = nullptr;
        QToolBar *m_interactiveToolBar = nullptr;

        void addSnapTools();
        void removeSnapTools();
        SnapManager *m_snapToolManager = nullptr;
        QToolBar *m_snapToolBar = nullptr;

        void addDockWidgets();
        void removeDockWidets();
        TaskDockWidget *m_taskDockWidget = nullptr;
        UndoDockWidget *m_undoDockWidget = nullptr;

    private slots:
        void pushCommand(UndoCommand *command);
        void addDocumentItem(quint64 id, const Item *item);
        void updateDocumentItemProperty(quint64 itemId, quint64 propertyId, const QVariant &value);
        void removeDocumentItem(quint64 id);
        void updateDocumentItemStack(quint64 itemId, quint64 beforeId);

        // IEditor interface
    public:
        bool open(QString *errorString, const QString &fileName);
        IDocument *document() const;
        QIcon icon() const;
        QString displayName() const;
        void activate(QMainWindow *mainWindow);
        void desactivate(QMainWindow *mainWindow);
    };
}
