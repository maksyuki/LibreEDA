#include "undodockwidget.h"
#include "OldGraphicsView/Settings.h"

#include <QUndoView>
#include <QUndoStack>

namespace SymbolEditor
{

    UndoDockWidget::UndoDockWidget(QWidget *parent, Qt::WindowFlags flags):
        DockWidget(parent, flags),
        m_view(new QUndoView)
    {
        setWindowTitle("Undo/Redo Stack");
        setWidget(m_view);
    }

    void UndoDockWidget::setStack(QUndoStack *stack)
    {
        m_view->setStack(stack);
    }

    void UndoDockWidget::applySettings(const LeGraphicsView::Settings &settings)
    {
        Q_UNUSED(settings);
    }

}
