#pragma once

#include "OldGraphicsView/Settings.h"

#include <QObject>
#include <QString>
#include <QList>
#include <QMap>

class QAction;
class QActionGroup;
class QGraphicsScene;

namespace SymbolEditor
{

    class AbstractTool;
    class Scene;
    class UndoCommand;

    class ToolManager : public QObject
    {
        Q_OBJECT

    public:
        explicit ToolManager(QObject *parent = nullptr);
        ~ToolManager();

        QList<QAction *> actions() const;

        UndoCommand *nextPendingCommand();
        bool hasPendingCommand() const;

    public slots:
        void setScene(Scene *scene);
        void applySettings(const LeGraphicsView::Settings &settings);

    signals:
        void newCommandAvailable();
        void taskWidgetsChanged(QList<QWidget *> widgets);

    private:
        QActionGroup *m_actionGroup = nullptr;
        QMap<QAction *, AbstractTool*> m_actionToTool;
        QList<AbstractTool *> m_tools;
        AbstractTool *m_activeTool = nullptr;
        AbstractTool *m_defaultTool = nullptr;
        Scene *m_scene = nullptr;
        QList<UndoCommand *> m_pendingCommands;

    private slots:
        void setActiveTool(AbstractTool *tool);
        void appendNewCommand();

        // QObject interface
    public:
        bool eventFilter(QObject *watched, QEvent *event) override;
    };

} // namespace SymbolEditor

