#include "editorfactory.h"
#include "constants.h"
#include "editor.h"
#include "OldGraphicsView/Settings.h"

#include "core/core.h"

namespace SymbolEditor
{

    EditorFactory::EditorFactory(QObject *parent) :
        IEditorFactory(parent)
    {
        setId(SCHEDITOR_ID);
        setFileExtensions(QStringList() << SCHEDITOR_EXT);
        setDisplayName("SCH editor");
    }

    void EditorFactory::applySettings()
    {
        LeGraphicsView::Settings settings;
        settings.load(Core::settings());
        for (auto editor : m_editors)
        {
            editor->applySettings(settings);
        }
    }

    IEditor *EditorFactory::createEditor()
    {
        auto editor = new Editor();
        m_editors.append(editor);
        connect(editor, &QObject::destroyed,
                this, [this, editor](QObject *)
        {
            m_editors.removeOne(editor);
        });
        LeGraphicsView::Settings settings;
        settings.load(Core::settings());
        editor->applySettings(settings);
        return editor;
    }

}
