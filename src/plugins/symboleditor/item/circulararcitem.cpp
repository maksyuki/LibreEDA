#include "item/circulararcitem.h"

#include <QtMath>
#include <QStyleOptionGraphicsItem>
#include <QPainter>
#include <QVector2D>

namespace SymbolEditor
{

    GraphicsCircularArcItem::GraphicsCircularArcItem(GraphicsItem *parent):
        GraphicsItem(parent), m_radius(0.0),
        m_startAngle(0.0), m_spanAngle(360.0)
    {
        addHandles();
    }

    GraphicsCircularArcItem::~GraphicsCircularArcItem()
    {

    }

    qreal GraphicsCircularArcItem::radius() const
    {
        return m_radius;
    }

    qreal GraphicsCircularArcItem::startAngle() const
    {
        return m_startAngle;
    }

    qreal GraphicsCircularArcItem::spanAngle() const
    {
        return m_spanAngle;
    }

    void GraphicsCircularArcItem::setRadius(qreal r)
    {
        qreal radius = qAbs(r);
        if (qFuzzyCompare(radius, m_radius))
        {
            return;
        }
        prepareGeometryChange();
        m_boundingRect = QRectF();
        m_radius = radius;
        update();
        updateHandles();
    }

    void GraphicsCircularArcItem::setStartAngle(qreal a)
    {
        qreal angle = fmod(a, 360);
        if (qFuzzyCompare(angle, m_startAngle))
        {
            return;
        }
        prepareGeometryChange();
        m_boundingRect = QRectF();
        m_startAngle = angle;
        update();
        updateHandles();
    }

    void GraphicsCircularArcItem::setSpanAngle(qreal a)
    {
        qreal angle = fmod(a, 360);
        if (qFuzzyCompare(angle, m_spanAngle))
        {
            return;
        }
        prepareGeometryChange();
        m_boundingRect = QRectF();
        m_spanAngle = angle;
        update();
        updateHandles();
    }

    void GraphicsCircularArcItem::addHandles()
    {
        addHandle(RadiusHandle, Handle::Role::Move, Handle::Shape::Diamond);
        addHandle(StartAngleHandle, Handle::Role::Move, Handle::Shape::Circle);
        addHandle(SpanAngleHandle, Handle::Role::Move, Handle::Shape::Circle);
    }

    void GraphicsCircularArcItem::updateHandles()
    {
        blockItemNotification();
        handleAt(RadiusHandle)->setPos(QPointF(m_radius, 0.0));
        handleAt(StartAngleHandle)->setPos(pointAt(m_startAngle));
        handleAt(SpanAngleHandle)->setPos(pointAt(m_startAngle + m_spanAngle));
        unblockItemNotification();
    }

    QPointF GraphicsCircularArcItem::pointAt(qreal angle) const
    {
        qreal theta = qDegreesToRadians(angle);
        return QPointF(m_radius * qCos(theta), -m_radius * qSin(theta));
    }

    qreal GraphicsCircularArcItem::angleAt(const QPointF &pos) const
    {
        QLineF vector(QPointF(0, 0), QPointF(pos.x() / radius(), pos.y() / m_radius));
        return vector.angle();
    }

    QRectF GraphicsCircularArcItem::rect() const
    {
        return QRectF(-m_radius, -m_radius, m_radius * 2.0, m_radius * 2.0);
    }

    bool GraphicsCircularArcItem::isFullCircle() const
    {
        return qFuzzyCompare(m_spanAngle, 360.0);
    }

    QRectF GraphicsCircularArcItem::boundingRect() const
    {
        if (m_boundingRect.isNull())
        {
            qreal pw = pen().style() == Qt::NoPen ? qreal(0) : pen().widthF();
            if (pw == 0.0 && isFullCircle())
            {
                m_boundingRect = rect();
            }
            else
            {
                m_boundingRect = shape().controlPointRect();
            }
        }
        return m_boundingRect;
    }

    QPainterPath GraphicsCircularArcItem::shape() const
    {
        QPainterPath path;
        if (isFullCircle())
        {
            path.addEllipse(rect());
        }
        else
        {
            path.arcTo(rect(), m_startAngle, m_spanAngle);
        }

        return shapeFromPath(path, pen());
    }

    void GraphicsCircularArcItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                        QWidget *widget)
    {
        Q_UNUSED(option);
        Q_UNUSED(widget);
        painter->setPen(pen());
        painter->setBrush(brush());
        if (isFullCircle())
        {
            painter->drawEllipse(rect());
        }
        else
        {
            painter->drawPie(rect(), qFloor(m_startAngle * 16), qFloor(m_spanAngle * 16));
        }
    }

    QVariant GraphicsCircularArcItem::itemChange(QGraphicsItem::GraphicsItemChange change,
                                 const QVariant &value)
    {
        if (change == QGraphicsItem::ItemSelectedHasChanged)
        {
            for (int i = 0; i < handleCount(); i++)
            {
                handleAt(i)->setVisible(isSelected());
            }
        }
        return value;
    }

    void GraphicsCircularArcItem::itemNotification(IObservableItem *item)
    {
        Handle *handle = static_cast<Handle *>(item);
        qreal angle = angleAt(handle->pos());
        switch (handle->handleId())
        {
            case RadiusHandle:
                setRadius(handle->x());
                break;
            case StartAngleHandle:
                setStartAngle(angle);
                break;
            case SpanAngleHandle:
                setSpanAngle(angle + 360 - m_startAngle);
                break;
            default:
                Q_ASSERT(false);
                break;
        }
    }

    GraphicsItem *GraphicsCircularArcItem::clone()
    {
        GraphicsCircularArcItem *item = new GraphicsCircularArcItem();
        item->setRadius(m_radius);
        item->setStartAngle(m_startAngle);
        item->setSpanAngle(m_spanAngle);
        GraphicsItem::cloneTo(item);
        return item;
    }

    void GraphicsCircularArcItem::setProperty(PropertyId id, const QVariant &value)
    {
        switch (id)
        {
            case RadiusProperty:
                setRadius(value.toReal());
                break;
            case StartAngleProperty:
                setStartAngle(value.toReal());
                break;
            case SpanAngleProperty:
                setSpanAngle(value.toReal());
                break;
            default:
                GraphicsItem::setProperty(id, value);
                break;
        }
    }

    QList<QPointF> GraphicsCircularArcItem::endPoints() const
    {
        return QList<QPointF>() << pointAt(m_startAngle) << pointAt(m_startAngle + m_spanAngle);
    }

    QList<QPointF> GraphicsCircularArcItem::midPoints() const
    {
        return QList<QPointF>() << pointAt(m_startAngle + m_spanAngle / 2.0);
    }

    QList<QPointF> GraphicsCircularArcItem::centerPoints() const
    {
        return QList<QPointF>() << QPointF(0, 0);
    }

    QList<QPointF> GraphicsCircularArcItem::nearestPoints(QPointF pos) const
    {
        return QList<QPointF>() << nearestPoint(QPointF(0, 0), m_radius,
                                                m_startAngle, m_spanAngle, mapFromScene(pos));
    }

}
