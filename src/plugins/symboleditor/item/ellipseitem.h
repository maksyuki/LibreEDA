#pragma once

#include "item/item.h"

namespace SymbolEditor
{

    class GraphicsEllipseItem : public GraphicsItem
    {
    public:
        enum { Type = ModelType + Ellipse };

        enum HandleId
        {
            XRadiusHandle = 0,
            YRadiusHandle,
            NbHandles
        };

        explicit GraphicsEllipseItem(GraphicsItem *parent = nullptr);
        ~GraphicsEllipseItem();

        qreal xRadius() const;
        void setXRadius(qreal xRadius);
        qreal yRadius() const;
        void setYRadius(qreal yRadius);

    private:
        QPointF pointAt(qreal angle) const;
        qreal angleAt(const QPointF &pos) const;
        qreal m_xRadius;
        qreal m_yRadius;

        // QGraphicsItem interface
    public:
        QRectF boundingRect() const override;
        QPainterPath shape() const override;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

        // SchItem interface
    public:
        GraphicsItem *clone() override;
        QList<PropertyId> propertyIdList() const override;
        void setProperty(PropertyId id, const QVariant &value) override;
        QVariant property(PropertyId id) const override;

        QList<QPointF> endPoints() const override;
        QList<QPointF> midPoints() const override;
        QList<QPointF> centerPoints() const override;
        QList<QPointF> nearestPoints(QPointF pos) const override;

        // QGraphicsItem interface
    protected:
        QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
        int type() const override { return Type; }

        // IGraphicsItemObserver interface
    public:
        void itemNotification(IObservableItem *item) override;
    };

}
