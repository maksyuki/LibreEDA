#pragma once

#include "item/item.h"

// TODO: Pie vs chord vs whole shape
// TODO: keep ratio (ellipse vs circle)

namespace SymbolEditor
{

    class GraphicsEllipticalArcItem : public GraphicsItem
    {
    public:
        enum { Type = ModelType + EllipticalArc };

        enum HandleId
        {
            XRadiusHandle = 0,
            YRadiusHandle,
            StartAngleHandle,
            SpanAngleHandle
        };

        explicit GraphicsEllipticalArcItem(GraphicsItem *parent = nullptr);
        ~GraphicsEllipticalArcItem();

        qreal xRadius() const;
        void setXRadius(qreal radius);
        qreal yRadius() const;
        void setYRadius(qreal radius);
        qreal startAngle() const;
        void setStartAngle(qreal angle);
        qreal spanAngle() const;
        void setSpanAngle(qreal angle);

    private:
        void addHandles();
        void updateHandles();
        QPointF pointAt(qreal angle) const;
        qreal angleAt(const QPointF &pos) const;
        QRectF rect() const;
        qreal m_xRadius;
        qreal m_yRadius;
        qreal m_startAngle;
        qreal m_spanAngle;
        mutable QRectF m_boundingRect;

        // QGraphicsItem interface
    public:
        QRectF boundingRect() const override;
        QPainterPath shape() const override;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

        // QGraphicsItem interface
    protected:
        QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
        int type() const override { return Type; }

        // IGraphicsItemObserver interface
    public:
        void itemNotification(IObservableItem *item) override;

        // SchItem interface
    public:
        GraphicsItem *clone() override;
        void setProperty(PropertyId id, const QVariant &value) override;

        QList<QPointF> endPoints() const override;
        QList<QPointF> midPoints() const override;
        QList<QPointF> centerPoints() const override;
        QList<QPointF> nearestPoints(QPointF pos) const override;
    };

}
