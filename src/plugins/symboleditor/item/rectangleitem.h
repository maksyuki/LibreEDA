#pragma once

#include "item/item.h"

namespace SymbolEditor
{

    // TODO: AbstractGraphicsShape
    // TODO: add properties
    class GraphicsRectangleItem : public GraphicsItem
    {
    public:
        enum { Type = ModelType + Rectangle };

        enum HandleId
        {
            TopLeft = 0,
            BottomRight,
            Bottom,
            Left,
            XRoundness,
            YRoundness,
            NbHandles
        };

        explicit GraphicsRectangleItem(GraphicsItem *parent = nullptr);
        ~GraphicsRectangleItem();

        QRectF rect() const;
        void setRect(const QRectF &rect);

        void setRoundness(qreal xRoundness, qreal yRoundness);
        qreal xRoundness() const;
        void setXRoundness(qreal roundness);
        qreal yRoundness() const;
        void setYRoundness(qreal roundness);

    private:
        QRectF m_rect;
        qreal m_xRoundness;
        qreal m_yRoundness;
        void updateSizeHandles();
        void updateRoundnessHandles();

        // ScheItem interface
    public:
        GraphicsItem *clone() override;
        void itemNotification(IObservableItem *item) override;


        QList<PropertyId> propertyIdList() const override;
        void setProperty(PropertyId id, const QVariant &value) override;
        QVariant property(PropertyId id) const override;

        QList<QPointF> endPoints() const override;
        QList<QPointF> midPoints() const override;
        QList<QPointF> centerPoints() const override;
        QList<QPointF> nearestPoints(QPointF pos) const override;

        // QGraphicsItem interface
    public:
        QRectF boundingRect() const override;
        QPainterPath shape() const override;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
        int type() const override { return Type; }

        // QGraphicsItem interface
    protected:
        QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

        // QGraphicsItem interface
    public:
        bool contains(const QPointF &point) const override;
    };

}
