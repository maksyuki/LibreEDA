#pragma once

#include "abstracttool.h"

namespace SymbolEditor
{
    class GraphicsRectangleItem;

    class PlaceRectangleTool : public AbstractTool
    {
        Q_OBJECT

    public:
        PlaceRectangleTool(QObject *parent = nullptr);
        ~PlaceRectangleTool();

    private:
        enum State
        {
            WaitingForActivation,
            WaitingForFirstPoint,
            WaitingForSecondPoint,
        };
        State m_state;
        QPointF m_p1;
        QPointF m_p2;
        GraphicsRectangleItem *m_item;

        void addItem();
        void updateItem();
        void removeItem();

        // GraphicsTool interface
    public:
        void activate(Scene *scene) override;
        void desactivate() override;

        // GraphicsTool interface
    protected:
        bool mouseClickEvent(QGraphicsSceneMouseEvent *event) override;
        bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

    };

} // namespace SymbolEditor

