#pragma once

#include "abstracttool.h"

namespace SymbolEditor
{
    class GraphicsCircularArcItem;

    class PlaceCircularArcTool : public AbstractTool
    {
        Q_OBJECT

    public:
        PlaceCircularArcTool(QObject *parent = nullptr);
        ~PlaceCircularArcTool();

    private:
        enum State
        {
            WaitingForActivation,
            WaitingForCenter,
            WaitingForRadius,
            WaitingForStartAngle,
            WaitingForSpanAngle,
        };
        State m_state;
        QPointF m_center;
        QPointF m_radiusPos;
        QPointF m_startAnglePos;
        QPointF m_spanAnglePos;
        GraphicsCircularArcItem *m_item;

        void addItem();
        void updateItem();
        void removeItem();

        // GraphicsTool interface
    public:
        void activate(Scene *scene) override;
        void desactivate() override;

        // GraphicsTool interface
    protected:
        bool mouseClickEvent(QGraphicsSceneMouseEvent *event) override;
        bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

    };

} // namespace SymbolEditor

