#pragma once

#include "abstracttool.h"

namespace SymbolEditor
{
    class GraphicsCircleItem;

    class PlaceCircleTool : public AbstractTool
    {
        Q_OBJECT

    public:
        PlaceCircleTool(QObject *parent = nullptr);
        ~PlaceCircleTool();

    private:
        enum State
        {
            WaitingForActivation,
            WaitingForCenter,
            WaitingForRadius,
        };
        State m_state;
        QPointF m_p1;
        QPointF m_p2;
        GraphicsCircleItem *m_item;

        void addItem();
        void updateItem();
        void removeItem();

        // GraphicsTool interface
    public:
        void activate(Scene *scene) override;
        void desactivate() override;

        // GraphicsTool interface
    protected:
        bool mouseClickEvent(QGraphicsSceneMouseEvent *event) override;
        bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

    };

} // namespace SymbolEditor

