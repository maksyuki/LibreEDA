#pragma once

#include "tool/abstracttool.h"

#include <QPointF>

class QGraphicsItem;

namespace SymbolEditor
{


    class MoveItemTool : public AbstractTool
    {
    public:
        MoveItemTool(QObject *parent = nullptr);

    private:
        enum State
        {
            WaitForActivation,
            WaitForOrigin,
            WaitForDestination
        };
        State m_state;
        QList<QGraphicsItem *> m_items;
        QPointF m_origin;
        QPointF m_destination;

        // GraphicsTool interface
        void moveItems(const QPointF &pos);

        void pushCommand();

    public:
        void activate(Scene *scene) override;
        void desactivate() override;

        // GraphicsTool interface
    protected:
        bool mouseClickEvent(QGraphicsSceneMouseEvent *event) override;
        bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    };

}
