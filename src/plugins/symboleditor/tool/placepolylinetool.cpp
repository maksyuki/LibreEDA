#include "tool/placepolylinetool.h"
#include "item/polylineitem.h"
#include "xdl/symbol.h"
#include "command/additemcommand.h"

#include <QAction>
#include <QList>

namespace SymbolEditor
{

    PlacePolyineTool::PlacePolyineTool(QObject *parent):
        PlacementTool(parent), m_item(nullptr)
    {
        QAction *action = new QAction(QIcon::fromTheme("draw-polyline"),
                                      "<b>P</b>lace a Poly<b>l</b>ine <i>p,l</i>", nullptr);
        action->setShortcut(QKeySequence("p,l"));
        setAction(action);
        setToolGroup("interactive-tools");
    }

    GraphicsItem *PlacePolyineTool::beginInsert(const QPointF &pos)
    {
        m_item = new GraphicsPolylineItem();
        m_item->setPos(pos);
        return m_item;
    }

    void PlacePolyineTool::addPoint(int idx, const QPointF &pos)
    {
        Q_ASSERT(idx < 2);

        QPointF itemPos = m_item->mapFromScene(pos);
        if (idx == 0)
        {
            setP1(itemPos);
        }
        else
        {
            setP2(itemPos);
        }
    }

    void PlacePolyineTool::freezePoint(int idx, const QPointF &pos)
    {
        Q_UNUSED(pos);
        if (idx == 0)
        {
            return;
        }

        QList<QPointF> vertices;
        vertices << m_item->line().p1() << m_item->line().p2();

        auto command = new AddItemCommand();
        command->setItemType(Polyline);
        command->setItemProperty(PositionProperty, m_item->mapToScene(m_item->pos()));
        command->setItemProperty(VerticesProperty, QVariant::fromValue<QList<QPointF>>(vertices));
        command->setItemProperty(RotationProperty, m_item->rotation());
        command->setItemProperty(OpacityProperty, m_item->opacity()*100);
        command->setItemProperty(XMirroredProperty, m_item->isXMirrored());
        command->setItemProperty(YMirroredProperty, m_item->isYMirrored());
        command->setItemProperty(LockedProperty, !m_item->isEnabled());
        command->setItemProperty(VisibilityProperty, m_item->isVisible());
        command->setItemProperty(LineColorProperty, m_item->lineColor());
        command->setItemProperty(LineWidthProperty, m_item->lineWidth());
        command->setItemProperty(LineStyleProperty, m_item->lineStyle());
        command->setItemProperty(FillColorProperty, m_item->fillColor());
        command->setItemProperty(FillStyleProperty, m_item->fillStyle());
        emit commandRequested(command);

        delete m_item;
        m_item = nullptr;

        resetTool();
    }

    bool PlacePolyineTool::removePoint(int idx, const QPointF &pos)
    {
        Q_UNUSED(pos);
        Q_UNUSED(idx);
        return false; // Remove and delete line
    }

    void PlacePolyineTool::movePoint(int idx, const QPointF &pos)
    {
        Q_ASSERT(idx < 2);

        QPointF itemPos = m_item->mapFromScene(pos);
        if (idx == 0)
        {
            setP1(itemPos);
        }
        else
        {
            setP2(itemPos);
        }
    }

    void PlacePolyineTool::endInsert(const QPointF &pos)
    {
        Q_UNUSED(pos);
    }

    void PlacePolyineTool::cancelInsert()
    {

    }

    void PlacePolyineTool::setP1(const QPointF &pos)
    {
        QLineF line = m_item->line();
        line.setP1(pos);
        m_item->setLine(line);
    }

    void PlacePolyineTool::setP2(const QPointF &pos)
    {
        QLineF line = m_item->line();
        line.setP2(pos);
        m_item->setLine(line);
    }

}
