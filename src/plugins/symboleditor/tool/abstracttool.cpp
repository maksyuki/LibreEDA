#include "abstracttool.h"
#include "command/command.h"
#include "view/scene.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QAction>

namespace SymbolEditor
{

    AbstractTool::AbstractTool(QObject *parent):
        QObject (parent),
        m_action(new QAction(this)),
        m_scene(nullptr)
    {
    }

    AbstractTool::~AbstractTool()
    {
        qDeleteAll(m_pendingCommands);
    }

    void AbstractTool::setLabel(const QString &label)
    {
        m_label = label;
    }

    void AbstractTool::setShortcut(const QKeySequence &shortcut)
    {
        m_shortcut = shortcut;
    }

    QAction *AbstractTool::action() const
    {
        return m_action;
    }

    QList<QWidget *> AbstractTool::widgets() const
    {
        return m_widgets;
    }

    UndoCommand *AbstractTool::nextPendingCommand()
    {
        if (m_pendingCommands.isEmpty())
        {
            return nullptr;
        }
        return m_pendingCommands.takeFirst();
    }

    bool AbstractTool::hasPendingCommand() const
    {
        return !m_pendingCommands.isEmpty();
    }

    void AbstractTool::setIcon(const QIcon &icon)
    {
        m_icon = icon;
    }

    void AbstractTool::updateAction()
    {
        m_action->setShortcut(m_shortcut);
        m_action->setToolTip(QString("%1 <i>%2</i>").arg(m_label).arg(m_shortcut.toString()));
        m_action->setIcon(m_icon);
        m_action->setCheckable(true);
        m_action->setChecked(false);
    }

    void AbstractTool::setWidgets(const QList<QWidget *> &widgets)
    {
        m_widgets = widgets;
    }

    // TODO: call setup()/cleanup()
    void AbstractTool::setScene(Scene *scene)
    {
        if (m_scene != nullptr)
        {
            //m_scene->removeEventFilter(this);
        }

        m_scene = scene;

        if (m_scene != nullptr)
        {
            //m_scene->installEventFilter(this);
        }
    }

    Scene *AbstractTool::scene() const
    {
        return m_scene;
    }

    void AbstractTool::appendCommand(UndoCommand *command)
    {
        m_pendingCommands.append(command);
        emit newCommandAvailable();
    }

    bool AbstractTool::mouseClickEvent(QGraphicsSceneMouseEvent *event)
    {
        Q_UNUSED(event);
        return false;
    }

    bool AbstractTool::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
    {
        Q_UNUSED(event);
        return false;
    }

    bool AbstractTool::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
    {
        Q_UNUSED(event);
        return false;
    }

}
