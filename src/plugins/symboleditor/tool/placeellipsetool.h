#pragma once

#include "abstracttool.h"

namespace SymbolEditor
{
    class GraphicsEllipseItem;

    class PlaceEllipseTool : public AbstractTool
    {
        Q_OBJECT

    public:
        PlaceEllipseTool(QObject *parent = nullptr);
        ~PlaceEllipseTool();

    private:
        enum State
        {
            WaitingForActivation,
            WaitingForCenter,
            WaitingForXRadius,
            WaitingForYRadius,
        };
        State m_state;
        QPointF m_center;
        QPointF m_xRadiusPos;
        QPointF m_yRadiusPos;
        GraphicsEllipseItem *m_item;

        void addItem();
        void updateItem();
        void removeItem();

        // GraphicsTool interface
    public:
        void activate(Scene *scene) override;
        void desactivate() override;

        // GraphicsTool interface
    protected:
        bool mouseClickEvent(QGraphicsSceneMouseEvent *event) override;
        bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

    };

} // namespace SymbolEditor

