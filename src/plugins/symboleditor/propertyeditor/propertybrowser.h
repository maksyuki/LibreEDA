
#pragma once

#include <qtpropertybrowser/qtpropertybrowser.h>
#include <qtpropertybrowser/qttreepropertybrowser.h>
#include <qtpropertybrowser/qtgroupboxpropertybrowser.h>
#include <qtpropertybrowser/qtbuttonpropertybrowser.h>

namespace SymbolEditor
{

    typedef QtAbstractPropertyBrowser PropertyBrowser;
    typedef ::QtTreePropertyBrowser TreePropertyBrowser;
    typedef ::QtGroupBoxPropertyBrowser GroupBoxPropertyBrowser;
    typedef ::QtButtonPropertyBrowser ButtonPropertyBrowser;

}
