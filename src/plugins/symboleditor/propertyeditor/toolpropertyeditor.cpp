#include "toolpropertyeditor.h"
#include "propertybrowser.h"
#include "propertymanager.h"
#include "tool/abstracttool.h"

#include <QVBoxLayout>

#include <QVariant>
#include <QMetaObject>
#include <QMetaProperty>

namespace SymbolEditor {

    ToolPropertyEditor::ToolPropertyEditor(QWidget *parent):
        QWidget (parent),
        m_updatingProperties(false),
        m_tool(nullptr)
    {
        setLayout(new QVBoxLayout);

        // TODO: setBrowser();
        //m_browser = new QtGroupBoxPropertyBrowser;
        m_browser = new QtButtonPropertyBrowser;
        //m_browser = new TreePropertyBrowser();
        layout()->addWidget(m_browser);
        layout()->setMargin(0);

        // TODO: setManager();
        m_manager = new PropertyManager(this);
        m_manager->setBrowserFactories(m_browser);

        connect(m_manager, &PropertyManager::propertyAdded,
                m_browser, &QtAbstractPropertyBrowser::addProperty);

        connect(m_manager, &PropertyManager::valueChanged,
                this, &ToolPropertyEditor::onValueChanged);
    }

    ToolPropertyEditor::~ToolPropertyEditor()
    {
    }

    void ToolPropertyEditor::addCoordinate(const QString &display,
                                           const char *name)
    {
//        auto *metaObject = m_tool->metaObject();
//        auto index = metaObject->indexOfProperty(name);
//        Q_ASSERT(index>0);
//        auto metaProperty = metaObject->property(index);
//        auto id = quint64(index);
//        m_manager->addCoordinate(id, display);
//        m_manager->setValue(id, metaProperty.read(m_tool));
    }

    void ToolPropertyEditor::addLineStyle(const QString &display,
                                          const char *name)
    {
//        auto *metaObject = m_tool->metaObject();
//        auto index = metaObject->indexOfProperty(name);
//        Q_ASSERT(index>0);
//        auto metaProperty = metaObject->property(index);
//        auto id = quint64(index);
//        m_manager->addLineStyle(id, display);
//        m_manager->setValue(id, metaProperty.read(m_tool));
    }

    void ToolPropertyEditor::addLineWidth(const QString &display, const char *name)
    {
//        auto *metaObject = m_tool->metaObject();
//        auto index = metaObject->indexOfProperty(name);
//        Q_ASSERT(index>0);
//        auto metaProperty = metaObject->property(index);
//        auto id = quint64(index);
//        m_manager->addLineWidth(id, display);
//        m_manager->setValue(id, metaProperty.read(m_tool));
    }

    void ToolPropertyEditor::addColor(const QString &display, const char *name)
    {
//        auto *metaObject = m_tool->metaObject();
//        auto index = metaObject->indexOfProperty(name);
//        Q_ASSERT(index>0);
//        auto metaProperty = metaObject->property(index);
//        auto id = quint64(index);
//        m_manager->addColor(id, display);
//        m_manager->setValue(id, metaProperty.read(m_tool));
    }

    void ToolPropertyEditor::addFillStyle(const QString &display, const char *name)
    {
//        auto *metaObject = m_tool->metaObject();
//        auto index = metaObject->indexOfProperty(name);
//        Q_ASSERT(index>0);
//        auto metaProperty = metaObject->property(index);
//        auto id = quint64(index);
//        m_manager->addFillStyle(id, display);
//        m_manager->setValue(id, metaProperty.read(m_tool));
    }

    void ToolPropertyEditor::applySettings(const LeGraphicsView::Settings &settings)
    {
        Q_UNUSED(settings);
    }

    void ToolPropertyEditor::setEdaPalette(LeGraphicsView::Palette palette)
    {
        m_manager->setPalette(palette);
    }

//    void ToolPropertyEditor::setTool(InteractiveTool *tool)
//    {
//        m_browser->clear();
//        m_manager->clear();
//        m_tool = tool;
//    }

    void ToolPropertyEditor::onValueChanged(quint64 id, const QVariant &value)
    {
//        if (m_updatingProperties)
//        {
//            return;
//        }
//        QMetaProperty metaproperty = m_tool->metaObject()->property(int(id));
//        metaproperty.write(m_tool, value);
    }
}
