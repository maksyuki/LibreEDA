#pragma once

#include "OldGraphicsView/Palette.h"
#include "OldGraphicsView/Settings.h"

#include <QWidget>
#include <QMetaProperty>

class QtAbstractPropertyBrowser;

namespace SymbolEditor
{
    class PropertyManager;
    class Tool;

    class ToolPropertyEditor : public QWidget
    {
    public:
        ToolPropertyEditor(QWidget *parent = nullptr);
        ~ToolPropertyEditor();

        void addCoordinate(const QString &display,
                           const char *propertyName);
        void addLineStyle(const QString &display,
                          const char *name);
        void addLineWidth(const QString &display,
                          const char *name);
        void addColor(const QString &display,
                      const char *name);
        void addFillStyle(const QString &display,
                          const char *name);

        void applySettings(const LeGraphicsView::Settings &settings);
        void setEdaPalette(LeGraphicsView::Palette palette);

        //void setTool(InteractiveTool *tool);

    private slots:
        void onValueChanged(quint64 id, const QVariant &value);
    private:
        bool m_updatingProperties;
        QtAbstractPropertyBrowser *m_browser;
        PropertyManager *m_manager;
        Tool *m_tool;
    };

}

