#pragma once

#include <QGraphicsScene>

#include "OldGraphicsView/Scene.h"
#include "OldGraphicsView/View.h"

namespace SymbolEditor
{
    class View : public LeGraphicsView::View
    {
        Q_OBJECT

    public:
        explicit View(QWidget *parent = nullptr);
        ~View();
    };

}
