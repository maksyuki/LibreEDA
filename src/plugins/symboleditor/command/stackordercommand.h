#pragma once

#include "command.h"

#include <QMap>

namespace SymbolEditor {

    class Item;

    class StackOrderCommand : public UndoCommand
    {
    public:
        StackOrderCommand(UndoCommand *parent = nullptr);
        ~StackOrderCommand();

        void setItem(quint64 id);
        void setReferenceItem(quint64 id);
        void setStackOperation(StackOrderOperation operation);

    private:
        StackOrderOperation m_operation;
        quint64 m_itemId;
        quint64 m_referenceItemId;
        int m_oldItemIndex;
        int m_oldRefIndex;

        void stackUp(int itemIndex, int refIndex);
        void stackDown(quint64 referenceId);

        // QUndoCommand interface
    public:
        void undo() override;
        void redo() override;
    };

} // namespace SymbolEditor

