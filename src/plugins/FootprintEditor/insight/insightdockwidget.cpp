#include "insightdockwidget.h"

#include "views/mainview.h"
#include "OldGraphicsView/NavigationView.h"

#include <QSplitter>

namespace FootprintEditor
{

    InsightDockWidget::InsightDockWidget(QWidget *parent) :
        QDockWidget(parent),
        m_mainView(nullptr),
        m_navigationView(new LeGraphicsView::NavigationView())
    {
        setWidget(m_navigationView);
    }

    void InsightDockWidget::attachView(MainView *view)
    {
        m_navigationView->setMainView(view);
    }

}
