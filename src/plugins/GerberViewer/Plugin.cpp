#include "Plugin.h"
#include "EditorFactory.h"
//#include "settings/appearancesettingspage.h"

#include "xdl/symbol.h"

// See Q_INIT_RESOURCE documentation for why this is done this way
static void GerberViewer_InitialiseResources()
{
    Q_INIT_RESOURCE(GerberViewer);
}

namespace GerberViewer
{

    Plugin::Plugin(QObject *parent):
        IPlugin(parent)
    {
        GerberViewer_InitialiseResources();
        registerMetaTypes();
    }

    Plugin::~Plugin()
    {

    }

    bool Plugin::initialize(const QStringList &arguments, QString *errorString)
    {
        Q_UNUSED(arguments);
        Q_UNUSED(errorString);

        m_editorFactory = new EditorFactory(this);
        addObject(m_editorFactory);

//        m_settingsPage = new AppearanceSettingsPage(this);
//        connect(m_settingsPage, &AppearanceSettingsPage::settingsChanged,
//                this, &Plugin::applySettings);
//        addObject(m_settingsPage);

        return true;
    }

    void Plugin::extensionsInitialized()
    {

    }

    void Plugin::shutdown()
    {

    }

    void Plugin::applySettings()
    {
        m_editorFactory->applySettings();
    }

    void Plugin::registerMetaTypes()
    {

    }

}
