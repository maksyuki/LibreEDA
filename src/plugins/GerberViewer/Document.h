#pragma once

#include "GerberViewer.h"

#include "core/editormanager/idocument.h"

#include <QScopedPointer>

namespace LeGerber
{
    class Object;
    class GraphicsScene;
}

namespace GerberViewer
{

    class GERBERVIEWER_EXPORT Document: public IDocument
    {
        Q_OBJECT

    public:
        explicit Document(QObject *parent = 0);

        bool open(QString *errorString, const QString &fileName);

        LeGerber::GraphicsScene *scene();

    signals:

    public slots:

    private:
        QList<LeGerber::Object*> m_objects;
        LeGerber::GraphicsScene *m_scene;

        // IDocument interface
    public:
        virtual bool save(QString *errorString, const QString &fileName) override;
        virtual void render(QPainter *painter) override;
    };

}
