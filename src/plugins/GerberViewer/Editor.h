#pragma once

#include "GerberViewer.h"
#include "core/editormanager/ieditor.h"
#include "OldGraphicsView/Settings.h"

namespace LeGraphicsView
{
    class View;
}

namespace GerberViewer
{

    class Document;

    class GERBERVIEWER_EXPORT Editor : public IEditor
    {
        Q_OBJECT
    public:
        explicit Editor(QObject *parent = nullptr);
        ~Editor();

    public slots:
        void applySettings(const LeGraphicsView::Settings &settings);

    private:
        Document *m_document;
        LeGraphicsView::View *m_view;

        // IEditor interface
    public:
        bool open(QString *errorString, const QString &fileName);
        IDocument *document() const;
        QIcon icon() const;
        QString displayName() const;
        void activate(QMainWindow *mainWindow);
        void desactivate(QMainWindow *mainWindow);
    };
}
