#pragma once

#include <qglobal.h>

#if defined(SIMPLETEXTEDITOR_LIBRARY)
#  define SIMPLETEXTEDITOR_EXPORT Q_DECL_EXPORT
#else
#  define SIMPLETEXTEDITOR_EXPORT Q_DECL_IMPORT
#endif

