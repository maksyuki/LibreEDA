import qbs 1.0

LedaPlugin {
    name: "SimpleTextEditor"

    cpp.defines: base.concat([
                                 "SIMPLETEXTEDITOR_LIBRARY",
                             ])
    
    Depends { name: "Qt"; submodules: ["core", "gui", "widgets", "opengl"] }
    Depends { name: "Core" }
    //Depends { name: "Utils" }

    files: [
        "simpletextdocument.cpp",
        "simpletextdocument.h",
        "simpletexteditor.cpp",
        "simpletexteditor.h",
        "simpletexteditor.qrc",
        "simpletexteditor_global.h",
        "simpletexteditorconstants.h",
        "simpletexteditorfactory.cpp",
        "simpletexteditorfactory.h",
        "simpletexteditorplugin.cpp",
        "simpletexteditorplugin.h",
    ]
}
